%TABLE  Show channel in a table.

function [] = table(chan)

time = get(chan,'Time');
data = double(chan);

data = [time, data];

name = genvarname(get(chan,'Name'));

assignin('base',name,data);
openvar(name);
