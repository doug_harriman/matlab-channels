% pictread
% Reads in an picure file and returns an object of the 
% picture class.
%
% Syntax:  >>image = pictread(fileName)
%

function im = pictread(filename)

% Open dialog box if no input filename
if ~nargin,
   [filename,pathname] = uigetfile('*.bmp;*.jpg;*.tif',...
                                   'Open Image File') ;
   filename = [pathname '\' filename] ;
end

% Read the data
im      = imfinfo(filename);
im.data = imread(filename);

% Set the class
im = pict(im);

%*.hdf;*.jpeg;*.tiff;*.xwd*.pcx;
