%MECH  Set's current mech.

function [val] = mi_servo_mech(val)

grp     = 'MI_SERVO';
varname = 'CurrentMech';

if nargin == 0
    % Get call
    if ~ispref(grp,varname)
        warning('No mech set.  Set with: >> mech <mech>');
        val = [];
        return;
    end 
       
    val = getpref(gpr,varname);
    return;
end

% Set call
setpref(grp,varname,val);