%SYNCOFFSET  Synchronizes time offsets for all channels in Channel Manager.
%   Searches through all channels in Channel Manager looking for minimum
%   time offset.  This value is then used as 0 time, and all channels are
%   syncronized to this 0 time.
%

% To do:
% - Should ignore channels with sample times instead of time vectors.
%       These channels always have a min time of 0.

% DLH
% 06/25/04 - Created

function [] = syncoffset()

chanList = chanman('GetChannelAll');
if isempty(chanList), return; end
chanman('ClearAll');

% Loop 1 finds global minimum time.
nChan = length(chanList);
minTime = inf;
for iChan = 1:nChan,
    time = get(chanList{iChan},'Time');
    minTime = min(minTime,min(time));
end

% Loop 2 offsets to global minimum time
for iChan = 1:nChan,
    time = get(chanList{iChan},'Time');
    time = time - minTime;
    chanList{iChan} = set(chanList{iChan},'Time',time);
    chanman('AddChannel',chanList{iChan});
end
