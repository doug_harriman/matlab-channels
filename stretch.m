% stretch
% Makes a plot fill the full x axis.
%

function [] = stretch(axhan)

% error check
if nargin==0,
   axhan = gca ;
end

% Get max x point
linehan = get(axhan,'children') ;
linehan = findobj(linehan,'Type','line');
linehan = linehan(1) ;
data = get(linehan,'xdata') ;

% Set the limit
set(axhan,'xlim',[min(data) max(data)]) ;



