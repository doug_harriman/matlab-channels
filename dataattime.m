%DATAATTIME  Returns interpolated data value from 
%            a channel at the specified independent 
%            variable value.
%
% >> value = dataattime(channel,time)
%
function value = dataattime(chan,time)

% Get data
data = get(chan,'Data');
timeVec = get(chan,'Time');

if ~isa(data,'double'),
   data = double(data);
end

value = interp1(timeVec, data, time);
