%EDUNIT  Edit channel units.
%
% >> edunit(channel)
%

function [] = edname(chan)

% Setup for dialog box
prompt  = 'Channel Units:';
default = {get(chan,'units')};
title   = 'Units Change';
lineNo  = 1;

% Get and set
newName = inputdlg(prompt,title,lineNo,default);
if ~isempty(newName),
   set(chan,'units',newName{1});
   assignin('caller',inputname(1),chan) ;
end