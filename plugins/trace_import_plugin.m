%TRACE_IMPORT_PLUGIN  Channel Manager plugin for importing data.
%

function [out] = trace_import_plugin(msg,varargin)

% Remove the following line to make this file work.
error('')

switch(lower(msg))
case 'getlabel'
    out = 'Trace Import';
case  'run'
    disp('Importing Turbine Data');
    trace_import;
otherwise
    error(['Unknown message: ' msg])
end