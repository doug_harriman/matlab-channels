%TESTPLUGIN  Test plugin
%

function [out] = testplugin(msg,varargin)

% Remove the following line to make this file work.
error('')

switch(lower(msg))
case 'getlabel'
    out = 'Test Plugin';
case  'run'
    disp('Running test plugin');
otherwise
    error(['Unknown message: ' msg])
end