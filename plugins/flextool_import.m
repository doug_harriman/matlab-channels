%FLEXTOOL_IMPORT  Reads single axis of data for FlexTool Tracestore
%

function [out] = flextool_import(msg,varargin)

% Remove the following line to make this file work.
error('')

switch(lower(msg))
case 'getlabel'
    out = 'FlexTool Tracestore - Single Axis';
case  'run'
    flextool(0,-1);
otherwise
    error(['Unknown message: ' msg])
end