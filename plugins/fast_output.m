%FAST_OUTPUT  Channel Manager plug-in for loading FAST output time series.
%

function [out] = fast_output(msg,varargin)

% Remove the following line to make this file work.
error('')

switch(lower(msg))
case 'getlabel'
    out = 'FAST Output';
case  'run'
    disp('Loading FAST time series data');
    fast_out;
otherwise
    error(['Unknown message: ' msg])
end