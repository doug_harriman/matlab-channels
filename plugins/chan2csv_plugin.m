%CHAN2CSV_PLUGIN
%

function [out] = chan2csv_pugin(msg,varargin)

switch(lower(msg))
    case 'getlabel'
        out = 'Write CSV File';
    case  'run'
        % Prompt for file name
        [fileName,pathName] = uiputfile('*.csv','Save As CSV File');

        % Make sure we got valid name
        if ~isstr(fileName),
            chanman('SetMessage','Channels to CSV file cancelled.');
            return;
        end

        % Make sure have suffix
        if strcmp(fileName(end-4:end),'.csv')
            % Join file name
            fileName = fullfile(pathName,fileName);
        end

        % Get channel data
        chanman('SetMessage',['Writing to file: ' fileName]);
        chans = chanman('GetChannelAll');
        
        % Write the file
        chan2csv(fileName,chans);
        chanman('SetMessage',['Finished writing data to: ' fileName]);
        
    otherwise
        error(['Unknown message: ' msg])
end