%IMPORT_TEK

function [out] = tek_import(msg,varargin)

switch(lower(msg))
case 'getlabel'
    out = 'TEK Scope Data Import';
case  'run'
   tek2chan;
otherwise
    error(['Unknown message: ' msg])
end