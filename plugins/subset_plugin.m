%CHANMANSUBSET_PLUGIN  Channel Manager plugin for subset extraction.
%

function [out] = chanmansubset_plugin(msg,varargin)

switch(lower(msg))
case 'getlabel'
    out = 'Subset';
case  'run'
    chanmansubset;
otherwise
    error(['Unknown message: ' msg])
end