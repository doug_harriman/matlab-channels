%TURBINE_IMPORT_PLUGIN  Channel Manager plugin for importing data.
%

function [out] = turbine_import_plugin(msg,varargin)

% Remove the following line to make this file work.
error('')

switch(lower(msg))
case 'getlabel'
    out = 'Turbine Import';
case  'run'
    disp('Importing Turbine Data');
    turbine_import;
otherwise
    error(['Unknown message: ' msg])
end