%EXCEL_EXPORT  Writes channels to Excel file.
%

function [out] = excel_export(msg,varargin)

switch(lower(msg))
    case 'getlabel'
        out = 'Write to Excel File';
    case  'run'
        % Handle the file name
        if nargin < 2,
            % Prompt for file name
            [fileName,pathName] = uiputfile('*.xls','Save As Excel File');
            
            % Make sure we got valid name
            if ~isstr(fileName),
                chanman('SetMessage','Channels to Excel file cancelled.');
                return;
            end
            
            % Make sure have suffix
            if strcmp(fileName(end-4:end),'.xls')
                
                % Join file name
                fileName = fullfile(pathName,fileName);
            end
            
            % Get channel data
            chanman('SetMessage',['Writing to file: ' fileName]);
            chans = chanman('GetChannelAll');
            
            % Add a data sheet.
            sheet = 'Channel Data';
            if exist(fileName) == 2
                [type,sheets] = xlsfinfo(fileName);
                orig_sheet = sheet;
                idx = 1;
                while ismember(sheet,sheets)
                    sheet = [orig_sheet ' ' num2str(idx)];
                    idx   = idx + 1;
                end
            end
            
            % Cycle through the channels, writing data
            numChans = length(chans);
            row = 2;
            
            col = 1;
            for i = 1:numChans,
                % UI
                name = get(chans{i},'Name');
                fprintf('Writing channel %d/%d "%s"...',i,numChans,name);
                
                % Time Vector
                headerStr = {'Time [sec]'};
                RC = [char(col+64) num2str(row) ];
                xlswrite(fileName,headerStr,sheet,RC);
                
                data = double(get(chans{i},'Time'));
                data = reshape(data,numel(data),1); % force column vector
                RC = [char(col+64) num2str(row+1) ];
                xlswrite(fileName,data,sheet,RC);
                col = col + 1;
                
                % Data Vector
                % Header
                uStr = get(chans{i},'Units');
                headerStr = {[name ' [' uStr ']']};
                RC = [char(col+64) num2str(row) ];
                xlswrite(fileName,headerStr,sheet,RC);
                
                % Data
                data = double(chans{i});
                data = reshape(data,numel(data),1); % force column vector
                RC = [char(col+64) num2str(row+1) ];
                xlswrite(fileName,data,sheet,RC);
                col = col + 1;
                
                fprintf('done\n');
            end
            
            % UI
            chanman('SetMessage',['File ' fileName ' written']);
            
            % Open XLS file.
            if ispc
                winopen(fileName);
                return;
            elseif isunix
                % See if Excel is available, ie Crossover Office.
                if system('which excel') == 0
                    exe = 'excel';
                elseif system('which oocalc') == 0
                    exe = 'oocalc';
                else
                    error('Not able to find EXCEL or OOCALC');
                end
                
                % Open the file
                r = system([exe ' ' '''' fileName ''' &']);
                
                if r
                    error(['Unable to open motor database file with ' exe ]);
                end
            else
                error('Unsupported OS');
            end
        end
        
    otherwise
        error(['Unknown message: ' msg])
end