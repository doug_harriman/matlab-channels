%CSV_IMPORT  Imports CSV files with the appropriately formatted header.
%  Uses CSV2CHAN.
%

function [out] = csv_import(msg,varargin)

switch(lower(msg))
case 'getlabel'
    out = 'CSV Import';
case  'run'
   csv2chan;
otherwise
    error(['Unknown message: ' msg])
end