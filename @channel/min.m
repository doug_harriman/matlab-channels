% channel/min  Min of channel data
%

function out = min(in)

% Take min
out = min(in.data) ;