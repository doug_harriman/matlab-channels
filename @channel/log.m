% channel/log  Natural logarithm of channel data
%

function out = log(in)

% Take abs
out      = in ;
out.data = log(in.data) ;
out.name = ['ln of ' in.name] ;