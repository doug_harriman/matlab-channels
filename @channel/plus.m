%PLUS Binary addition for channels
%

% Doug Harriman (doug.harriman@gmail.com)

function out = plus(chan1,chan2)

% See if they are channels or numbers,
if isa(chan1,'channel') && isa(chan2,'channel')
    % Error check
    if xor(isreal(chan1.sample),isreal(chan2.sample))
        % Mixed time/frequency data
        error('Mixed Time/Frequency Data')
    end
    if min(chan1.sample - chan2.sample)
        % Resample
        disp('Resampling Channel 2')
        chan2 = resample(chan2,chan1.sample) ;
    end
    
    % Create output object
    out = channel ;
    
    % Math
    len_c1 = length(chan1.data);
    len_c2 = length(chan2.data);
    if (len_c1 > len_c2)
        chan1.data = chan1.data(1:len_c2);
    elseif (len_c2 > len_c1)
        chan2.data = chan2.data(1:len_c1);
    end
    out.data = chan1.data + chan2.data  ;
    
    % Rename
    out.name = [chan1.name ' + ' chan2.name] ;
    
    % Error check units
    if isa(chan1.data,'unit')
        if ~isconsistent(chan1.data,chan2.data)
            error('Inconsistant Units')
        else
            out.units = chan1.units ;
        end
    end
    
    % Metadata
    out.metadata = chan1.metadata;
    
    % Sample
    out.sample = chan1.sample ;
    
elseif isa(chan1,'channel') && ~isa(chan2,'channel')
    
    % Create output object
    out = chan1 ;
    
    % Do math
    out.data = chan1.data + chan2 ;
    
    % Rename
    out.name = [chan1.name ' + ' num2str(chan2)] ;
    
elseif ~isa(chan1,'channel') && isa(chan2,'channel')
    
    % Create output object
    out = chan2 ;
    
    % Do math
    out.data = chan1 + chan2.data ;
    
    % Rename
    out.name = [num2str(chan1) ' + ' chan2.name] ;
    
else
    error(['CHANNEL/PLUS called to add objects of type: ' ...
        class(chan1) ' and ' class(chan2)]);
end