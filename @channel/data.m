% channel/data  Returns a channels data attribute
%

function out = data(chan)

out = chan.data ;