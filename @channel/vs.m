%VS  Versus, but always generates scatterplot.
%

function [handle] = vs(chan1,chan2,str)

% Error checks
error(nargchk(2,3,nargin));
if nargin < 3
    str = '';
end

% Just call versus.
han = versus(chan1,chan2,str);
set(han,'Marker','.',...
    'LineStyle','None');
