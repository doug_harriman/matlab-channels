%CHANNEL/RESAMPLE  Resamples a channel to the given sample rate.
%   channel_resampled = resample(channel, sample_rate,method)
%
%   NOTE: Sample rate may be either for time or frequency.
%   See INTERP1 for interpolation method.
%
%   See also: interp1
%

% DLH
% Updated  02/07/00

function chan = resample(chan,newRate,method)

if nargin < 3,
    method = 'linear';
end

% Handle time/frequency specification with units
if isa(newRate,'unit')
    % Frequency specification
    if isconsistent(newRate,'Hz')
        % No-op, just error checking.
    elseif isconsistent(newRate,'sec')
        newRate = 1/newRate;
    else
        error('Inconsistent units.  Rate must be either frequency or time.');
    end
    
    % Now we know we have a rate, convert it.
    newRate = convert(newRate,'Hz');
    newRate = double(newRate);
end

% Error check
if xor(isreal(chan.sample),isreal(newRate)),
    % Mixed time/frequency data
    error('Channel and attribute must both be time or frequency') ;
end

% Interpolate data
if length(chan.sample) == 1,
    iv_old = 0:chan.sample:chan.sample*(length(chan.data)-1);
else
    iv_old = chan.sample;
end

% Make sure that time vector starts at 0.
% This will not be true if there is a time offset.
data = double(chan.data);
if(iv_old(1) ~= 0)
    iv_old = reshape(iv_old,numel(iv_old),1);
    data   = reshape(data,numel(data),1);
    
    iv_old = [0; iv_old];
    data   = [data(1); data];
end

% Create new interpolation vector
if length(newRate) == 1
    iv_new = 0:1/newRate:max(iv_old);
else
    iv_new = newRate;
end

% Clean up any single time data point repeats.
idx = diff(iv_old)==0;
if any(idx)
    error('Time vector has repeated time values.');
end

% Linear interpolation
data = interp1(iv_old,data,iv_new,method);

% Make sure no NaN's appeared.
data(isnan(data)) = 0;

% Force column vector
data = reshape(data,numel(data),1);

% Create output channel
if isa(chan.data,'unit'),
    data = unit(data,char(chan.data));
end

chan.data = data;
if length(newRate) == 1,
    chan.sample = 1/newRate;
else
    chan.sample = newRate;
end

