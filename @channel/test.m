% channel/test  Gets/Sets test definition of a channel.
%
% string  = test(channel)         Returns test.
% channel = test(channel,string)  Sets test.
%

function out = test(chan,in)

% 1 input -> return units
if nargin == 1,
   out = chan.test ;
elseif nargin == 2,
   out = chan ;
   out.test = in ;
else,
   error('Illegal input to channel/units')
end
