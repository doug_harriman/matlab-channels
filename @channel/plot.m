% channel/plot  Plots channel data
%

function han = plot(varargin)

% Don't plot in DataLink Window
if strcmp(get(gcf,'tag'),'DataLinkWindow'),
    figure;
end

% Hold the plot for all plots
oldhold = ishold ;
if ~oldhold,
    cla
end
hold on

% Cycle through arguments
k = 1 ;
plotstr = '' ;
for i = 1:length(varargin),
    if ischar(varargin{i}),
        plotstr{k-1} = varargin{i} ;
    else
        ch{k} = varargin{i} ;
        
        % Plot channel default color if exists.
        plotstr{k} = ch{k}.plot_color;
        k = k + 1 ;
        
    end
end

% Cycle through channels
for i = 1:k-1,
    
    % Get channels
    chan = ch{i} ;
    x    = sampvec(chan) ;
    y    = chan.data     ;
    
    % Apply time units if there are none.
    if ~isa(x,'unit')
        if isreal(chan.sample)
            x = unit(x,'sec');
        else
            x = unit(x,'Hz');
        end
    end
    
    % Try to find reasonable time units for plotting.
    % TODO - This should be an option for users to set plot units.
    if isa(x,'unit')
        if isconsistent(x,unit(1,'sec'))
            % Get time span
            dx = range(x);
            
            if dx < unit(1,'sec')
                x = convert(x,'ms');
                
            elseif dx < unit(10,'min')
                x = convert(x,'sec');
                
            elseif dx < unit(3,'hr')
                x = convert(x,'min');
                
            elseif dx < unit(2,'day')
                x = convert(x,'hr');
                
            elseif dx < unit(4,'week')
                x = convert(x,'day');
                
            elseif dx < unit(1,'yr')
                x = convert(x,'week');
                
            else
                x = convert(x,'yr');
            end
        end
    end
    
    % Plot it
    temp = plot(x,y,plotstr{i});
    han(i) = temp(1);
    
    % Store application specific data
    setappdata(han(i),'ChannelName',chan.name);
    setappdata(han(i),'ChannelUnits',chan.units);
    setappdata(han(i),'ChannelMetadata',chan.metadata);
    set(han(i),'DisplayName',chan.name);
    
    % Special Y-axis handling of "boolean" data.
    tmpy = y;
    tmpy(isnan(tmpy)) = 0;
    uy = unique(tmpy);
    if (length(uy) == 2) && all(uy == [0 1]')
        % Have a boolean vector
        % if we're the only thing on the plot, set the ylim.
        if length(get(gca,'Children')) == 1
            set(gca,'ylim',[-0.5 1.5]);
        end
    end
    
    % Label units are handled by the units Toolbox.
    % Add text to units if needed.
    label = get(get(gca,'ylabel'),'string');
    if ~isempty(label) && (label(1) == '[')
        name = strrep(chan.name,'_',' ');
        label = [name ' ' label]; %#ok<AGROW>
        
        % Pretty plot a few units.
%         label = strrep(label,'deg','\circ');
        
        ylabel(label);
    end
    
    % Y label for channels without units
    if isempty(label)
        ylabel(chan.name);
    end
    
    label = get(get(gca,'xlabel'),'string');
    if ~isempty(label) && (label(1) == '[')
        if isconsistent(x,'sec')
            label = ['Time ' label];
        end
        if isconsistent(x,'Hz')
            label = ['Frequency ' label];
        end
        xlabel(label);
    end
    
    % Title if none exists.
    hTitle = get(gca,'Title');
    str = get(hTitle,'String');
    
    if isempty(str)
        % Grab channel metadata
        m = meta(chan);
        if ~isempty(m.sourcefile)
            title(m.sourcefile,'Interpreter','None');
        end
    end
end

% Create grid
grid('on');
set(gca,'Box','on');

% Restore hold state
if ~oldhold
    hold off
end

% Remove any possibly empty handles from the handle array
han = han(han~=0)' ;

% Activate plottools
pt;

% See if we have output
if ~nargout
    clear han
end