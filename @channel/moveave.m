%MOVEAVE Moving average of a regularly sampled signal.
% syntax: out = moveave(channel, points, type)
%	out     = moving average signal.
%	channel = input signal.
%	points  = number of points for average.
%   avetype = 'center' or 'trailing'.  
%             Default is trailing. Trailing is ~100x faster.
%

% DLH

function [avg,stdev] = moveave(in,pts,avetype)

% Error checks
narginchk(2,3);

% Sample rate check
rate = get(in,'samplerate');
if rate == 0
    error('Channel must have a fixed sample rate.');
end

% Get data
data = double(get(in,'data'));
data = reshape(data,1,length(data));
npts = length(data);
if npts <= pts
    error(['data vector too short for a ' int2str(pts) ' point moving average.']);
end

if nargin < 3
    avetype = 'trailing'; 
end

% Preallocate
avg   = ones(size(data))*NaN;
stdev = ones(size(data))*NaN;

switch avetype
    case 'center'
        halfPts = round((pts-1)/2);
        % Loop through all data points
        for i = 1:npts
            
            % Case i<pts
            if i<=halfPts 
                % Startup
                vec = data(1:2*i-1);
            elseif i+(pts-1)/2+1 > npts
                % Wind down
                dist = npts - i;
                vec = data(i-dist:end);
            else
                % Update vector
                vec = data(i-halfPts:i+halfPts);
            end
            
            % Calculate the ave
            avg(i)   = mean(vec);
            stdev(i) = std(vec);
            
        end   
        
    case 'trailing'
        % Fully vectorized and accelerated.
        avg = filter(ones(1,pts)/pts,1,data);
        stdev = [];
        
    otherwise
        error(['Unsupported averaging type: ' upper(avetype)]);
end


avg = channel([num2str(pts) ' point ' avetype ' moving average of ' get(in,'name')],...
    avg,get(in,'sampletime'),get(in,'units'));
if ~isempty(stdev)
    stdev = channel([num2str(pts) ' point ' avetype ' moving std dev of ' get(in,'name')],...
        stdev,get(in,'sampletime'),get(in,'units'));
end
set(avg,'plotcolor','g');


