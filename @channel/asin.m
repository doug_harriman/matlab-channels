% channel/asin  Arcsine of channel data
%

function out = asin(in)

% Take acos
out      = in ;
out.data = asin(in.data) ;
out.name = ['Arcsine of ' in.name] ;
out.units = 'Radians' ;

% Test
if strcmp(deblank(chan1.test.name),deblank(chan2.test.name)),
   out.test = chan1.test ;
end
