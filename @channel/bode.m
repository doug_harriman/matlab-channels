%@CHANNEL/BODE  Computes the experimental Bode plot from intput to output.
%
% Syntax: [magnitude, phase] = bode(input,output)
% magnitude []
% phase     [degrees]
%

% DLH 6/23/98

function [mag, ph] = bode(in,out)

% Build a filter for the time data
filt = buildfilt(get(in,'SampleRate')/3,4,get(in,'SampleTime')) ;

% Filter the time data
in  = filter(in,filt) ;
out = filter(out,filt) ;

% Do FFT's of signals
[y_w,w] = fft_t(get(out,'time'), get(out,'data')) ;
[u_w,w] = fft_t(get(in,'time'), get(in,'data')) ;

% Calculate bode quantities.
mag = abs(y_w./u_w);
ph  = unwrap(angle(y_w./u_w))*180/pi;

% Convert to channels
tempName = [ get(in,'name') ' to ' get(out,'name') ' TF' ];
mag = channel([tempName ' Magnitude'],mag, w*i,'',get(in,'test'));
ph  = channel([tempName ' Phase'],ph, w*i,'Hz',get(in,'test'));

% Plot if no output requested.
if nargout == 0,
   % Plots
   figure
   subplot(2,1,1);
   plot(20*log10(mag));
   set(gca,'XScale','Log');
   ylabel('Magnitude (dB)')
   title([ 'Bode Plot: ' get(in,'Name') ' -> ' get(out,'Name') ])
   
   subplot(2,1,2);
   plot(ph);
   set(gca,'XScale','Log');
   ylabel('Phase (deg)')
   
   % Clear outputs
   clear mag, ph 
   
end

