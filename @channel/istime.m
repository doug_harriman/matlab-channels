%CHANNEL/ISTIME  Returns true if channel is a time signal.
%

% DLH, 11/10/98

function val = istime(chan)

val = isreal(chan.sample);