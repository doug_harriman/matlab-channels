%CHANNEL/HASTEST  Returns 1 if the given channel object has 
%                 a defined test definition structure.
%

function result = hastest(chan)

% Default to false
result = 0;

% Get the data 
test = get(chan,'Test');

% Test it
if isempty(test.name) & isempty(test.type) & isempty(test.date) & isempty(test.time),
   return;
end

% Must not be empty
result = 1;
