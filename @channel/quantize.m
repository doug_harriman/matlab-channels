%QUANTIZE  Quantize a channel.
%   QCHAN=QUANTIZE(CHAN,QUANTA) quantizes CHAN from continuous data to
%   an integer value of steps of size QUANTA.
%

% DLH
% 05/15/05 - Created

function qchan = quantize(chan,quanta)

% Error check inputs.
if nargin ~= 2, error('Two inputs expected.'); end
if ~isscalar(quanta), error('Quanta specification must be scalar.'); end
if isnan(quanta) | isempty(quanta), error('Illegal quanta specification.');end
if quanta <= 0, error('Quantization interval must be greater than zero.'); end

% Start with output equivalent to input.
qchan = chan;
qchan.units = '';

% Quantize data
data  = get(chan,'Data');
data  = double(data);
int   = data/quanta;
int   = fix(int);
set(qchan,'Data',int);

% Update name
name = get(chan,'Name');
name = ['Quantized ' name];
set(qchan,'Name',name);