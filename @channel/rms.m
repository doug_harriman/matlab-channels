%CHANNEL/RMS  RMS value of a channel.
%

% DLH
% 09/22/04 - Created

function [val] = rms(chan)

val = (mean(chan^2))^0.5;
