% channel/display  Displays channel object.
% Can also return strings.

% DLH
% Changelog
% 01/03/03 - Added support for irregular time sequence offset.

function [n,l,s,u] = display(chan)

% Generate print strings
n  = chan.name                          ;
nn = ['Channel Name         : ' n ]     ;

l  = num2str(length(chan.data))         ;
ll = ['Number of data points: ' l ]     ;

if isa(chan.data,'unit'),
    %    chan.units = display(chan.data,'Singleline');
    chan.units = char(chan.data);
end

if isa(chan.data,'double'),
    u  = chan.units                         ;
    ustr = ['Units                : [' u ']'] ;
elseif isa(chan.data,'unit'),
    %    u = display(chan.data,'SingleLine');
    u = char(chan.data);
    ustr = ['Units                : [' u ']'] ;
end

% a = chan.axis;
% aa = ['Axis                 : ' a ]     ;
%
% t  = chan.test.name                     ;
% tt = ['Test                 : ' t ]     ;

if isreal(chan.sample)
    % We have a time data
    if length(chan.sample) == 1
        % Have a sample rate
        s  = [num2strp(inv(chan.sample)) 'Hz']  ;
        ss = ['Sample Rate          : ' s ]     ;
    else
        % Have a time sequence
        s  = [num2str(max(chan.sample)-min(chan.sample)) ' sec'];
        ss = str2mat('Irregular Time Sequence',...
            ['            Duration : ' s ])     ;

        if min(chan.sample ~= 0)
            ss = str2mat(ss,['            Offset   : ' num2str(min(chan.sample)) ' sec']);
        end
    end
else
    % We have frequency data
    if length(chan.sample) == 1,
        % Have a sample rate
        s  = [num2strp(abs(chan.sample)) 'Hz']  ;
        ss = ['Even Frequency Grid  : ' s] ;
    else
        % Have a time sequence
        s  = [num2strp(min(abs(chan.sample))) 'Hz to ' num2strp(max(abs(chan.sample))) 'Hz'] ;
        ss = str2mat('Irregular Frequency',...
            ['       Sequence, From: ' s]) ;
    end
end

% Print if necessary
if nargout == 0,
    disp(' ')
    fprintf('%s\n',nn) ;
    fprintf('%s\n',ll) ;
    if ~strcmp(chan.units,''),
        fprintf('%s\n',ustr) ;
    end
    disp(ss) ;

    % Plot chanmeta if exists
    props = fieldnames(chan);
    if ismember('chanmeta',props)
        dispshort(chan.chanmeta);
    end
    
    %    if ~strcmp(chan.axis,''),
    %        fprintf('%s\n',aa);
    %    end

    %    if ~strcmp(chan.test.name,''),
    %       fprintf('%s\n',tt) ;
    %    end
    disp(' ')
end
