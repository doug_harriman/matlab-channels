% @channel/filter
% channel_out = filter(channel_in,filter)
% filter - LTI system representing the filter to implement
% channel_in - channel to filter
% channel_out - filtered channel

function [out] = filter(in,sys) 

% Convert filter to num,den
[b,a] = tfdata(sys,'v') ;

% Apply filter
data = get(in,'data') ;
data = filter(b,a,data) ;

% set output
out = in ;
set(out,'data',data) ;