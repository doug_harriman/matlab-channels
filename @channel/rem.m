%CHANNEL/REM  Remainder value of channel data.
%  CH_OUT=ABS(CH_IN,DENOMINATOR)
%

function out = abs(in,den)

% Take abs
out      = in ;
out.data = rem(in.data,den) ;
out.name = ['Rem of ' in.name '/' num2str(den)] ;