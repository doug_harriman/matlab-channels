%CHANNEL/CUMSUM  Cumulative sum of channel data
%

% Doug Harriman (doug.harriman@gmail.com)

function [out] = cumsum(in)

% Channel components
name = ['Cumulative sum of ' get(in,'name') ' Data'];

ts = get(in,'SampleTime');
if length(ts) == 1
   if ~isa(ts,'unit')
      ts = unit(ts,'sec');
   end
   data = (cumsum(get(in,'data')))*ts;
else
   ts = unit(ts,'sec');
   dt = diff(ts);
   data = get(in,'Data');
   data = data(1:end-1);
   data = data .* dt;
   data = cumsum(data);
end

out = channel(name,data,ts);
