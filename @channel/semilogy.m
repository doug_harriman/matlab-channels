%CHANNEL/SEMILOGY Semilog plot of channel data.
%

% DLH, 11/09/98

function han = semilogy(varargin)

% Build call string
str = '';
for i=1:nargin,
   str = [str ',varargin{' num2str(i) '}'];
end
str = ['plot(' str(2:end) ');'];
eval(str);
set(gca,'YScale','Log');
