% channel/log10  Base 10 logarithm of channel data
%

function out = log10(in)

% Take abs
out      = in ;
out.data = log10(in.data) ;
out.name = ['Log Base 10 of ' in.name] ;