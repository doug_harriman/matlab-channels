%CHANNEL/DIFF   Differential of a signal.
% Output channel will have equal number of data
% points as input channel by repeating the last point.
%

function out = diff(in,subSample)

% Do derivative
if nargin == 1,
   % Simple method, backwards difference
   %data = diff([in.data(1); in.data]) ;
   data = diff(in.data);
   
   % Center difference
   %data = ( diff([in.data(1); in.data]) + diff([in.data; in.data(end)]))/2;
   
   % Housekeeping
   subSample = 1;
   
else
   % Complex method, use points 'subSample' back
   data = zeros(subSample,1);
   for i=subSample+1:length(in.data),
      data(i) = in.data(i) - in.data(i-subSample);
   end
      
end

% Set up output
out = in ;

if size(out.data,1) > size(out.data,2)
    out.data = out.data';
end

% Divide by sample rate
% Use units if data is a unit
if isa(data,'unit'),
   if isreal(in.sample),
     % out.units = [in.units '/sec'] ;
      out.name = ['d(' in.name ')/dt'] ;
      
      % Handle sample rate and time vector data
      if length(in.sample) == 1,
          t = unit(abs(in.sample)*subSample,'sec');
      else
          %t = unit(diff([in.sample(1); in.sample]),'sec');    
          t = unit(diff(in.sample),'sec');    
      end
   else
      out.units = [in.units '/Hz'];
      out.name = ['d(' in.name ')/dw'];
      t = unit(abs(in.sample)*subSample,'Hz');
   end
   out.data = data./t;
   out.data = reshape(out.data,numel(out.data),1);
   out.data = [out.data; out.data(end)];
else
   % Non unit data

   % Handle sample rate and time vector data
   if length(in.sample) == 1,
       t = abs(in.sample)*subSample;
   else
       t = diff([in.sample]);    
   end
   temp = data./t;
   if length(temp) ~= size(temp,1),
       temp = temp';
   end
   out.data = [temp;temp(end)];
   
   if isreal(in.sample),
      out.units = [in.units '/sec'] ;
      out.name = ['d(' in.name ')/dt'] ;
   else
      out.units = [in.units '/Hz'];
      out.name = ['d(' in.name ')/dw'];
   end
end


