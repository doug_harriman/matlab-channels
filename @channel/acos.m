% channel/acos  Arccosine of channel data
%

function out = acos(in)

% Take acos
out      = in ;
out.data = acos(in.data) ;
out.name = ['Arccosine of ' in.name] ;
out.units = 'rad' ;

