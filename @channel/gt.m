% channel/lt  Channel greater than
%

function out = gt(chan1,chan2)

% See if they are channels or numbers,
if isa(chan1,'channel') && isa(chan2,'channel')
   % Error check
   if xor(isreal(chan1.sample),isreal(chan2.sample))
      % Mixed time/frequency data
      error('Mixed Time/Frequency Data')
   end
   if min(chan1.sample - chan2.sample)
      % Resample
      disp('Resampling Channel 2')
      chan2 = resample(chan2,chan1.sample) ;   
   end
   
   out = chan1.data > chan2.data ;
   
   % Error check units
   if ~strcmp(deblank(chan1.units),deblank(chan2.units))
      warning('Inconsistant Units')
   end
   
elseif isa(chan1,'channel') && ~isa(chan2,'channel')
   out = double(chan1.data) > chan2 ;

elseif ~isa(chan1,'channel') && isa(chan2,'channel')
   out = chan1 > double(chan2.data) ;
   
else
   error('Unknown object pairing')

end


