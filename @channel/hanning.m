%HANNING  Applies hanning window to channel data.
%         Window is of length length(data).
%

% DLH 10/2/98

function [out]=hanning(in)

% Create hanning window
win = hanning(get(in,'Points'));

% Get data
% data = get(in,'data');
data = double(in);

% Apply the window
if ~all(size(data)==size(win))
    win = win';
end
data = data.*win;

% Create the output
out = in;
set(out,'data',data);
name = get(out,'name');
name = ['Hanning windowed ' name];
set(out,'name',name);

% Plot if no output
if nargout == 0,
   plot(out);
end
