%CHANNEL/UNWRAP Unwrap channel phase angle data.
%

function out = unwrap(in)

% Take abs
out      = in ;
out.data = unwrap(in.data) ;
out.name = ['Unwrapped angle of ' in.name] ;