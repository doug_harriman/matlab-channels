% channel/logical Channel Object to logical converter
%

function data = logical(chan)

% Make sure we have a channel
data  = double(chan);
idx = isnan(data);
if ~isempty(idx)
    warning('NaN values converted to false.');
end

data(idx) = 0;
data = logical(data);
