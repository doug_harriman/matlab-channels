% channel/cos  Cosine of channel data
%

function out = cos(in)

% Take cos
in = convert(in,'rad');
out      = in ;
out.data = cos(in.data) ;
out.name = ['Cosine of ' in.name] ;