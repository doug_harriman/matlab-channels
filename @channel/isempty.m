%ISEMPTY  Channel isempty check.
%

function tf = isempty(chan)

tf = isempty(chan.data);