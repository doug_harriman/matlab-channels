%TIMESERIES  Creates timeseries object from channel object.
%

% DLH
% 04/14/05 - Created

function [ts] = timeseries(chan)

data = get(chan,'Data');
t    = get(chan,'Time');
name = get(chan,'Name');

ts = timeseries(double(data),t,'name',name);

if isa(data,'unit')
    ts.DataInfo.Units = char(data);
end