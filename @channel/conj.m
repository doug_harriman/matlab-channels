%CHANNEL/CONJ  Return the complex conjugate of a channel.
%

% DLH, 11/08/98

function out = conj(in)

out = in;
out.data = conj(in.data);
out.name = ['Complex conjugate of ' in.name];