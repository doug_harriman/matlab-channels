% CHANNEL/PSD  Power spectral desity for a channel object.
%              Syntax: >> psd(channel) -> PSD of channel data.
%                      >> psd(channel1, channel2) -> PSD of channel 1 data 
%                                                    with channel 2 data as time vector.
%

% DLH 
% ??/??/?? - Created
% 06/15/01 - Updated to allow non-time based sampling.

function mag = psd(chan1,chan2) 

% Do FFT, convert to PSD
if nargin < 2,
    [FFT] = fft(chan1) ;
else
    [FFT] = fft(chan1,chan2);
end

% FFT -> PSD
mag   = abs(FFT)  ;
mag   = mag/length(mag);  % Normalize

% Call psd routine
if nargout == 0,
    if nargin == 1,
        % Label plot
        plot(mag);
        ylabel(['PSD of ' chan1.name]) ;
    else
        h = plot(mag);
        ylabel(['PSD of ' chan1.name]) ;
        xlabel(['Frequency [1/' get(chan2,'units') ']']);
        
        figure
        xData = get(h,'xData');
        plot(1./(xData),double(mag));
        ylabel(['PSD of ' chan1.name]) ;
        xlabel(['Frequency [' get(chan2,'units') ']']);
        pt
    end
else
    % Create output channel
    set(mag,'Name',['PSD of ' chan1.name]);
end


