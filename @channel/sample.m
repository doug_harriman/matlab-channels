% channel/sample  Returns a channels sample attribute
%

function out = sample(chan)

out = chan.sample ;

if ~isreal(out),
   out = imag(out) ;
end
