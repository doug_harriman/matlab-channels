% channel/end  length of channel data
%

% DLH
% 11/21/03 - Created

function out = end(in,varargin)

% Take max
out = numel(in.data) ;