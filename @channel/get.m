% @channel/get
% Gets a channel property
% Syntax: value = get(channel,property)
% Valid properties:
%    'Name'
%    'Data'
%    'Units'
%    'SampleTime'
%    'SampleRate'
%    'Points'     - Number of data points.
%    'Time'       - Time vector.
%    'Freqs'      - Frequencies for freqency data only.
%    'UserData'
%    'MetaData'

function out = get(in,prop)

% Error check input
if nargin ~= 2
    error('Must have a channel and one property')
end

% Switch for all the things you can change
switch lower(prop)

    case 'name'
        out = in.name ;

    case 'data'
        out = in.data ;

    case 'units'

        % Try to get from data, otherwise use field.
        if isa(in.data,'unit'),
            out = char(in.data);
        else
            out = in.units ;
        end

    case 'sampletime'
        out = in.sample ;
        out = unit(out,'sec');

    case 'samplerate'
        if length(in.sample) == 1
            out = inv(in.sample) ;
        else
            out = 0;
        end

    case 'points'
        out = length(in.data) ;

    case 'time'
        if length(in.sample) == 1,
            out = 0:in.sample:in.sample*(length(in.data)-1);
            out = out';
        else
            out = in.sample;
        end

    case 'freqs'
        out = imag(in.sample) ;

    case 'plotcolor'
        out = in.plot_color;

    case 'userdata'
        out = in.userdata;

    case 'metadata'
        out = in.metadata;

    case 'toffset'
        out = in.sample(1);
        
    otherwise
        disp(['Unknown channel property: ''' prop '''']) ;
end
