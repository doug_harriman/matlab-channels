%FIXEDSAMPLE  Converts irregular sample times to fixed sample rate.
%  FIXEDSAMPLE attemps to estimate the true sample rate of an irregular
%  sample time channel.
%


% Doug Harriman

function [out] = fixedsample(in)

% Error checks
error(nargchk(1,1,nargin));

t = get(in,'Time');
dt = diff(t);
f  = 1./dt;
f  = round(f);
f_est = mode(f);

out = in;
out.sample = 1/f_est;