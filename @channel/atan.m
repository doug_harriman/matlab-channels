% channel/atan  Arctangent of channel data
%

function out = atan(in)

% Take acos
out      = in ;
out.data = atan(in.data) ;
out.name = ['Arctangent of ' in.name] ;
out.units = 'Radians' ;
out.test = in.test ;
