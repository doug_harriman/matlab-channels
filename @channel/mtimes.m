% channel/mtimes  Channel multiplication
%

function out = mtimes(chan1,chan2)

% See if they are channels or numbers,
if isa(chan1,'channel') && isa(chan2,'channel')
   % Error check
   if xor(isreal(chan1.sample),isreal(chan2.sample))
      % Mixed time/frequency data
      error('Mixed Time/Frequency Data')
   end
   if min(chan1.sample - chan2.sample)
      % Resample
      disp('Resampling Channel 2')
      chan2 = resample(chan2,chan1.sample) ;   
   end
   
   % Create output object
   out = channel ;
   
   % Math
   out.data = chan1.data .* chan2.data  ;
   
   % Rename 
   out.name = [chan1.name '*' chan2.name] ;   
   
   % Sample
   out.sample = chan1.sample ;
   
   % Unit modification
   if ~strcmp(chan1.units,'') && ~strcmp(chan2.units,'')
      out.units = [chan1.units '*' chan2.units] ;
   end
   
   % Test
   out.metadata = chan1.metadata;

elseif isa(chan1,'channel') && ~isa(chan2,'channel')
   
   % Create output object
   out = chan1 ;
   
   % See if we have a scalar or vector of same length
   if length(chan1.data)==length(chan2)
	   % Do vector math
      out.data = chan1.data .* chan2 ;
      
   	% Don't Rename   
   else
	   % Do scalar math
      out.data = chan1.data * chan2(1) ;
      
      % Rename 
	   out.name = [chan1.name '*' num2str(chan2)] ;   
   end
      

elseif ~isa(chan1,'channel') && isa(chan2,'channel')
   
   % Create output object
   out = chan2 ;
   
   % Do math
   out.data = chan1 * chan2.data ;
   
   % Rename 
   out.name = [num2str(chan1) '*' chan2.name] ;   
   
else
   error('Unknown object pairing')

end

% Check units
if isa(out.data,'unit')
   out.units = char(out.data);
end

