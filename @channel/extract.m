%EXTRACT  Extract the subset of channel data between two time points.
%  CH_NEW = EXTRACT(CHAN,TIME_1,TIME_2) creates a new channel CH_NEW with
%  the data from CHAN between the two time points.
%

% Doug Harriman (doug.harriman@gmail.com)
% 11-Feb-09 - Created.

function [new] = extract(obj,t1,t2)

% Error checks
error(nargchk(nargin,3,3));

% Get channel data
time = get(obj,'Time');
data = get(obj,'Data');

% Subset indecies
idx = (time >= t1) & (time <= t2);

% Data subset.
time = time(idx);
data = data(idx);

% New channel
new = channel(['Subset of ' get(obj,'Name')],data,time);
