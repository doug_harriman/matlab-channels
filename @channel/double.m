% channel/double Channel Object to double converter
%

function data = double(chan)

% Make sure we have a channel
if isa(chan,'channel'),
   % Convert it
   data = double(chan.data) ;
else
   % Error
   disp('Not a channel object') ;
end

data = reshape(data,length(data),1);