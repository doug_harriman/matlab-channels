%@CHANNEL/LOADOBJ  Loads CHANNEL objects and updates to most recent definition.
%

% DLH
% 11/15/02 - Created

function out = loadobj(in)

% Early bailout
if strcmp(class(in),'channel'),
    out = in;
    return;
end


% OK, if we got here we must have a somewhat older version 
% of a class object.  As of 11/15/02, there is only 1 new
% field added to the definition: plot_color.
% Check for requisite fields, then add this one.

% Make sure the struct has a name, data & sample time field,
% otherwise it isn't a valid CHANNEL object.
if ~isfield(in,'name'), error('Input object has no NAME field.'); end
if ~isfield(in,'data'), error('Input object has no DATA field.'); end
if ~isfield(in,'sample'), error('Input object has no SAMPLE field.'); end
    

% OK looks like a valid CHANNEL object, so add the missing fields
if ~isfield(in,'plot_color'),
    in.plot_color = 'b';

    % Go ahead an assign 'Commaned' named signals a color of red.
    if ~isempty(findstr(in.name,'Commanded'))
        in.plot_color = 'r';
    end
end

% Extension fields
if ~isfield(in,'userdata')
    in.userdata = [];
end
if ~isfield(in,'metadata')
    in.metadata = [];
end

% Conversion to new metadata.
if ~isfield(in,'chanmeta')
    in.chanmeta = chanmeta;
    
    % Conversion.
    if isfield(in,'metadata')
        % Copy pertinent data over.
        if isfield(in.metadata,'file_name')
           [path,file,ext] = fileparts(in.metadata.file_name);
           in.chanmeta.sourcefile = [file ext];
           in.chanmeta.sourcepath = path;
           in.chanmeta.project    = setproj;
        end
    end
end

% Obsolete fields
if isfield(in,'axis')
    in=rmfield(in,'axis');
end
if isfield(in,'test')
    % Convert test data to straight metadata.
    in.metadata=in.test;
    in=rmfield(in,'test');
end

% Assign the class to the object and exit.
out = class(in,'channel');