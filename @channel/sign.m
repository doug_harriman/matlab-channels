%CHANNEL/SIGN  Sign of channel data.
%

function out = sign(in)

% Take abs
out      = in ;
out.data = sign(in.data) ;
out.name = ['Sign of ' in.name] ;