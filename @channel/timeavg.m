%TIMEAVG  Trailing average of a signal for a fixed sample rate or sample time.
%  CH2=TIMEAVG(CH1,SAMPLE) averages the data in CH1 to the sample rate or
%  samlpe time SAMPLE, and provides a new, downsampled channel CH2.
%
%  This function is different than RESAMPLE, as RESAMPLE does a linear
%  interpolation of the data, not averaging, for downsampling.

function ch2 = timeavg(ch1,sample)

% Input checks
error(nargchk(2,2,nargin));

% Type checks
if ~isa(ch1,'channel')
    error('Channel input expected.');
end
if ~isa(sample,'unit')
    error('SAMPLE must be a unit object.');
end
if ~isconsistent(sample,unit(1,'sec')) && ~isconsistent(sample,unit(1,'Hz'))
    error('SAMPLE must have units of either time or frequency.');
end

% Get new sample time.
if ~isconsistent(sample,unit(1,'sec'))
    % Must have frequency units.
    sample = 1/sample;
end
sample = convert(sample,'sec');
sample = double(sample);

% Verify that new sample time is greater than max of old sample time.
t_old  = get(ch1,'time');
dt_old = diff(t_old);
if sample < max(dt_old)
    error(['TIMEAVG supports downsampling only.  ' ...
        'Check minimum sample rate of input data.']);
end

% Need to resample input data to fixed period.  The fixed period must be
% such that the new sample rate is an integer multiple of it.
ts = min(dt_old);
ratio = round(sample/ts);  
ts_new = sample/ratio;

% Resample the input data.
ch1_resample = resample(ch1,unit(ts_new,'sec'));

% Average the data
ch1_avg = moveave(ch1_resample,ratio,'trailing');

% Extract data points of interest.
time = get(ch1_avg,'Time');
data = get(ch1_avg,'Data');

% Extract points of interest.
idx  = [ratio:ratio:length(data)];
data = double(data(idx));
ch2  = ch1;
ch2.sample = time(idx) + time(2);
ch2.data   = data;

