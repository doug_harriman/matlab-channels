%CHANNEL/IMAG  Return the imaginary part of a channel.
%

% DLH, 11/08/98

function out = imag(in)

out = in;
out.data = imag(in.data);
out.name = ['Imaginary part of ' in.name];