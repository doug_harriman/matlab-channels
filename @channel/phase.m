% channel/phase Phase of channel
% chan = phaseshift(chan) 

function chanout = phase(chan)

% Do FFT's
c1 = fft_t(chan) ;

% Calculate phase angles
phase1 = atan(imag(c1.data)./real(c1.data)) ;

% Create output
chanout = channel ;
chanout.name = ['Phase of ' chan.name ] ;
chanout.data = phase1 ;
chanout.sample = c1.sample ;

% Plot if no output
if nargout == 0,
   figure ;
   plot(chanout) ;
end
