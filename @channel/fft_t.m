% channel/fft_t  FFT of channel
%

function chanout = fft_t(chan)

[c,w] = fft_t(sampvec(chan),data(chan)) ;

% Create output channel
chanout = channel ;
chanout.name = ['FFT of ' chan.name] ;
chanout.data = c ;
chanout.sample = w'*i ;

