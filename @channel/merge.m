%MERGE Merges multiple channels into one.
%

% DLH 
% 05/12/05 - Created

function [out] = merge(varargin)

name = 'Merge of';
data = [];

for i = 1:nargin,
    chan = varargin{i};
    name = [name ' ' get(chan,'Name')];

    t = get(chan,'Time');
    d = get(chan,'Data');
    
    data = [data;[t d]];
end

data = sortrows(data);

out = channel(name,data(:,2),data(:,1));
