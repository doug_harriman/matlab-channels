% channel/max  max of channel data
%

% DLH

function out = max(in)

% Take max
out = max(in.data) ;