% channel/name Returns or sets a 
% channel's name attribute
%

function out = name(chan,str)

if nargin == 1,
   out = chan.name ;
else
   chan.name = str ;
   out = chan ;
end
