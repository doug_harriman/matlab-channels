% channel/mrdivide   Channel division
%

function out = mrdivide(chan1,chan2)

% See if they are channels or numbers,
if isa(chan1,'channel') & isa(chan2,'channel'),
   % Error check
   if xor(isreal(chan1.sample),isreal(chan2.sample)),
      % Mixed time/frequency data
      error('Mixed Time/Frequency Data')
   end
   if min(chan1.sample - chan2.sample)
      % Resample
      disp('Resampling Channel 2')
      chan2 = resample(chan2,chan1.sample) ;   
   end
   
   % Create output object
   out = channel ;
   
   % Math
   out.data = chan1.data ./ chan2.data  ;
   
   % Rename 
   out.name = [chan1.name '/' chan2.name] ;   
   
   % Unit modification
	if ~strcmp(chan1.units,'') & ~strcmp(chan2.units,'')
	   out.units = [chan1.units '/' chan2.units] ;
	end
   
   % Sample
   out.sample = chan1.sample ;
   
elseif isa(chan1,'channel') & ~isa(chan2,'channel'),
   
   % Create output object
   out = chan1 ;
   
   % Do math
   out.data = chan1.data ./ chan2 ;
   
   % Rename 
   out.name = [chan1.name '/' num2str(chan2)] ;   

elseif ~isa(chan1,'channel') & isa(chan2,'channel'),
   
   % Create output object
   out = chan2 ;
   
   % Do math
   out.data = chan1 ./ chan2.data ;
   
   % Rename 
   out.name = [num2str(chan1) '/' chan2.name] ;   
   
else,
   error('Unknown object pairing')

end


