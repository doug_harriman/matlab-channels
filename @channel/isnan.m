%ISNAN  ISNAN for Channel objects.
%

% DLH
% 06/18/04

function [out] = isnan(ch)

out = isnan(double(ch));