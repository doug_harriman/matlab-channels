%ATAN2  Four quadrant inverse tangent for channel objects.
%  THETA=ATAN2(Y,X)
%
% See also: ATAN2
%

function theta = atan2(y,x)

narginchk(2,2);

% Validate channels.
if length(x) ~= length(y)
    error('Channels must be same length');
end

% Create channels.
ts = get(x,'SampleTime');
th = atan2(double(y),double(x));
theta = channel(['Arctan of (' x.name ',' y.name ')'],th, ts,'rad');
