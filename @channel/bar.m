%@CHANNEL/BAR Channel Bar Plot
%
% >> bar(channel)
%

% DLH
% 12/17/02 - Created

function [] = bar(ch)

% Do the plot
data = get(ch,'data');
time = get(ch,'time');
bar(time,data);

% Labels
xlabel('Time [sec]');
ylabel([get(ch,'Name') ' [' get(ch,'Units') ']']);
pt