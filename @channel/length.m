% @channel/length  This function returns the number of data
%                  points stored in a channel.  It is the same
%                  as: >> get(channel,'points')

function pts = length(chan)

pts = length(chan.data);