% channel/exp  Exponential of channel data
%

function out = exp(in)

% Take abs
out      = in ;
out.data = exp(in.data) ;
out.name = ['exp of ' in.name] ;