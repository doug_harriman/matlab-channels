% @channel/channel.m   Channel Class Constructor
% Creates an object of type channel.
% Synax:
%        chan = channel(name,data,sample_time,units,param,value)
%

% DLH
% 12/17/04 - Got rid of 'test', added 'userdata' and 'metadata'.
%            Allow setting of multipl properties
% 11/15/02 - Updated to add plot color to object.

% Note: If adding a field to the class definition, add to end of field list and update
%       @channel/loadobj to provide backwards compatibility.

function out = channel(varargin)

if nargin >= 3
    % Get empty object
    out = channel;

    % Have full description
    out.name   = varargin{1} ;

    data = varargin{2} ;
    time = varargin{3};

    % Handle mismatched time/data vector lengths
    if length(time) > 1
        len = min(length(time),length(data));
        time = time(1:len);
        data = data(1:len);
    end
    
    % Make sure data is a column vector
    if ~isvector(data)
        error('Data must be a vector.');
    end
    data = reshape(data,numel(data),1);
    out.data = data ;

    % Sample rate
    % Handled united time data
    if isa(time,'unit')
        time = convert(time,'sec');
        time = double(time);
    end
    time = reshape(time,numel(time),1);
    
    
    out.sample = time;

    if nargin >= 4
        if haveunitclass
            out.units  = varargin{4} ;
            if ~isempty(deblank(out.units));
              % Cleanups.
              str = out.units;
              str = strrep(str,'\cdot','*');  % LaTeX cdot
              out.data = unit(out.data,str);
            end
        else
            out.units  = varargin{4} ;
        end
    else
        % See if data was a unit
        if isa(out.data,'unit'),
            out.units = char(out.data);
        end
    end

    % Handle additional param/value pairs
    if length(varargin) > 4
        % Have additional args.
        % Make sure have pairs.
        if isodd(nargin)
            error('Parameter/value inputs must come in pairs.');
        end
        
        % Loop through additional inputs
        nParam = (nargin-4)/2;
        for iParam = 1:nParam,
            param = varargin{2*iParam + 3};
            value = varargin{2*iParam + 4};
            
            switch(lower(param)),
                case 'plotcolor',
                    out.plot_color = value;
                case 'userdata',
                    out.userdata = value;
                case 'metadata',
                    out.metadata = value;
                otherwise,
                    error(['Unknown parameter: ' upper(param)]);
            end
        end
    end

else
    out.name   = '' ;
    out.data   = []  ;
    out.sample = []  ;
    out.units  = '' ;
    out.plot_color = 'b';
    out.userdata = [];
    out.metadata = chanmeta;
end

% Give it some class
out = struct(out);
out = class(out,'channel' ) ;
superiorto('unit');