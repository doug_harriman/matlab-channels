%MTIMEAVG  Multiple time averaging for Channel data.
%  [AVG,TIME_WINDOW_SIZE]=MTIMEAVG(CHAN,NPTS,TMAX) averages CHAN data over NPTS 
%  logrithmically spaced time window bins ranging from the sample time to 
%  the TMAX.  If TMAX is not specified, the maximum data time will be
%  used.
%

% Doug Harriman (doug.harriman@gmail.com)

function [avg,t_bins] = mtimeavg(obj,npts,tmax)

% Error checks
error(nargchk(1,3,nargin));
if nargin < 2
    npts = 10;  % Default
end
if ~isint(npts) || ~isscalar(npts)
    error('NPTS must be a scalar integer.');
end

% Only support fixed sample times.
ts = get(obj,'SampleTime');
if ts == 0
    error('MTIMEAVG only supports fixed sample time channels.');
end

% Bin the time slices in time.
data = get(obj,'Data');
if nargin < 3
    % Default tmax
    tmax = ts*numel(data);
end
    
t_bins = logspace(log10(ts),log10(tmax),npts);

% Convert bins to samples
s_bins = round(t_bins/ts);
t_bins = s_bins*ts;

% Calc the time averages.
avg = zeros(npts,1);
for i = 1:npts
    if s_bins(i) == 1
        avg(i) = max(obj);
    elseif s_bins(i) == length(data)
        avg(i) = mean(data);
    else
        avg(i) = max(moveave(obj,s_bins(i)));
    end
end

% Only plot if no outputs.
if nargout > 0
    return;
end

% Output
h = plot(t_bins,avg,'.-');
set(h,'DisplayName',get(obj,'Name'));
ylabel({'Windowed time average of ',...
    [strrep(get(obj,'Name'),'_',' ') ' [' get(obj,'Units') ']']});
xlabel('Averaging Window Size [sec]');
set(gca,'XScale','log');
pt;
