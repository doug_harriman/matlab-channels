%CHANNEL/SAMPVEC  Provides sample time or frequency data vector.
% 
% Always provides real valued data.
%

% DLH, 11/10/98

function vec = sampvec(chan)

% Get sample data
x    = chan.sample ;

% See if we need to generate data
if length(x) == 1,
   x = abs(x);
   vec = [0:x:x*(length(chan.data)-1)]' ;
else
   vec = x ;
end

% See if we need to add units to vec
if isa(chan.data,'unit')
   % Add units to the vec   
   if ~isreal(vec),
      vec = unit(vec,'Hz');
   else
      vec = unit(vec,'sec');
   end
end
