% channel/uminus  Unary minus of a channel
%

function in = uminus(in)

% Negate data
in.data = -in.data ;