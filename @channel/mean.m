%CHANNEL/MEAN  Mean of channel data.
%

function out = mean(in)

% Get mean
out = mean(in.data) ;