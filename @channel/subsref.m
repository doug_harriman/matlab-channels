% channel/subsref  Channel subscript reference
%

function chan = subsref(chan,sub)

% Convert sub struct
sub = sub.subs{:} ;

if length(sub) > 1,
   
   % Get data
   chan.data = chan.data(sub) ;
%   chan.name = ['Subset of ' chan.name] ;
   
   % Sample subsetting
   if length(chan.sample) > 1,
      chan.sample = chan.sample(sub) ;
   end
else
   chan = chan.data(sub);
end
