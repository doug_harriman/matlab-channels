% @channel/std  Standard deviation of channel data.
%

function out = std(in)

% Take std
out = std(double(in.data)) ;