% channel/sin  Sine of channel data
%

function out = sin(in)

% Take sin
in = convert(in,'rad');
out      = in ;
out.data = sin(in.data) ;
out.name = ['Sine of ' in.name] ;
