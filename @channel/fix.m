%CHANNEL/FIX  Fix channel data
%

function out = fix(in)

% Take abs
out      = in ;
out.data = fix(double(in.data)) ;
