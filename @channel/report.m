% @channel/report.m  Does a simple plot of a channel.
%   Plot includes signal, PSD of signal, and
%   associated labels. 

function [] = report(chan,varargin)

if nargin == 1,

% Load plot limits
fileName = 'report.ini';
str = ['load '  fileName ' -mat'];
if exist(fileName)==2,
   str = ['load '  fileName ' -mat'];
   eval(str);
   haveFile = 1;
else
   haveFile = 0;
   useLimits= 0;
end

% Open a new figure and do plots
figure;
subplot(2,1,1);
plot(chan);pt;
set(gca,'Tag','TimeAxis');
%if haveFile&useLimits, axis(time_axis_limits);end
%set(gca,'ylim',[-1 1]*.2);
stretch;
if nargin == 2,
   title([get(chan,'name') ': 'str]);   
else
   title(get(chan,'name'));
end

subplot(2,1,2);psd(chan);title('');grid;pt;
set(gca,'Tag','FrequencyAxis');
%if haveFile&useLimits, axis(frequency_axis_limits);end
ylabel('PSD');
xlabel('Frequency (Hz)');
%label(chan);
grid

% Add menu to figure
if 0
   reportMenu = uimenu(gcf,'Label','&Report');
uimenu(reportMenu,'Label','Save Limits',...
   'Callback','report(channel('''',[]),''saveaxes'');');
uimenu(reportMenu,'Label','Use Limits',...
   'Tag','UseLimitsMnu',...
   'Callback','report(channel('''',[]),''limittoggle'');');
end

% Store the name of the channel
setuprop(gcf,'ChannelName',inputname(1));


% Got a message
else,
   
   switch lower(varargin{1}),
      
   case 'saveaxes'
      % Save the current limits in a file
      handle = findobj(get(gcf,'children'),'Tag','TimeAxis');
      time_axis_limits = [get(handle,'xlim') get(handle,'ylim')];
      
      handle = findobj(get(gcf,'children'),'Tag','FrequencyAxis');
      frequency_axis_limits = [get(handle,'xlim') get(handle,'ylim')];
      
      save('report.ini','time_axis_limits','frequency_axis_limits','useLimits');
      
   case 'limittoggle'
      % Change the current state
      handle = gcbo;
      stateStr = get(handle,'Checked');
      if strcmp(stateStr,'On');
         set(handle,'Checked','Off');
         useLimits = 1;
         save report.ini time_axis_limits frequency_axis_limits useLimits
      else
         set(handle,'Checked','On');
         useLimits = 0;
         save report.ini time_axis_limits frequency_axis_limits useLimits
      end
      
   end
   
end
