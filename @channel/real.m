%CHANNEL/REAL  Return the real part of a channel.
%

% DLH, 11/08/98

function out = real(in)

out = in;
out.data = real(in.data);
out.name = ['Real part of ' in.name];