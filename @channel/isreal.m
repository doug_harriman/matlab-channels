%CHANNEL/ISREAL  isreal for channel data.
%

% DLH, 11/08/98

function bool = isreal(chan)

bool = isreal(chan.data);