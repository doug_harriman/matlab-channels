%HORZCAT  Concatenation of time based channels.
%  CHAN=HORZCAT(CHAN1,CHAN2) or CHAN=[CHAN1 CHAN2].  Data and time vectors
%  for CHAN are the concatenated data and time vectors of CHAN1 and CHAN2.
%
%  There are two valid cases of sample time relations:
%  1) CHAN1 and CHAN2 have a the same, fixed sample time.  Mismatched
%     sample times are not supported.  To concatenate channels with
%     different sample times, resample one of the channels with RESAMPLE so
%     that the sample times match.
%  2) CHAN1 and CHAN2 have a time vector with times that do not overlap.
%     Time and data will be simply concantenated.  Overlapping time vectors
%     will throw an error.
%
%  See also: channel/resample.
%

% Doug Harriman (doug.harriman@gmail.com)

function out = horzcat(chan1,chan2)

% Error check inputs
error(nargchk(nargin,2,2));
if ~isreal(chan1.sample) || ~isreal(chan2.sample)
    error('Concatenation only works for time based channels.');
end

% Make sure fixed time step channels have same sample rate.
if get(chan1,'SampleRate') ~= get(chan2,'SampleRate')
    error('Channels must have same sample rate. Try resampling with CHANNEL/RESAMPLE.');
end

% Now have either two fixed sample time channels with the same sample rate,
% or two with full time vectors.
t1 = get(chan1,'SampleTime');
t2 = get(chan2,'SampleTime');

% Time concat
if length(t1) == 1
    % Fixed sample rate;
    time = t1;
else
    % Time vectors.
    % Force column vectors.
    t1 = reshape(t1,numel(t1),1);
    t2 = reshape(t2,numel(t2),1);
    
    % Figure out which vector should come first.
    if t1(1) < t2(1)
        % t1 is first.  Make sure no overlap.
        if t1(end) > t2(1)
            error('Overlapping time vectors.');
        end
        
        time = [t1;t2];
    else
        % t2 is first.  Make sure no overlap.
        if t2(end) > t1(1)
            error('Overlapping time vectors.');
        end
        
        time = [t2;t1];
    end
end

% Data concat
data1 = get(chan1,'Data');
data2 = get(chan2,'Data');
data  = [reshape(data1,numel(data1),1); reshape(data2,numel(data2),1)];

% Build the new channel.
out = channel(['[' get(chan1,'Name') ', ' get(chan2,'Name')],...
    data,time);
