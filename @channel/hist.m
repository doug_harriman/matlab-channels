% channel/hist  Histogram of channel data.
%

function [n,x] = hist(in,buckets)

% Error checks & defaults
narginchk(1,2);
if nargin < 2 
   buckets = 10;
end

% Do histogram
[n,x]=hist(in.data,buckets) ;

if nargout > 0
    return;
end

% No output args, so do plot.
bar(x,n);
title(['Histogram of ' in.name]);
xlabel(['Value [' in.units ']']);
ylabel('Count');
pt;