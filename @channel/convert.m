%CHANNEL/CONVERT  Unit conversion of channel object.
%                 Requires Unit Management Toolbox.
%

% DLH
% 04/12/05 - Correctly handle case where channel object has units, but data
%            field is of class 'double' not 'unit'.
% 03/04/99 - Fixed () problem on conversion.

function out = convert(chan,unitStr)

% See if the toolbox exists
if haveunitclass
    % Make sure that data has units.
    if ~isa(chan.data,'unit')
        % See if we have units from the channel that we can apply.
        if ~isempty(chan.units),
            chan.data = unit(chan.data,chan.units);
        else
            error('Channel has no units.');
        end
    end

    chan.data = convert(chan.data,unitStr);
    chan.units = char(chan.data);
    if strcmp(chan.units(1),'('),
        chan.units = chan.units(2:length(chan.units)-1);
    end

    out = chan;
else
    error('Unit Management Toolbox required.');
end

% If no output, then assign the new units in callers
% workspace.  This saves typing.
if nargout==0
    % Change value in caller's workspace
    assignin('caller',inputname(1),out) ;
end