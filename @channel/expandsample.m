% channel/expandsample  Expands sample rate to time series.
%

function time = expandsample(chan)

% Error check
if ~isa(chan,'channel'),
   error('Argument must be a channel object') 
elseif ~isreal(chan.sample),
   error('Channel must be a time sequence')
end

% Convert
rate = (chan.sample) ;
time = [0:rate:rate*(length(chan.data)-1)] ;