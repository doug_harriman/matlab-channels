%CHANNEL/UNIQUE  Returns a vector of type double containing unique values.
%
%   [VALUES,INDEX]=UNIQUE(CHANNEL) returns the unique VALUES that exist in
%   CHANNEL.  The VALUES exist at the elements INDEX.
%

% DLH
% 08/30/04 - Created

function [val,index] = unique(chan)

data = double(chan);
[val,index] = unique(data);