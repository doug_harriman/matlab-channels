%CHANNEL/FFT  FFT of a channel.
% Syntax:
%   [fft_coeff] = fft(channel)
%
%   FFT length is the length of the channel. 
%

% DLH
% 11/08/98 - Created
% 06/15/01 - Updated to handle non-time based sampling

function chanout = fft(chan1,chan2) 

% Set chan1 time to chan2 data if non-time based
if nargin > 1,
    t = double(get(chan2,'data'));
    
    % Make sure increasing
    if t(end) < t(1),
        t = flipud(t);
    end
    
    % Start at zero
    t = t - t(1);
    
    chan1 = channel(get(chan1,'Name'),double(get(chan1,'data')),t);
end

% Resample if irregular time sequence
if length(chan1.sample) ~= 1,
   chan1 = resample(chan1,1./min(abs(diff(chan1.sample)))) ;
end

% generate time vector
t = get(chan1,'time');
data = double(get(chan1,'data'));

% Do the FFT
y = fft(data) ;  
x = [0:length(t)-1]/max(t);  % Frequencies
ind = round(length(x)/2) ;  % First half of the data set
x = x(1:ind) ;  % First half of frequencies
dw = diff(x);
dw = dw(1);

% Create output chan1nel
chanout = channel(['FFT of ' chan1.name],y(1:ind),dw'*i) ;
