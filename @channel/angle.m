%CHANNEL/ANGLE  Return the phase angle of a channel.
%

% DLH, 11/08/98

function out = angle(in)

out = in;
out.data = angle(in.data);
out.name = ['Phase angle of ' in.name];
out.units = 'rad';