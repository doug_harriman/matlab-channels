%@channel/sound  Plays channel as a sound at the given sample rate.

function [] = sound(in)

% Play the sound
disp(['Playing: ' get(in,'name')]);
sound(get(in,'data'),get(in,'SampleRate'));