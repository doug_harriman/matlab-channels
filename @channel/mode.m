%CHANNEL/MODE  Mode of channel data.
%

function out = mode(in)

% Get mean
out = mode(double(in.data)) ;