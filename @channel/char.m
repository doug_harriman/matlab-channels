% channel/char Channel Object to character converter
%

function data = char(varargin)

% Call recursively to handle multiple args
if nargin>1,
    for i = 1:nargin,
        data{i} = char(varargin{i});
    end
    return;
end

% Make sure we have a channel
chan = varargin{1};
if isa(chan,'channel'),
   % Convert it
   data = chan.name ;
else
   % Error
   disp('Not a channel object') ;
end
