%CHANNEL/ABS  Absolute value of channel data.
%

function out = abs(in)

% Take abs
out      = in ;
out.data = abs(in.data) ;
out.name = ['ABS of ' in.name] ;