% channel/mpower Raised a channels data to a power
% channel_out = power(channel,exponent)
% 

function out = mpower(in1,in2)

% Now simple divide the names and data
out = channel([in1.name '^' num2str(in2)],...
   in1.data .^ in2,in1.sample) ;

% Unit modification
if ~strcmp(in1.units,''),
   out.units = [in1.units '^' num2str(in2)] ;
end
