% channel/sqrt Square root of channel data 
% 

function out = sqrt(in1)

out = in1^0.5;