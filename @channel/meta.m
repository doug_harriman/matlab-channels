%CHANNEL/META  Return or display channel metadata object.
%

% Doug Harriman

function [obj] = meta(chan)

% Output requested
if nargout > 0
    obj = chan.metadata;
    return;
end

% Just display
display(chan.metadata);