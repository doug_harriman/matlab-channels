% channel/timeshift Time shift of two signals
% chan = timeshift(chan1,chan2) 

function chanout = timeshift(chan1,chan2)

% Make sure that channels are on same sample time
if ~min(chan1.sample == chan2.sample)
   error('Channel must have same sample parameter')
end

% Do FFT's
c1 = fft_t(chan1) ;
c2 = fft_t(chan2) ;

% Calculate phase angles
phase1 = atan(imag(c1.data)./real(c1.data)) ;
phase2 = atan(imag(c2.data)./real(c2.data)) ;

% Calculate phase difference
delta = phase1 - phase2 ;

% Calculate time shift
delta_t = delta./(2*pi*imag(c1.sample)) ;

% Create output
chanout = channel ;
chanout.name = ['Time shift between ' chan1.name ' and ' chan2.name] ;
chanout.data = delta_t ;
chanout.sample = c1.sample ;

% Plot if no output
if nargout == 0,
   figure ;
   plot(chanout) ;
end
