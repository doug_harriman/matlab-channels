% channel/bobfilt  Channel version of Bob C's Wonder Filter
%

function chan = bobfilt(chan,coeff) 

% Error check
if ~isreal(chan.sample) | length(chan.sample) ~= 1,
   error('bobfilt for channels works only on sampled time data');
end

% Pass data to filter
if nargin == 1,
   chan.data = bobfilt(chan.data) ;
else,
   chan.data = bobfilt(chan.data,coeff) ;
end

if nargout==0
	% Change value in caller's workspace
   assignin('caller',inputname(1),chan) ;
end