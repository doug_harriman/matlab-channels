% channel/tan  Tangent of channel data
%

function out = tan(in)

% Take acos
out      = in ;
out.data = tan(in.data) ;
out.name = ['Tangent of ' in.name] ;
