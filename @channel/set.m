% @channel/set
% Sets a channel property
% Syntax: set(channel,property1,value1,...)
% Valid properties:
%    'Name'
%    'Data'
%    'Units'
%    'SampleTime'
%    'SampleRate'
%    'TOffset'     - Time offset
%    'PlotColor'
%    'UserData'
%    'MetaData'

% TODO - Support numeric color descriptions for plotcolor.
%        To corrosponding changes to channel/plot.
% TODO - Change 'PlotColor' to 'Color' to match PLOT format.

% DLH
% Changelog
% 12/17/04 - Removed test, axis.  Added userdata, metadata
% 01/03/03 - Added setting of time vector.

function [out] = set(in,varargin)

% Error check input
if mod(length(varargin),2) ~= 0
    % Wrong number of property/value pairs
    error('Wrong number of inputs, must have property/value pairs')
elseif nargin < 3
    error('Must have a channel and at least one property/value pair')
end

% Copy output into input
out = in ;

% Loop through pairs
for i = 1:length(varargin)/2
    % Calculate property(i) index
    ind = (i-1)*2+1 ;

    % Switch for all the things you can change
    switch lower(varargin{ind})

        case 'name'
            out.name = varargin{ind+1} ;

        case 'data'
            out.data = varargin{ind+1} ;
            if strcmp(class(out.data),'unit'),
                out.units = char(out.data);
            end

        case 'units'
            out.units = varargin{ind+1} ;
            if strcmp(class(out.data),'unit')
                out.data = convert(out.data,varargin{ind+1});
            else
                out.data = unit(out.data,varargin{ind+1});
            end

        case 'sampletime'
            out = resample(in,inv(varargin{ind+1})) ;

        case 'samplerate'
            out = resample(in,(varargin{ind+1})) ;

        case 'plotcolor'
            if ~ischar(varargin{ind+1})
%                 error('Channel default plot color only supports string color definitions.');
            end
            out.plot_color = varargin{ind+1};

        case 'time'
            out.sample = varargin{ind+1};

        case 'userdata'
            out.userdata = varargin{ind+1};
            
        case 'metadata'
            out.metadata = varargin{ind+1};

        case 'chanmeta'
            out.metadata = varargin{ind+1};

        case 'toffset'
            % Set offset to 0, then to the new value.
            if isscalar(in.sample)
                t = get(in,'time');
                t = t + varargin{ind+1};
                out = set(in,'time',t);
            else
                in.sample = in.sample - in.sample(1);
                out.sample = in.sample + varargin{ind+1};
            end
            
        otherwise
            error(['CHANNEL objects have no property: ' upper(varargin{ind})]);

    end

end

% Change value in caller's workspace
if nargout == 0
    assignin('caller',inputname(1),out) ;
end