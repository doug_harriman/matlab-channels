% channel/units  Gets/Sets units of a channel
% string = units(channel)  Returns units.
% channel = units(channel,string)  Sets units.
%

function out = units(chan,str)

% 1 input -> return units
if nargin == 1,
   out = chan.units ;
elseif nargin == 2,
   out = chan ;
   out.units = str ;
else,
   error('Illegal input to channel/units')
end
