% channel/exp  Exponential of channel data
%

function out = round(in)

% Take abs
out      = in ;
out.data = round(in.data) ;
