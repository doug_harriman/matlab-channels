%CHANNEL/MEDIAN Median of channel data.
%

function out = median(in)

% Get median
out = median(in.data) ;