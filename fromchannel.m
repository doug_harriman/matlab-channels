%FROMCHANNEL  Converts channel object to Simulink signal source.
%
%

% DLH 7/5/98

function [sys,x0,str,ts] = FromChannel(t,x,u,flag,chanName,source)

switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
     % System sizes
     sizes = simsizes;
     sizes.NumContStates  = 0;
     sizes.NumDiscStates  = 0;
     sizes.NumOutputs     = 1;
     sizes.NumInputs      = 0;
     sizes.DirFeedthrough = 0;
     sizes.NumSampleTimes = 1;   % at least one sample time is needed
     sys = simsizes(sizes);
     
     % Initial conditions
     x0  = [];
     
     % str is always an empty matrix
     str = [];
     
     % Make sure the names match
     set_param(get_param(gcb,'Parent'),'Name',chanName);
     
     % Get data vector from the channel object
     switch source
     case 'Matlab Workspace'
        chan = evalin('base',chanName);
     case 'Channel Manager'
        chan = ChanMan('GetChan',chanName);
     end
     data = double(get(chan,'Data'));
     sampleRate = get(chan,'SampleRate');
     set_param(gcb,'UserData',{sampleRate data});
     
     % Initialize the array of sample times
     ts  = [inv(sampleRate) 0];
     
     % Make sure we don't try to simulate for longer than
     % we have data for.
     parent = get_param(gcb,'Parent');
     parent = parent(1:find(parent=='/')-1);
     maxTime = (length(data)-1)/sampleRate;
     if get_param(parent,'StopTime') > maxTime,
        set_param(parent,'StopTime',num2str(maxTime));
     end
     
     % Make sure that if using a fixed-step solver that the simulation 
     % time step is 0.1 the sample time.
%     set_param(parent,'FixedStep',num2str(inv(sampleRate*10)));     
     
  case 2,
     sys = [];
     
  case 3,
     % Retrieve
     data = get_param(gcb,'UserData');
     sampleRate = data{1};
     data = data{2};
     sys  = data(round(t*sampleRate)+1);
     
  case 9,
     
     sys = [];     
     
  otherwise
    error(['Unhandled flag = ',num2str(flag)]);

end

% end FromChannel


