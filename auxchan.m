%AUXCHAN  Creates auxillary channel data from 
%         flight recorder data.
%

% DLH, 01/07/99

function [] = auxchan()

% Get Data that should be there
pos  = chanman('GetChan','Actual Position');
vel  = chanman('GetChan','Actual Velocity');
cpos = chanman('GetChan','Commanded Position');
cvel = chanman('GetChan','Commanded Velocity');
epos = chanman('GetChan','Position Error');
evel = chanman('GetChan','Velocity Error');

% See what we need to add, then add it 
if ~isa(vel,'channel')
   vel = diff(pos);
   vel = convert(vel,'IPS');
   set(vel,'Name','Actual Velocity');
   chanman('AddChannel',vel);
end

if ~isa(cvel,'channel')
   if isa(cpos,'channel'),
      cvel = diff(cpos);
      cvel = convert(cvel,'IPS');
      set(cvel,'Name','Commanded Velocity');
      chanman('AddChannel',cvel);
   end
end

if ~isa(epos,'channel')
   if isa(pos,'channel') & isa(cpos,'channel'),
      len = min(length(cpos),length(pos));
      cpos = cpos(1:len);
      pos  = pos(1:len);
      epos = cpos - pos;
      set(epos,'Name','Position Error');
      chanman('AddChannel',epos);
   end
end

if ~isa(evel,'channel')
   if isa(epos,'channel'),
      evel = diff(epos);
      evel = convert(evel,'IPS');
      set(evel,'Name','Velocity Error');
      chanman('AddChannel',evel);
   end
end

% Add signals we know aren't there
acc = diff(vel);
acc = bobfilt(acc);
set(acc,'Name','Actual Acceleration');
acc = convert(acc,'G');
chanman('AddChannel',acc);