%CHANMAN  Channel Object Manager UI.
%
% Messages:
%   create
%   resize
%   actionpush
%   import
%   importone
%   export
%   exportone
%   load
%   addchannel
%   channelselect
%   clearall
%   clearone
%   rename
%   edittest
%   reunit
%   reunitchannel
%
%  Messages with updated documentation:
%   ClearChannelSelectCallback    - Clear user defined callback for channel selection.
%   DeleteChannelByName           - Deletes the channel with the given name if it exists.
%   Exit                          - Shut down Channel Manager.
%   GetChannelAll                 - Returns a cell array of all of the channels.
%   GetChannelByName              - Returns the channel with the given name if it exists.
%   GetChannelSelected            - Returns the currently selected channel.
%   Map                           - Map the predicate function over all channels.
%                                   The predicate function takes the channel
%                                   as an input, and may return a channel. If the channel
%                                    has data, it will replace the original channel in the list.
%                                   The predication function call must look like:
%                                   fcn_name(arg1,...,ch,...,argN)
%                                   'ch' will be replaced with an actual channel when called.
%                                   The function must return a channel as it's default output.
%   Save                          - Saves channels to MAT file.  Will prompt for filename if not provided.
%   SetMessage                    - Sets status bar message.  Only callable from callbacks.
%   SetPlotFigure                 - Sets the figure to use for plotting.
%   SortChannels                  - Sorts channels by name.
%   RegisterChannelSelectCallback - Register a user defined callback for channel selection.
%
%  Depricated Messages
%   GetChan    - Use 'GetChannelByName'.
%   GetChannel - Use 'GetChannelSelected'.
%   DeleteChan - Use 'DeleteChannelByName'.
%
%  Old/Unused Messages
%   GetData                      - Retrieves data from VB.

% DLH

% To Do List:
% Allow nesting of plugin menus and plugin menus at top level.
% Allow drag of channels onto existing figures to add to plot.

% Changelog
% 01/03/03 - Fixed bug in exporting to workspace where not finding illegal symbols was crashing.

function [out] = chanman(msg,varargin)

% Flag for saves
persistent chanmanobj

% Make sure we get an input message
if nargin == 0,
    if isempty(chanmanhan),
        msg = 'create' ;
    else
        figure(chanmanhan);
        msg = 'null';
    end
end

% Drop case
msg = lower(msg);

% Don't allow any axes in window
ChanManWindow = chanmanhan;

switch msg

    % Window Creation
    case 'create'

        % Make sure we only open one window
        temphan = ChanManWindow ;
        if ~isempty(temphan),
            % Recall figure
            close(temphan),chanman
            figure(temphan) ;
        end

        if isempty(temphan)
            % Default figure position.
            pos = [];
            v = ver('Matlab');
            v = str2num(v.Version);
            if v>=7.6
                if ispref('CHANNELS','ChanManFigurePos')
                    pos_dict = getpref('CHANNELS','ChanManFigurePos');
                    screen   = get(0,'ScreenSize');

                    % Look up screen size in dict
                    pos = pos_dict(screen);
                else
                    addpref('CHANNELS','ChanManFigurePos',dict);
                end
            end
            if isempty(pos)
                pos = [798    64   441   290];
            end

            % Create Window
            Fig = figure('numbertitle','off',...
                'menubar','none',...
                'position',pos,...
                'tag','ChanManWindow',...
                'name','Channel Manager',...
                'DeleteFcn','chanman(''Exit'');',...
                'resizefcn','chanman(''Resize'');');

            % Create User Properties
            setappdata(Fig,'Data',[]) ;
            setappdata(Fig,'PlotFig',[]) ;
            setappdata(Fig,'StringWidths',zeros(1,5)) ;

            % Window sizes
            window = get(gcf,'position') ;
            window_w = window(3) ;
            window_h = window(4) ;

            % Create window objects
            % Selection Action Frame
            x = 3 ;      y = 3 ;
            frame_h = 127 ;
            uicontrol('tag','frmSelect',...
                'style','frame',...
                'position',[x (window_h-frame_h-y) 135 frame_h]) ;
            uicontrol('tag','txtSelect',...
                'string','Select Action',...
                'style','text',...
                'horizontalalignment','left',...
                'position',[x+5 (window_h-20-y) 65 18]) ;
            uicontrol('tag','rbActionNone',...
                'string','None',...
                'style','radio',...
                'value',1,...
                'callback','chanman(''ActionPush'');',...
                'horizontalalignment','left',...
                'position',[x+10 (window_h-40-y) 45 18]) ;
            uicontrol('tag','rbActionPlot',...
                'string','Plot',...
                'style','radio',...
                'value',0,...
                'callback','chanman(''ActionPush'');',...
                'horizontalalignment','left',...
                'position',[x+10 (window_h-60-y) 45 18]) ;
            uicontrol('tag','rbActionDragPlot',...
                'string','Drag to Plot',...
                'style','radio',...
                'value',0,...
                'callback','chanman(''ActionPush'');',...
                'horizontalalignment','left',...
                'position',[x+10 (window_h-80-y) 90 18]) ;
            uicontrol('tag','rbActionEvalCmd',...
                'string','Eval Command:',...
                'style','radio',...
                'value',0,...
                'callback','chanman(''ActionPush'');',...
                'horizontalalignment','left',...
                'position',[x+10 (window_h-100-y) 90 18]) ;
            uicontrol('tag','chkActionPlot',...
                'string','New Figure',...
                'style','check',...
                'value',0,...
                'enable','off',...
                'horizontalalignment','left',...
                'position',[x+55 (window_h-60-y) 75 18]) ;
            uicontrol('tag','edtActionEvalCmd',...
                'string',' ',...
                'style','edit',...
                'enable','off',...
                'horizontalalignment','left',...
                'position',[x+25 (window_h-122-y) 105 22]) ;

            % Workspace Transfer Frame
            x = 3 ;      y = 125 ;
            frame_w = 135 ;      frame_h = 72 ;
            uicontrol('tag','frmWorkspace',...
                'style','frame',...
                'position',[x (window_h-frame_h-y) frame_w frame_h]) ;
            uicontrol('tag','txtWorkspace',...
                'string','Workspace Transfer',...
                'style','text',...
                'horizontalalignment','left',...
                'position',[x+5 (window_h-20-y) 105 18]) ;
            uicontrol('tag','txtExport',...
                'string','Export:',...
                'style','text',...
                'horizontalalignment','left',...
                'position',[x+10 (window_h-43-y) 40 18]) ;
            uicontrol('tag','edtExport',...
                'backgroundcolor',[1 1 1],...
                'string',' ',...
                'callback','chanman(''Export'');',...
                'style','edit',...
                'horizontalalignment','left',...
                'position',[x+47 (window_h-43-y) (frame_w-52) 22]) ;
            uicontrol('tag','txtImport',...
                'string','Import:',...
                'style','text',...
                'horizontalalignment','left',...
                'position',[x+10 (window_h-68-y) 40 18]) ;
            uicontrol('tag','edtImport',...
                'backgroundcolor',[1 1 1],...
                'callback','chanman(''Import'');',...
                'string',' ',...
                'style','edit',...
                'horizontalalignment','left',...
                'position',[x+47 (window_h-68-y) (frame_w-52) 22]) ;

            % Clear Frame
            x = 3 ;      y = 212 ;
            frame_w = 135 ;      frame_h = 68 ;
            uicontrol('tag','frmClear',...
                'style','frame',...
                'position',[x (window_h-frame_h-y) frame_w frame_h]) ;
            uicontrol('tag','btnClearOne',...
                'string','Clear',...
                'callback','chanman(''ClearOne'');',...
                'style','pushbutton',...
                'horizontalalignment','center',...
                'position',[x+5 (window_h-30-y) (frame_w-11) 25]) ;
            uicontrol('tag','btnClearAll',...
                'string','Clear All',...
                'callback','chanman(''ClearAll'');',...
                'style','pushbutton',...
                'horizontalalignment','center',...
                'position',[x+5 (window_h-60-y) (frame_w-11) 25]) ;

            % Statusbar Frame
            x = 3 ;      y = 1 ;
            frame_w = window_w-3 ;      frame_h = 24 ;
            uicontrol('tag','frmStatusBar',...
                'style','frame',...
                'position',[x y frame_w frame_h]) ;
            uiHandle = uicontrol('tag','txtStatusBar',...
                'string','Ready',...
                'style','text',...
                'horizontalalignment','left',...
                'position',[x+3 (y+2) (frame_w-10) 18]) ;

            % Channel List Frame
            x = 140 ;         y = 260 ;
            frame_w = 300 ;   frame_h = 257 ;
            uicontrol('tag','frmChannelList',...
                'style','frame',...
                'position',[x (window_h-y) frame_w frame_h]) ;
            uicontrol('tag','lstChannelList',...
                'style','listbox',...
                'string','Empty',...
                'fontname','courier',...
                'Callback','chanman(''ChannelSelect'');',...
                'ButtonDownFcn','chanman(''ChannelRightSelect'');',...
                'position',[x+2 (window_h-y+2) (frame_w-4) (frame_h-24)]) ;
            uicontrol('tag','txtChannelList',...
                'string','Name',...
                'style','text',...
                'fontname','courier',...
                'horizontalalignment','left',...
                'position',[x+5 (window_h-y+235) (frame_w-10) 16]) ;

            % Menus
            % File Menu
            mnuFile = uimenu('label','&File') ;
            uimenu('parent',mnuFile,'label','&Open',...
                'callback','chanman(''Load'');') ;
            uimenu('parent',mnuFile,'label','&Save',...
                'callback','chanman(''Save'');') ;
            uimenu('parent',mnuFile,'label','E&xit',...
                'separator','on',...
                'callback','chanman(''Exit'');') ;

            % Modify menu
            mnuModify = uimenu('tag','mnuModify',...
                'label','&Modify');%'enable','off') ;
            uimenu('parent',mnuModify,'label','&Name',...
                'callback','chanman(''Rename'');') ;
            uimenu('parent',mnuModify,'label','&Test',...
                'callback','chanman(''EditTest'');') ;
            uimenu('parent',mnuModify,'label','&Units',...
                'callback','chanman(''Reunit'');') ;

            % Tools menu
            toolDefs = chanman_tools;
            if ~isempty(toolDefs)
                mnuTools = uimenu('Label','&Tools');
                for i = 1:length(toolDefs),
                    uimenu('Parent',mnuTools,...
                        'Label',toolDefs(i).Label,...
                        'Callback',toolDefs(i).Callback);
                end
            end

            % Plug-ins menu, to replace tools menu when
            % plugins reach full functionality
            pluginDefs = chanman_plugins;
            if ~isempty(pluginDefs)
                mnuPlugin = uimenu('Label','&Plugins');
                mnuPluginSub = [];
                for i = 1:length(pluginDefs),
                    % Handle submenus
                    if isempty(pluginDefs(i).SubMenuName),
                        uimenu('Parent',mnuPlugin,...
                            'Label',pluginDefs(i).Label,...
                            'Callback',pluginDefs(i).Callback);
                    else
                        % Do we need to create new submenu?
                        if isfield(mnuPluginSub,pluginDefs(i).SubMenuName),
                            % No, have a submenu
                            uimenu('Parent',getfield(mnuPluginSub,pluginDefs(i).SubMenuName),...
                                'Label',pluginDefs(i).Label,...
                                'Callback',pluginDefs(i).Callback);
                        else
                            % Yes, create submenu
                            newMnuHan = uimenu('Parent',mnuPlugin,...
                                'Label',pluginDefs(i).SubMenuLabel);,...
                                mnuPluginSub = setfield(mnuPluginSub,pluginDefs(i).SubMenuName,newMnuHan);

                            % Now add the menu item
                            uimenu('Parent',newMnuHan,...
                                'Label',pluginDefs(i).Label,...
                                'Callback',pluginDefs(i).Callback);

                        end
                    end
                end
            end

        end

        % Set handle visibility
        set(Fig,'HandleVisibility','callback') ;
        ChanManFig = Fig;

        % Set save flag
        dataDirty = 0;

        % End 'create'
        %---------------------------------------------------

    case 'resize'
        % Window sizes
        window = get(gcf,'position') ;
        window_w = window(3) ;
        window_h = window(4) ;

        % Limit the height reduction
        if (window_h < 290)
            window(4) = 290;
            set(gcf,'position',window);
            return
        end

        % Create window objects
        % Selection Action Frame
        x = 3 ;      y = 3 ;
        frame_h = 127 ;
        set(findobj('tag','frmSelect'),...
            'position',[x (window_h-frame_h-y) 135 frame_h]) ;
        set(findobj('tag','txtSelect'),...
            'position',[x+5 (window_h-20-y) 65 18]) ;
        set(findobj('tag','rbActionNone'),...
            'position',[x+10 (window_h-40-y) 45 18]) ;
        set(findobj('tag','rbActionPlot'),...
            'position',[x+10 (window_h-60-y) 45 18]) ;
        set(findobj('tag','rbActionDragPlot'),...
            'position',[x+10 (window_h-80-y) 90 18]) ;
        set(findobj('tag','rbActionEvalCmd'),...
            'position',[x+10 (window_h-100-y) 95 18]) ;
        set(findobj('tag','chkActionPlot'),...
            'position',[x+55 (window_h-60-y) 75 18]) ;
        set(findobj('tag','edtActionEvalCmd'),...
            'position',[x+25 (window_h-122-y) 105 22]) ;

        % Workspace Transfer Frame
        x = 3 ;      y = 125 ;
        frame_w = 135 ;      frame_h = 72 ;
        set(findobj('tag','frmWorkspace'),...
            'position',[x (window_h-frame_h-y) frame_w frame_h]) ;
        set(findobj('tag','txtWorkspace'),...
            'position',[x+5 (window_h-20-y) 105 18]) ;
        set(findobj('tag','txtExport'),...
            'position',[x+10 (window_h-43-y) 40 18]) ;
        set(findobj('tag','edtExport'),...
            'position',[x+47 (window_h-43-y) (frame_w-52) 22]) ;
        set(findobj('tag','txtImport'),...
            'position',[x+10 (window_h-68-y) 40 18]) ;
        set(findobj('tag','edtImport'),...
            'position',[x+47 (window_h-68-y) (frame_w-52) 22]) ;

        % Clear Frame
        x = 3 ;      y = 212 ;
        frame_w = 135 ;      frame_h = 68 ;
        set(findobj('tag','frmClear'),...
            'style','frame',...
            'position',[x (window_h-frame_h-y) frame_w frame_h]) ;
        set(findobj('tag','btnClearOne'),...
            'position',[x+5 (window_h-30-y) (frame_w-11) 25]) ;
        set(findobj('tag','btnClearAll'),...
            'position',[x+5 (window_h-60-y) (frame_w-11) 25]) ;

        % Statusbar Frame
        x = 3 ;      y = 1 ;
        frame_w = window_w-3 ;      frame_h = 24 ;
        set(findobj('tag','frmStatusBar'),...
            'position',[x y frame_w frame_h]) ;
        set(findobj('tag','txtStatusBar'),...
            'position',[x+3 (y+2) (frame_w-10) 18]) ;

        % Channel List Box
        x = 140 ;         y = 260 ;
        frame_w = 300 ;   frame_h = 257 ;
        set(findobj('tag','frmChannelList'),...
            'position',[x 30 window_w-140 window_h-33]) ;
        set(findobj('tag','lstChannelList'),...
            'position',[x+2 32 (window_w-140-4) (window_h-24-33)]) ;
        set(findobj('tag','txtChannelList'),...
            'position',[x+5 (window_h-y+235) (window_w-140-10) 16]) ;


        % End 'Resize'
        %---------------------------------------------------

    case 'actionpush'
        % User pushed one of the selection action radio buttons
        name = get(gcbo,'tag') ;

        % Take action depending on which was pushed
        switch name
            case 'rbActionNone'
                % Set other's values to zero, disable their
                % auxilliary controls.
                set(findobj('tag','rbActionNone'),'value',1) ;
                set(findobj('tag','rbActionPlot'),'value',0) ;
                set(findobj('tag','rbActionDragPlot'),'value',0) ;
                set(findobj('tag','chkActionPlot'),'enable','off') ;
                set(findobj('tag','rbActionEvalCmd'),'value',0) ;
                set(findobj('tag','edtActionEvalCmd'),'enable','off',...
                    'backgroundcolor',[1 1 1]*.75) ;

            case 'rbActionPlot'
                % Set other's values to zero, disable their
                % auxilliary controls, enable this one's.
                set(findobj('tag','rbActionNone'),'value',0) ;
                set(findobj('tag','rbActionPlot'),'value',1) ;
                set(findobj('tag','rbActionDragPlot'),'value',0) ;
                set(findobj('tag','chkActionPlot'),'enable','on') ;
                set(findobj('tag','rbActionEvalCmd'),'value',0) ;
                set(findobj('tag','edtActionEvalCmd'),'enable','off',...
                    'backgroundcolor',[1 1 1]*.75) ;

            case 'rbActionDragPlot'
                % Set other's values to zero, disable their
                % auxilliary controls, enable this one's.
                set(findobj('tag','rbActionNone'),'value',0) ;
                set(findobj('tag','rbActionPlot'),'value',0) ;
                set(findobj('tag','rbActionDragPlot'),'value',1);
                set(findobj('tag','chkActionPlot'),'enable','off') ;
                set(findobj('tag','rbActionEvalCmd'),'value',0) ;
                set(findobj('tag','edtActionEvalCmd'),'enable','off',...
                    'backgroundcolor',[1 1 1]*.75) ;

            case 'rbActionEvalCmd'
                % Set other's values to zero, disable their
                % auxilliary controls, enable this one's.
                set(findobj('tag','rbActionNone'),'value',0) ;
                set(findobj('tag','rbActionPlot'),'value',0) ;
                set(findobj('tag','rbActionDragPlot'),'value',0) ;
                set(findobj('tag','rbActionEvalCmd'),'value',1) ;
                set(findobj('tag','chkActionPlot'),'enable','off') ;
                set(findobj('tag','edtActionEvalCmd'),'enable','on',...
                    'backgroundcolor',[1 1 1]) ;

        end

        % End 'ActionPush'
        %---------------------------------------------------

    case 'import'

        % Get variable name
        var = get(findobj('tag','edtImport'),'string') ;
        var = fliplr(deblank(fliplr(deblank(var))))    ;

        % See if we get one or all
        if strcmp(var,'all'),
            % Get all variables from workspace
            evalin('base','global IMPORTLIST_VAR')
            global IMPORTLIST_VAR
            evalin('base','IMPORTLIST_VAR=who;') ;

            % Loop through all returned
            for i = 1:length(IMPORTLIST_VAR),
                chanman('ImportOne',IMPORTLIST_VAR{i}) ;
            end

            % UI
            set(findobj('tag','txtStatusBar'),...
                'string','All channel objects from Workspace imported') ;

        else,
            % Just get the named one
            chanman('ImportOne',var) ;
        end

        % clear global in workspace
        evalin('base','clear IMPORTLIST_VAR')

        % End 'Import'
        %---------------------------------------------------

    case 'importone'
        % Import the given variable from the workspace
        var = varargin{1} ;

        % Setup global variable for data transfer
        evalin('base','global IMPORT_VAR')
        global IMPORT_VAR

        % Error check, see if it exists in workspace
        evalin('base',['IMPORT_VAR=exist(''' var ''',''var'');']) ;
        if IMPORT_VAR~=1,
            % UI
            set(findobj('tag','txtStatusBar'),'string',['Error: ''' var ''' does not exist in Workspace']) ;

            % Clear that field
            set(findobj('tag','edtImport'),'string','') ;

            % Break out of here
            return;
        end

        % Setup Global to get variable
        evalin('base',['IMPORT_VAR=' var ';'])

        % Error check, make sure it's a channel
        if isa(IMPORT_VAR,'channel')
            % Add it
            chanman('AddChannel',IMPORT_VAR) ;
            temp = struct(IMPORT_VAR) ;

            % UI
            set(findobj('tag','txtStatusBar'),'string',['''' temp.name ''' Imported from Workspace']) ;

        else,

            % UI
            set(findobj('tag','txtStatusBar'),'string',['Import of ''' var ''' Failed']) ;

        end

        % Clear global in workspace
        evalin('base','clear IMPORT_VAR')

        % Set save flag
        dataDirty = 1;

        % End 'ImportOne'
        %---------------------------------------------------

    case 'export'
        % Get variable name
        var = get(findobj('tag','edtExport'),'string') ;
        var = fliplr(deblank(fliplr(deblank(var))))    ;

        % See if one or all
        if ~strcmp(var,'all'),
            % Just the one

            % Get channel
            chan = chanman('GetChannel') ;

            chanman('ExportOne',var,chan) ;

        else,

            % Get channel data
            channels = getappdata(ChanManWindow,'Data') ;

            % Loop through all
            for i = 1:length(channels),

                % Name
                chan = channels{i}  ;
                temp = struct(chan) ;
                var  = temp.name    ;

                str = chanman('StringToVarName',var);
                
                % Call the export routine
                chanman('ExportOne',str,chan) ;

            end

            % UI
            set(findobj('tag','txtStatusBar'),...
                'string','All channels exported to Workspace') ;

        end

        % End 'Export'
        %---------------------------------------------------

    case 'exportone'

        % Get variables
        var  = varargin{1} ;
        chan = varargin{2} ;

        % Export it
        assignin('base',var,chan) ;

        % UI
        temp = struct(chan) ;
        set(findobj('tag','txtStatusBar'),'string',['''' temp.name ''' Exported to Workspace as ''' var '''']) ;

        % End 'ExportOne'
        %---------------------------------------------------

    case 'load'
        % UI
        set(findobj('tag','txtStatusBar'),'string',' ') ;

        % See if we were passed a file name or need to query user
        if length(varargin),
            % Extract from varargin
            FileName = varargin{1} ;
            PathName = '' ;
        else
            % Query user
            [FileName, PathName] = uigetfile('*.mat','Load File') ;
        end

        if FileName ~= 0,
            % UI
            set(findobj('tag','txtStatusBar'),'string',['Loading: ''' FileName '''']) ;

            % Load file and clear variables
            load([PathName FileName]) ;
            clear PathName

            % Who listing
            vars = who ;

            % Intitialize what we need
            str = '' ;

            % Cycle through variables
            for i = 1:length(vars),
                % Get first one
                eval(['v=' vars{i} ';']) ;

                % Test it
                if isa(v,'channel'),
                    % Add it
                    chanman('AddChannel',v) ;
                end
            end

            % UI
            set(findobj('tag','txtStatusBar'),'string',['''' FileName '''' ' Loaded']) ;

        else,
            % UI
            set(findobj('tag','txtStatusBar'),'string','Load Aborted') ;
        end

        % Set save flag
        dataDirty = 0;

        % End 'Load'
        %---------------------------------------------------

    case 'addchannel'
        % Get channel
        chan = varargin{1} ;

        % Error check
        if ~isa(chan,'channel'),
            % UI
            set(findobj('tag','txtStatusBar'),'string','Error Adding Channel') ;
            error('Must be a channel object') ;
        else,
            % Turn it in to a struct
            tmp = struct(chan) ;
        end

        % Get all channels
        fig = ChanManWindow ;
        channels = getappdata(fig,'Data') ;

        % Listbox handle
        han = findobj(get(fig,'children'),'tag','lstChannelList') ;

        % Make sure we have something
        if strcmp(get(han,'string'),'Empty'),
            % Only one in there
            channels{1} = chan ;
            num = 0 ;
            ChanList = '' ;
        else,
            % Just add this channel
            channels{length(channels)+1} = chan ;

            % Get channel list string
            ChanList = get(han,'string') ;

        end

        % Get pertinent strings for this channel
        [n,l,s,u]   = display(chan) ;    u = ['[' u ']'] ;
        ThisStringMat = str2mat(n,u,s,l)  ;
        ThisString    = '' ;

        % Widths of fields
        ThisWidth = [size(n,2) size(u,2) size(s,2) size(l,2)] + 3 ;
        AllWidth  = getappdata(fig,'StringWidths') ;

        % Loop through the fields
        for i = 1:4,
            % See which string needs extension
            if isempty(ChanList),
                % This is the only channel
                ThisString  = [ThisString deblank(ThisStringMat(i,:)) '   ' ] ;
                AllWidth(i) = ThisWidth(i) ;

            elseif ThisWidth(i) <= AllWidth(i),
                % Need to pad the new string
                ThisString = [ThisString deblank(ThisStringMat(i,:)) '   ' blanks(AllWidth(i)-ThisWidth(i))] ;

            elseif ThisWidth(i) > AllWidth(i),
                % Need to pad the list
                % Build the pad
                Pad = char(ones(size(ChanList,1),ThisWidth(i)-AllWidth(i))*' ') ;

                % Insert it
                InsertPoint = sum(AllWidth(1:i)) ;
                ChanList = [ChanList(:,1:InsertPoint) Pad ChanList(:,InsertPoint+1:size(ChanList,2))] ;

                % Change the AllWidth Entry
                AllWidth(i) = ThisWidth(i) ;

                % Update ThisString
                ThisString = [ThisString deblank(ThisStringMat(i,:)) '   '] ;
            end
        end

        % Save widths
        setappdata(fig,'StringWidths',AllWidth) ;

        % Display and save
        set(han,'string',[ChanList ; ThisString]) ;
        setappdata(fig,'Data',channels) ;

        % Update title string
        labelstr = ['Name' blanks(AllWidth(1)-4),...
            'Units' blanks(AllWidth(2)-5),...
            'Rate' blanks(AllWidth(3)-11),...
            'Points' blanks(AllWidth(4)-6)] ;
        set(findobj('tag','txtChannelList'),'string',labelstr) ;

        % UI
        set(findobj('tag','txtStatusBar'),'string',['Added Channel: ''' tmp.name '''']) ;

        % Enable the modify menu
        set(findobj('tag','mnuModify'),'enable','on') ;

        % Set save flag
        dataDirty = 1;

        % End 'AddChannel'
        %---------------------------------------------------

    case 'channelselect'

        % Set tooltip string
        % REVIST - If have more metadata, would be nice to display here.
        chan = chanman('GetChannel');
        str=['Selected Channel: ',char(chan)];
        set(findobj('tag','lstChannelList'),'tooltipstring',str);

        % User wants to do something
        % See which button is pushed
        if get(findobj('tag','rbActionPlot'),'value'),
            % User wants to plot
            % Get channel
            chan = chanman('GetChannel') ;
            if isempty(chan),
                return;
            end

            % Convert to struct to get name
            temp = struct(chan) ;

            % Get which figure to plot in
            Fig = ChanManWindow ;
            plotfig = getappdata(Fig,'PlotFig')    ;

            % Open new figure if necessary
            if isempty(plotfig),
                % Open new figure
                plotfig = figure('numbertitle','off',...
                    'name',[temp.name]) ;

                % Store it
                setappdata(Fig,'PlotFig',plotfig) ;
            else,
                % See if we need to open a new one
                if get(findobj('tag','chkActionPlot'),'value'),
                    % Open new figure
                    plotfig = figure('numbertitle','off',...
                        'name',[temp.name]) ;

                    % Store it
                    setappdata(Fig,'PlotFig',plotfig) ;

                else,
                    % Recall the old figure
                    figure(plotfig) ;

                    % Assume user wants to add plot
                    set(gca,'NextPlot','Add');

                    set(plotfig,'numbertitle','off',...
                        'name',[temp.name]) ;

                end

            end

            % Plot it
            plot(chan) ;

            % UI
            set(findobj('tag','txtStatusBar'),'string',['''' temp.name ''' Plotted']) ;

        elseif get(findobj('tag','rbActionEvalCmd'),'value'),
            % User wants to execute a command
            % Get command
            cmd = get(findobj('tag','edtActionEvalCmd'),'string') ;
            cmd = [cmd ';'] ;  % Don't want to print something every time

            % Get current channel, data and time
            ch   = chanman('GetChannel') ;
            temp = struct(ch) ;
            data = temp.data   ;
            time = sampvec(ch) ;

            % See if there's output
            if isempty(findstr(cmd,'=')),
                % No output
                eval(cmd,'cmd=''Error''') ;

                if strcmp(cmd,'Error'),
                    % UI
                    set(findobj('tag','txtStatusBar'),'string','Error in Command Evaluation') ;
                else,
                    % UI
                    set(findobj('tag','txtStatusBar'),'string',['''' cmd ''' Executed']) ;
                end

            else,

                % There's output, first evaluate
                eval(cmd,'cmd=''Error''') ;

                if strcmp(cmd,'Error'),
                    % UI
                    set(findobj('tag','txtStatusBar'),'string','Error in Command Evaluation') ;
                    return ;
                end

                if exist('n'),
                    if ~isstr(n),
                        if (length(n)==1) & (isa(n,'double')),
                            n = num2str(n) ;
                        else,
                            % Display in Matlab Window
                            disp(n) ;

                            % UI
                            set(findobj('tag','txtStatusBar'),'string',['''' cmd ''' Output in Matlab Window']) ;
                        end

                        % UI
                        set(findobj('tag','txtStatusBar'),'string',['Command Output: ' n]) ;

                    end

                elseif exist('x') & exist('y'),
                    % User wants a plot output
                    figure ;
                    plot(x,y);

                else,
                    % Output was a channel
                    chanman('AddChannel',ch) ;
                end

            end

        elseif get(findobj('tag','rbActionDragPlot'),'value'),
            % Register button down
            set(findobj('tag','lstChanneList'),'ButtonDownFcn','chanman(''ChannelDragStart'');');

        end

        drawnow

        % End 'ChannelSelect'
        %---------------------------------------------------

    case 'getchannel'

        % Get selected index
        ind = get(findobj(get(ChanManWindow,'Children'),'tag','lstChannelList'),'value') ;

        % Get data
        channels = getappdata(ChanManWindow,'Data') ;

        % Get our channel
        if isempty(channels),
            out = [];
        else
            out = channels{ind} ;
        end

        % End 'GetChannel'
        %---------------------------------------------------

    case 'clearall'

        % Clear Data
        setappdata(ChanManWindow,'Data',[]) ;

        % Get all children
        children = get(ChanManWindow,'Children');

        % Clear List
        set(findobj(children,'tag','lstChannelList'),'string','Empty',...
            'value',1) ;

        % Disable the modify menu
        set(findobj(children,'tag','mnuModify'),'enable','off') ;

        % UI
        set(findobj(children,'tag','txtStatusBar'),'string','All Channels Cleared') ;

        % Set save flag
        dataDirty = 0;

        % End 'ClearAll'
        %---------------------------------------------------

    case 'clearone'

        % Get Data
        Fig = ChanManWindow ;
        channels = getappdata(Fig,'Data')      ;

        % Check for only one channel
        if length(channels) > 1,
            % Get String
            children = get(Fig,'Children');
            ListHan = findobj(children,'tag','lstChannelList') ;
            str = get(ListHan,'string') ;

            % Get selected index
            ind = get(ListHan,'value') ;

            % Remove ind from both
            clearchannel = channels{ind} ;
            clearchannel = struct(clearchannel) ;
            clearchannel = clearchannel.name ;

            if ind == 1,
                str = str(2:length(channels),:)         ;
                channels = channels(2:length(channels)) ;
            else,
                str = str([1:(ind-1), (ind+1):length(channels)],:)         ;
                channels = channels([1:(ind-1), (ind+1):length(channels)]) ;
            end

            % Store them
            setappdata(Fig,'Data',channels) ;
            set(ListHan,'string',str,...
                'value',min(ind,size(str,1)))     ;
        else,
            % Just call ClearAll routine
            chanman('ClearAll') ;
            return ;
        end

        % UI
        if exist('clearchannel'),
            set(findobj('tag','txtStatusBar'),'string',['''' clearchannel ''' Cleared']) ;
        end

        % End 'ClearOne'
        %---------------------------------------------------
    case 'rename'

        % Get the channel, then clear it
        chan = chanman('GetChannel') ;
        chanman('ClearOne')          ;

        % Call the name editor
        edname(chan);

        % Add it back in
        chanman('AddChannel',chan) ;

        % Set save flag
        dataDirty = 1;

        % Enable modify menu
        set(findobj('tag','mnuModify'),'enable','on');

        % End 'Rename'
        %---------------------------------------------------

    case 'edittest'

        % Get the channel, then clear it
        chan = chanman('GetChannel') ;
        chanman('ClearOne')          ;

        % Call the name editor
        edtest(chan);

        % Add it back in
        chanman('AddChannel',chan) ;

        % Set save flag
        dataDirty = 1;

        % End 'edittest'
        %---------------------------------------------------
    case 'reunitchannel'

        % New name and conversion factor
        str = varargin{1} ;
        factor = varargin{2} ;

        % Get the channel, then clear it
        chan = chanman('GetChannel') ;
        chanman('ClearOne')          ;

        % Set the new name
        chan = struct(chan) ;
        chan.units = str ;
        chan = channel(chan.name,factor*chan.data,chan.sample,chan.units,chan.test) ;

        % Add it back in
        chanman('AddChannel',chan) ;

        % Set save flag
        dataDirty = 1;

        % End 'ReunitChannel
        %---------------------------------------------------

    case 'reunit'

        % Reunit a channel
        chan = chanman('GetChannel') ;

        if (exist('unitlibrary.mat') & (isa(get(chan,'data'),'unit'))),
            disp(['Current units: [' get(chan,'units') ']']);
            newUnit = input('New Units: ','s');
            convert(chan,newUnit);
            chanman('ClearOne');
            chanman('AddChannel',chan);
            return;
        end

        chan = struct(chan);
        handle = figure('menubar','none',...
            'numbertitle','off',...
            'position',[502   502   282   140],...
            'color',[1 1 1]*.8,...
            'resize','off',...
            'windowstyle','modal',...
            'name','Change Units',...
            'tag','mdlReunit') ;

        % Window sizes
        window = get(gcf,'position') ;
        window_w = window(3) ;
        window_h = window(4) ;

        % Create window objects
        % Text fields
        x = 3 ;      y = 3 ;
        frame_h = 135 ;

        uicontrol('style','frame',...
            'position',[x (window_h-frame_h-y) 280 frame_h]) ;
        uicontrol('string','Channel:',...
            'style','text',...
            'horizontalalignment','left',...
            'position',[x+5, (window_h-22-y) 65 18]) ;
        uicontrol('string',chan.name,...
            'style','text',...
            'horizontalalignment','left',...
            'position',[x+70 (window_h-22-y) 200 18]) ;

        uicontrol('string','Old Units:',...
            'style','text',...
            'horizontalalignment','left',...
            'position',[x+5 (window_h-46-y) 65 18]) ;
        uicontrol('string',['[' chan.units ']'],...
            'style','text',...
            'horizontalalignment','left',...
            'position',[x+70 (window_h-46-y) 200 18]) ;

        uicontrol('string','New Units:',...
            'style','text',...
            'horizontalalignment','left',...
            'position',[x+5 (window_h-70-y) 65 18]) ;
        uicontrol('tag','edtNewUnits',...
            'string',chan.units,...
            'style','edit',...
            'horizontalalignment','left',...
            'backgroundcolor',[1 1 1],...
            'position',[x+70 (window_h-72-y) 200 22]) ;

        uicontrol('string','Conversion:',...
            'style','text',...
            'horizontalalignment','left',...
            'position',[x+5 (window_h-94-y) 65 18]) ;
        uicontrol('tag','edtFactor',...
            'string','1',...
            'style','edit',...
            'horizontalalignment','left',...
            'backgroundcolor',[1 1 1],...
            'position',[x+70 (window_h-96-y) 200 22]) ;

        % Buttons
        str = ['name=get(findobj(''tag'',''edtNewUnits''),''string'');',...
            'factor=get(findobj(''tag'',''edtFactor''),''string'');',...
            'units;eval([''factor='' factor '';'']);',...
            'chanman(''ReunitChannel'',name,factor);',...
            'close(gcf);'];
        uicontrol('tag','btnOK',...
            'string','OK',...
            'callback',str,...
            'style','pushbutton',...
            'horizontalalignment','center',...
            'position',[x+145 (window_h-127-y) 80 25]) ;
        uicontrol('tag','btnCancel',...
            'string','Cancel',...
            'callback','Close(gcf);',...
            'style','pushbutton',...
            'horizontalalignment','center',...
            'position',[x+55 (window_h-127-y) 80 25]) ;

        % Set save flag
        dataDirty = 1;

        % End 'Reunit'
        %---------------------------------------------------

    case 'getdata'

        % Get data from datalink
        channels = Datalink('Data') ;

        % Cycle through them
        for i = 1:length(channels)
            chanman('AddChannel',channels{i}) ;
        end

        % End 'GetData'
        %---------------------------------------------------
    case 'getchan'

        % Get data
        channels = getappdata(ChanManWindow,'Data') ;
        numChans = length(channels);
        channame = deblank(varargin{1}) ;

        % Loop
        i = 1 ; ind = [] ;
        while (isempty(ind) & (i<=numChans)),
            % Check name
            chan = struct(channels{i}) ;
            if strcmp(deblank(chan.name),channame),
                ind = i ;
            end

            % Increment counter
            i = i + 1 ;

        end

        if isempty(ind),
            out = [];
        else,
            out = channels{ind} ;
        end

        % End 'GetChan'
        %---------------------------------------------------

    case 'deletechan'

        % Get data
        channels = getappdata(ChanManWindow,'Data') ;
        channame = deblank(varargin{1}) ;
        numChans = length(channels);

        % Loop
        i = 1 ; ind = [] ;
        while isempty(ind) & (i<=numChans),
            % Check name
            chan = struct(channels{i}) ;
            if strcmp(deblank(chan.name),channame),
                ind = i ;
            end

            % Increment counter
            i = i + 1 ;

        end

        % See if we found a channel
        if isempty(ind),
            % Didn't find one
            % UI
            set(findobj('tag','txtStatusBar'),'string',...
                ['No channel of name:' channame]) ;
            out = [];
            return;
        end

        % Highlight channel to clear
        children = get(ChanManWindow,'Children');
        set(findobj(children,'tag','lstChannelList'),'value',ind) ;

        % Clear it
        chanman('ClearOne') ;
        out = [];

        % End 'DeleteChan'
        %---------------------------------------------------

        % This function maps the predicate function over the
        % the entire list of channels in the channel manager.
        % The predicate function takes the channel
        % as an input, and may return a channel. If the channel
        % has data, it will be to be added to the channel list.
        % The predication function call must look like:
        %  fcn_name(arg1,...,ch,...,argN)
        %  'ch' will be replaced with an actual channel when called.
        % The function must return a channel as it's default output.
    case 'map'
        set(ChanManFig,'HandleVisibility','On');

        % Get data
        channels  = getappdata(ChanManWindow,'Data') ;
        predicate = deblank(varargin{1}) ;

        % UI
        set(findobj('tag','txtStatusBar'),'string',...
            ['Applying ''' predicate ''' to all channles.']) ;

        % Build the error string
        errstr = ['Error mapping ''' predicate ''' over the channel list.'] ;

        % Channels
        numChan = length(channels);

        % Loop
        for i = 1:numChan,

            % UI
            str = ['Mapping ''' predicate ''' over channel ' num2str(i) ' of ' num2str(numChan)];
            set(findobj('tag','txtStatusBar'),'string',str);
            drawnow

            % Build the string
            %      str = ['out = ' predicate '(channels{' num2str(i) '});'] ;
            str = ['out = ' strrep(predicate,'(ch)',['(channels{' num2str(i) '})']) ';'] ;

            % Call it
            lasterr('');
            eval(str,'');

            % Delete old channels if function works
            if ~isempty(lasterr),
                set(findobj('tag','txtStatusBar'),'string',errstr);
                return;
            end
            if (i == 1),
                chanman('ClearAll');
            end

            % See if we add a channel
            if isa(out,'channel'),
                if get(out,'points') > 0,
                    chanman('AddChannel',out);
                end
            end

        end

        % Delete old channels unless told to keep

        % UI
        set(findobj('tag','txtStatusBar'),'string',...
            'Ready') ;

        % Protect the UI again
        set(ChanManFig,'HandleVisibility','Callback');

        % End 'Map'
        %---------------------------------------------------

    case 'save'

        % Get channel data
        channels = getappdata(ChanManWindow,'Data') ;

        % Make sure data exists to store
        if isempty(channels),
            UIError('No channels to save.');
            return;
        end

        % Get file and path
        if length(varargin),
            % Got a name, so store it
            fileName = varargin{1};
            pathName = '';
        else
            % Didn't receive name, so prompt
            chan = channels{1};

            % If there was a source file for the channel data, propose it.
            meta = get(chan,'metadata');
            if ~isempty(meta) && isfield(meta,'source_file'),
                default_name = meta.source_file;
            else            
                default_name = 'data';
            end
            [fileName, pathName] = uiputfile([default_name '.mat'],'Save Channels');
        end

        if fileName ~= 0,

            % Loop through all
            nameString = '';
            for i = 1:length(channels),

                % Name
                chan = channels{i}  ;
                temp = struct(chan) ;
                var  = temp.name    ;

                % Remove trailing blanks
                str = deblank(var) ;

                str = makevalidname(str);

                % Set variable data to name
                eval([str ' =  chan;']);

                % Build name matrix
                nameString = str2mat(nameString, str);

            end

            % Build up save string
            saveString = '';
            for i = 2:length(channels)+1,
                saveString = [saveString '''' deblank(nameString(i,:)) ''','];
            end
            saveString = ['save(''' pathName fileName ''',' saveString(1:length(saveString)-1) ');'];

            % Save it
            eval(saveString);

            % Set save flat
            dirtyData = 0;

            % UI
            set(findobj('tag','txtStatusBar'),...
                'string',['File ''' fileName ''' saved']) ;

        else

            % UI
            set(findobj('tag','txtStatusBar'),...
                'string',['Save aborted.']) ;

        end


        % End 'Save'
        %---------------------------------------------------

    case 'exit'

        v = ver('Matlab');
        v = str2num(v.Version);
        if v>=7.6
            
            % Load dict
            pos_dict = getpref('CHANNELS','ChanManFigurePos');
            screen   = get(0,'ScreenSize');

            % Update entry for this screen size
            pos = get(ChanManWindow,'Position');
            pos_dict(screen) = pos;

            % Store out dict
            setpref('CHANNELS','ChanManFigurePos',pos_dict);
        end
        
        % Check the save flag and allow the user to save if needed.
        if dataDirty,

            % Prompt the user
            resp = questdlg('Data has not been saved.  Do you wish to save before exiting?',...
                'Channel Manager','Yes','No','Yes');

            % Allow to save if want to
            if strcmp(resp,'No'),
                % Set save flag for next call
                dataDirty=0;

                % Close the figure
                delete(gcf);

                return;
            else,
                % Call up the save routine
                chanman('save');
            end

        end

        % Set save flag for next call
        dataDirty=0;

        % Close the figure
        delete(gcf);

        % End 'exit'
        %---------------------------------------------------

    case 'setmessage'
        set(ChanManFig,'HandleVisibility','On');
        set(findobj('tag','txtStatusBar'),...
            'string',varargin{1}) ;
        set(ChanManFig,'HandleVisibility','Callback');

        % End 'setmessage'
        %---------------------------------------------------

    case 'getchannelselected'
        out = chanman('GetChannel');

        % End 'getchannelselected'
        %---------------------------------------------------

    case 'getchannelbyname'
        out = chanman('GetChan',varargin{1});

        % End 'getchannelbyname'
        %---------------------------------------------------

    case 'deletechannelbyname'
        out = chanman('deletechan',varargin{1});

        % End 'deletechannelbyname'
        %---------------------------------------------------



    case 'getchannelall'

        % Get data
        out = getappdata(ChanManWindow,'Data') ;

        % End 'getchannelall'
        %---------------------------------------------------



    case 'registerchannelselectcallback'
        % Set radio buttons so that "None" is selected,
        % and all are disabled.
        set(ChanManFig,'HandleVisibility','On');
        set(findobj('tag','rbActionNone'),'value',1,'Enable','Off') ;
        set(findobj('tag','rbActionPlot'),'value',0,'Enable','Off') ;
        set(findobj('tag','chkActionPlot'),'enable','off') ;
        set(findobj('tag','rbActionEvalCmd'),'value',0,'Enable','Off') ;
        set(findobj('tag','edtActionEvalCmd'),'enable','off',...
            'backgroundcolor',[1 1 1]*.75) ;

        % Change the callback
        han = findobj('tag','lstChannelList');
        set(han,'Callback',varargin{1});

        set(ChanManFig,'HandleVisibility','Callback');

        % End 'registerchannelselectcallback'
        %---------------------------------------------------

    case 'clearchannelselectcallback'
        % Enable all radio buttons.
        set(ChanManFig,'HandleVisibility','On');
        set(findobj('tag','rbActionNone'),'value',1,'Enable','On') ;
        set(findobj('tag','rbActionPlot'),'value',0,'Enable','On') ;
        set(findobj('tag','chkActionPlot'),'enable','off') ;
        set(findobj('tag','rbActionEvalCmd'),'value',0,'Enable','On') ;
        set(findobj('tag','edtActionEvalCmd'),'enable','off',...
            'backgroundcolor',[1 1 1]*.75) ;

        % Change the callback to the default.
        han = findobj('tag','lstChannelList');
        set(han,'Callback','chanman(''ChannelSelect'')');

        set(ChanManFig,'HandleVisibility','Callback');

        % End 'registerchannelselectcallback'
        %---------------------------------------------------

    case 'setplotfigure',
        % Store it
        Fig = ChanManWindow ;
        setappdata(Fig,'PlotFig',varargin{1}) ;

    case 'channeldragstart'
        % Channel drag to add to plot.
        disp('dragstart');

        set(ChanManWindow,'Pointer','Custom');


    case 'channeldragstop'
        % Channel drag complete.
        disp('dragstop');

    case 'sortchannels'
        chanList = chanman('GetChannelAll');
        chanman('ClearAll');
        nameList = char(chanList{:})';
        [sortedNames,ind] = sort(nameList);
        chanList = chanList(ind);

        for i = 1:length(chanList),
            chanman('AddChannel',chanList{i});
        end

    case 'stringtovarname'
        out = genvarname(varargin{1});

    case 'channelrightselect'
        % Get selected channel
        chan = chanman('GetChannelSelected');
        if isempty(chan), return; end
        meta = get(chan,'metadata');
        name = get(chan,'Name');
        name = chanman('StringToVarName',name);
        name = [name '_Metadata'];
        assignin('base',name,meta);
        openvar(name);
        
        
    case 'null'
        % Do nothing
    otherwise
        % Didn't recognize command
        error(['Unknown command: ' msg]);
end
% EOF


%---------------------------------------------------
%---------------------------------------------------
%---------------------------------------------------
% Support Functions
%---------------------------------------------------
%---------------------------------------------------
%---------------------------------------------------


%---------------------------------------------------
% UIError
% Displays error message
%---------------------------------------------------
function [] = UIError(msg)

set(findobj('tag','txtStatusBar'),'string',msg);
