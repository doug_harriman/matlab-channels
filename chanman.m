%CHANMAN  Channel Object Manager UI.
%
% Messages:
%   create
%   resize
%   actionpush
%   import
%   importone
%   export
%   exportone
%   load
%   addchannel
%   channelselect
%   clearall
%   clearone
%   rename
%   edittest
%   reunit
%   reunitchannel
%
%  Messages with updated documentation:
%   ClearChannelSelectCallback    - Clear user defined callback for channel selection.
%   DeleteChannelByName           - Deletes the channel with the given name if it exists.
%   Exit                          - Shut down Channel Manager.
%   GetChannelAll                 - Returns a cell array of all of the channels.
%   GetChannelByName              - Returns the channel with the given name if it exists.
%   GetChannelSelected            - Returns the currently selected channel.
%   Map                           - Map the predicate function over all channels.
%                                   The predicate function takes the channel
%                                   as an input, and may return a channel. If the channel
%                                    has data, it will replace the original channel in the list.
%                                   The predication function call must look like:
%                                   fcn_name(arg1,...,ch,...,argN)
%                                   'ch' will be replaced with an actual channel when called.
%                                   The function must return a channel as it's default output.
%   Save                          - Saves channels to MAT file.  Will prompt for filename if not provided.
%   SetMessage                    - Sets status bar message.  Only callable from callbacks.
%   SetPlotFigure                 - Sets the figure to use for plotting.
%   SortChannels                  - Sorts channels by name.
%   RegisterChannelSelectCallback - Register a user defined callback for channel selection.
%
%  Depricated Messages
%   GetChan    - Use 'GetChannelByName'.
%   GetChannel - Use 'GetChannelSelected'.
%   DeleteChan - Use 'DeleteChannelByName'.
%
%  Old/Unused Messages
%   GetData                      - Retrieves data from VB.

% DLH

function [out] = chanman(msg,varargin)

% Flag for saves
persistent objchanman

% Make sure we get an input message
if nargin == 0
    if ~isa(objchanman,'chantable')
        msg = 'create' ;
    elseif ~isvalid(objchanman)
        msg = 'create';
    else
        figure(objchanman.figurechanman);
        if nargout > 0
            out = objchanman;
        end
        return;
    end
end

% Drop case
msg = lower(msg);

switch msg
    % Window Creation
    case 'create'

        % Make sure we don't already have a valid chan man
        if isa(objchanman,'chantable') && isvalid(objchanman)
            out = objchanman;
            return;
        end
        
        % Create the object
        objchanman = chantable;
        
        if nargout>0
            out = objchanman;
        end
        
        % Total hack here.
        % For some reason, if a channel object is loaded, but no unit
        % objects have been created, the data is not handled correctly.
        % So, we'll create and trash a unit object just so we're sure
        % everything is started correctly.
        x = unit(1,'m'); %#ok<NASGU>
        
    case 'load'
        objchanman.load(varargin{1});
    case 'addchannel'
        for i = 1:length(varargin)
            ch = varargin{i};
            if isa(ch,'channel')
                objchanman.add(ch);
            end
        end
    case 'add'
        chanman('addchannel',varargin{:});
    case 'clearall'
        objchanman.removeall;
    case 'deletechan'
        objchanman.remove(varargin{:});
    case 'save'
        objchanman.save(varargin{1});
    case 'exit'
        objchanman.Exit;
    case 'getchannelselected'
        out = objchanman.selected;
    case 'getchannelbyname'
        out = objchanman.get(varargin{:});
        if length(out) == 1
            out = out{1};
        end
    case 'deletechannelbyname'
        objchanman.remove(varargin{:});
    case 'getchannelall'
        out = objchanman.channels;
    case 'null'
        % Do nothing
    case 'sortchannels'
        objchanman.sort;
        
    % Depricated
    case 'setmessage'
        statusbar(0,varargin{1});
    case 'registerchannelselectcallback'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'clearchannelselectcallback'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'clearone'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'rename'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'edittest'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'reunitchannel'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'reunit'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'getdata'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'getchan'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'channelselect'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'getchannel'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'map'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'import'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'importone'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'export'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'exportone'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'setplotfigure',
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'channeldragstart'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'channeldragstop'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'sortchannels'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'stringtovarname'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    case 'channelrightselect'
        error(['ChanMan command no longer supported: ' upper(msg)]);
    
    otherwise
        % Didn't recognize command
        error(['Unknown command: ' msg]);
end
