%ISCHANNEL  Returns true if input is a channel object.
%

% Doug Harriman (doug.harriman@hp.com)

function [tf] = ischannel(in)
tf = isa(in,'channel');