%HAVEUNITCLASS  Checks to see if there is a unit class.
%

% DLH, 12/22/98

function out = haveunitclass()

if exist('unit/unit') == 2,
   out = 1;
else,
   out = 0;
end
