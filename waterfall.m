%WATERFALL  Generate contour manager data set with frequency waterfall data.
%  CM=WATERFALL(CHANNEL,STEP)  Takes the given CHANNEL data, and creates a
%  contour manager object CM for plotting the time/frequency waterfall
%  plot.  The power spectral denisty for the data is done in non-overlapping 
%  chunks of size STEP.  Default value for STEP is 0.1 seconds.
%
%  See also: PSD, CONMAN.
%

% Doug Harriman (doug.harriman@gmail.com)

function cm = waterfall(ch,step)

% Make sure channel is sampled.
dt = get(ch,'SampleTime');
if ~isscalar(dt)
    dt = diff(dt);
    dt = mode(dt);
    ch = resample(ch,1/dt);
end

% Data chunk size and steps between chunks
% step  = 0.25;
if nargin < 2
    step  = 0.1;
end

% Number of FFT steps.
t = get(ch,'Time');
steps = floor(max(t)/step);

tstart = 0;
z = [];
for i = 1:steps
    % Index of points for this step
    idx = (t > tstart) & (t < tstart + step);

    % PSD
    p = psd(hanning(zeromean(ch(idx))));
    z(:,i) = double(p);
    
    % Update times
    tstart = tstart + step;
end
    
% Get PSD frequency resolution
f = get(p,'freqs');

% Create contour manager.
cm = conman;
cm.z = z;
cm.x = unit(0:step:step*(steps-1),'sec');
cm.y = unit(0:f:f*(length(p)-1),'Hz');
cm.isolabels = false;

% Plot if no outputs.
if nargout > 0
    return;
end
figure
s1; 
plot(ch);
xlim = get(gca,'XLim');
pt;

s2;
zmin = min(min(cm.z));
zmax = max(max(cm.z));
dz = zmax - zmin;
npts = 20;
cm.contour(zmin:dz/npts:zmax);
set(gca,'XLim',xlim);
pt;
xl = get(gca,'XLabel');
str = get(xl,'String');
str = ['Time ' str];
xlabel(str);

yl = get(gca,'YLabel');
str = get(yl,'String');
str = ['Frequency ' str];
ylabel(str);


