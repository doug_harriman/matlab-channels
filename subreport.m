% SUBREPORT  Creates a report of a selected region.

function [] = subreport()

% Go to the first plot on the given figure
fig = gcf;
ch  = findobj(get(fig,'Children'),'Type','axes');
axes(ch(1));

% Get the indecies.
ind = extract;

% Get the channel
chanName = getuprop(gcf,'ChannelName');
chan = evalin('base',chanName);

% Reduce data
chan = chan(ind);
%zeromean(chan);

% Create the new report
report(chan);


