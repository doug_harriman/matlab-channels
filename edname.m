%EDNAME  Edit channel name.
%
% >> edname(channel)
%

function [] = edname(chan)

% Setup for dialog box
prompt  = 'Channel Name:';
default = {get(chan,'name')};
title   = 'Name Change';
lineNo  = 1;

% Get and set
newName = inputdlg(prompt,title,lineNo,default,'on');
if ~isempty(newName),
   set(chan,'name',newName{1});
   assignin('caller',inputname(1),chan) ;
end