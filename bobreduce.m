% bobreduce.m
% Implements Bob C's wonderfilter as a resampling filter.
%

function out = bobreduce(in,factor) ;

% Get channel data
out = in;
in  = get(in,'data')';

% Filter coefficients
coeff = [-1 -5 -5 20 70 98 70 20 -5 -5 -1]/256 ;

% Apply filter window
j=1;
for i = 6:length(in)-5,
   if ~rem(i,factor)
      out_data(j) = sum(in(i-5:i+5).*coeff) ;
      j=j+1;
   end
end

% Set up output
set(out,'samplerate',get(out,'samplerate')/factor);
set(out,'data',out_data);