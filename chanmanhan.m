%CHANMANHAN  Returns handle to the Channel Manager Figure
%

% Doug Harriman
% Hewlett-Packard VCD
% doug_harriman@hp.com
% (360) 212-6522
% May 11, 1998

function [handle] = chanmanhan()

set(0,'ShowHiddenHandles','on');
handle = findobj(get(0,'children'),'Tag','ChanManWindow');
set(0,'ShowHiddenHandles','off');
