%TURBINE_IMPORT  Import turbine data CSV file.

function turbine_import(fn)

if nargin < 1
    [fn,pn] = uigetfile('*.csv');
    if fn == 0
        warning('No file selected.');
        return;
    end
    fn = [pn fn];
end
[~,tmp] = fileparts(fn);
disp(['Loading file: ' tmp]);

try 
    d = importdata(fn);
catch %#ok<CTCH>
    error(['Unable to process file: ' fn]);
end

% Get fields
if isfield(d,'colheaders')
    fields = d.colheaders;
else
fields = d.textdata(1,2:end);
fields = cellfun(@(x)strtrim(x),fields,'UniformOutput',false);
end

% Error checks
n_fields = length(fields);
if n_fields ~= size(d.data,2)
    error('Field names don''t match data');
end

% Generate new data structure.
S = struct;
for i = 1:n_fields
    field = strtrim(fields{i});
    S.(field) = d.data(:,i);
end

% Exract time.
tickfield = 'ticks';
ticks_per_second = 977;
assert(isfield(S,tickfield));
tic  = S.(tickfield);
time = tic;
time = time - time(1);  % Start at zero
time = time / ticks_per_second;
time = unit(time,'sec');
time = convert(time,'min');

% Handle tic rollover
dt = diff(time);
idx = find(dt < 0);
for i = 1:length(idx)
    time(idx(i)+1:end) = time(idx(i)+1:end) - dt(idx(i)) + unit(1,'sec');
end

S = rmfield(S,tickfield);

% Grab metadata.
meta = chanmeta(fn);

% Channel unit mapping.
ud = dict;
ud('wind_spd_1')  = 'm/s';
ud('turbine_rpm') = 'RPM';
ud('p_inverter1') = 'W';
ud('p_inverter2') = 'W';
ud('vdc_bus')     = 'V';
ud('vdc_inst')    = 'V';
ud('vboost')      = 'V';
ud('vboost_inst') = 'V';
ud('inverter_power') = 'W';
ud('idc_bus')        = 'A';
ud('idc_inst')       = 'A';
ud('prog_power')     = 'W';
ud('prog_power_slew')= 'W';    % With slew rate applied
ud('wind_direction') = 'deg';
ud('iac_phase1')     = 'A';
ud('dc_pwr_ten_sec') = 'W';
ud('outpower_inst')  = 'W';
% ud('') = '';

% Name cleanup
nd = dict;
nd('wind_spd_1')  = 'Wind Speed';
nd('turbine_rpm') = 'Rotor Speed';
nd('p_inverter1') = 'Inverter 1 Output';
nd('p_inverter2') = 'Inverter 2 Output';
nd('vdc_bus')     = 'DC Bus Voltage';
nd('vdc_inst')    = 'DC Bus Voltage';
nd('vboost')      = 'Boost Voltage';
nd('vboost_inst') = 'Boost Voltage, Inst.';
nd('inverter_power') = 'Total Inverter Output';
nd('idc_bus')        = 'DC Bus Current';
nd('idc_inst')       = 'DC Bus Current, Inst.';
nd('pwm_ratio')      = 'DL PWM';
nd('turbine_state')  = 'Turbine State';
nd('prog_power')     = 'Boost Target Power';
nd('prog_power_slew')= 'Boost Target Power, Slew Limited';
nd('wind_direction') = 'Wind Direction';
nd('sound_pl')       = 'Sound Pressure Level';
nd('iac_phase1')     = 'Phase A Current';
nd('hs_temp')        = 'Heat Sink Temperature';
nd('dc_pwr_ten_sec') = 'DC Bus Power, 10 sec avg';
nd('outpower_inst')  = 'Inverter Power Sum, Inst.';

% nd('') = '';

% Create channels.
fields = fieldnames(S);
chanman;
chanman('ClearAll');
for i = 1:length(fields)
    field = fields{i};
    
    % Get channel units
    ustr = '';
    if ud.iskey(field)
        ustr = ud(field);
    end
    
    % Get clean name
    name = field;
    if nd.iskey(field)
        name = nd(field);
    end
    
    ch = channel(name,S.(field),time,ustr,...
        'Metadata',meta);
    chanman('AddChannel',ch);
end
    
% Tics
ch = channel('tic',tic,time,'',...
    'Metadata',meta);
chanman('AddChannel',ch);

% Output power.
p1 = chanman('GetChannelByName','Inverter 1 Output');
assert(~isempty(p1));
p2 = chanman('GetChannelByName','Inverter 2 Output');
ptot = p1+p2;
set(ptot,'Name','Inverter Power Sum');
chanman('AddChannel',ptot);

% DL Power
pwm = chanman('GetChannelByName','DL PWM');
vdc = chanman('GetChannelByName','DC Bus Voltage');
if ~any([isempty(pwm) isempty(vdc)])
    % DL power
    dl_pwr = (pwm/100)*(vdc^2)/unit(6.6,'Ohm');
    dl_pwr = convert(dl_pwr,'W');
    
    dl_pwr = set(dl_pwr,'Name','DL Power');
    dl_pwr = set(dl_pwr,'PlotColor','k');
    chanman('AddChannel',dl_pwr);
end
    
% Wind direction cleanup.
% ch = 'Wind Direction';
% d = chanman('GetChannelByName',ch);
% chanman('DeleteChannelByName',ch);
% d = unwrap(d);
% d = set(d,'Name',ch);
% chanman('AddChannel',d);
% 

% Wind shear.
return;
path = which('applyshear');
if ~isempty(path)
    applyshear;
end
