%FLEXTOOL  Reads Flextool tracestore file and writes channels 
%          to the channel manager.
%

% DLH
% 11/13/02 - Error handling for files with no data.
% 10/18/02 - Got tic fixing correct.
% 05/13/02 - Created

function [chanmetadata] = flextool(fileName,uiHandle)

% UI stuff
if nargin < 2,
    uiHandle = 0;
end


% If not provided a file name, first look in current dir, then at c:\
if (nargin < 1) || (fileName == 0),
    % Try this dir
    thisPath = cd;
    dirInfo = dir('*.trc');
    
    if isempty(dirInfo),
        UIMessage(uiHandle,'Looking for TRC file in default location.');
        % Try default dir
        thisPath = cd;
        if ispc
            cd('c:\')
        end
        if isunix
            cd('~');
        end
        dirInfo = dir('*.trc');
    end

    % If didn't find anything in default dir, go back to working dir
    if isempty(dirInfo),
        UIMessage(uiHandle,'None found.  Returning to working dir.');
        cd(thisPath)
        dirInfo = dir('*.trc');
    end
    
    
    % If number of choices == 1, then open it, otherwise, show dialog.
    if length(dirInfo) ~= 1,
        [fileName,filePath] = uigetfile('*.trc','Flextool Tracestore File');
        if fileName == 0,
            return;
        end
        fileName = [filePath fileName];
    else
        % Found name
        fileName = [cd filesep dirInfo.name];
    end
    
    % Return to original dir
    cd(thisPath);
end

% Load the data the easy way.
UIMessage(uiHandle,'Loading TRC file data.');
data = importdata(fileName);

% Error check for files with no actual data
if ~isfield(data,'data'),
    % Throw up an error dialog and return
    errordlg(['File: "' fileName '" contains no data.'],...
        'Flextool TRC File Error');
    return;
end

% Parse any test meta data 
chanmetadata = metadata(data);
[temp_path,temp_name,temp_ext] = fileparts(fileName);
chanmetadata.file_name = [temp_name temp_ext];

% Provide minimal chanmetadata
if ~isstruct(chanmetadata),
    % Define the test info
    d = dir(fileName);
    file_date = datenum(d.date);
    chanmetadata.date = datestr(file_date,23);
    chanmetadata.time = datestr(file_date,16);
end

% Last point is usually bogus, so trim it.
data.data = data.data(1:end-1,:);

% Quick & dirty parse, need to improve.
UIMessage(uiHandle,'Fixing TIC data.');
numVars = size(data.data,2)-2;
tic = data.data(:,1)/1e6;

% Fix timer rollover
ind = find(diff(tic) < 0);
if ~isempty(ind),
    for i = 1:length(ind),
        tic(ind(i)+1:end) = tic(ind(i)+1:end) + tic(ind(i)) - tic(ind(i)+1);
    end
end
tic = tic - tic(1);

% Use time offset if present.
if isfield(chanmetadata,'test_start'),
    % Use test time info for synchronization with other data sources.
    [temp1,temp2,temp3,hourVal,minVal,secVal] = datevec(chanmetadata.test_start);
    tic = tic + hourVal*3600 + minVal*60 + secVal;
end

% Generate our own column headers if needed
if ~isfield(data,'colheaders')
    if isfield(data,'textdata')
        header = data.textdata{1};
        i = 1;
        while(~isempty(header))
           [col{i},header] = strtok(header);
           i = i + 1;
        end
        %col = col(3:end);
        data.colheaders = col;
    end
end

% Warn user if won't be able to use real variable names.
UIMessage(uiHandle,'Creating channels.');
if ~isfield(data,'colheaders')
    msg = {'Flexmech tracestore variable names do not match data.',...
            'This occurs if FHX file does match ALL file.',...
            'Regenerate ALL file to fix.',...
            'Default variable names will be used.'};
    uiwait(warndlg(msg,'Flextool Warning','modal'));
    drawnow;
end

id = data.data(:,2);
id = unique(id);
if length(id) > 1, error('Tracestore file contains data from multiple recorders.'); end

% ID to recorder map
idLib = [93 95 100 117 408 882 1760 1835];
if ~any(id==idLib), error(['Unknown component ID: ' num2str(id)]); end
recLib = {'fm','carriage','paper','service','pick','thermal_model','dryer','mi_servo'};
recorder = recLib{find(id==idLib)};
chanmetadata.axis = recorder;

% Conversions
switch recorder
    case 'carriage'
        % If synchronizing data, use the tic value
        if isfield(chanmetadata,'test_start'),
            ts = tic; 
        else
            ts = 1/1200;
        end
        posUnits = 'cEU';
        
    case 'paper'
        ts = tic;
        posUnits = 'pEU';    
        
    case 'service'
        ts = tic;
        posUnits = 'sEU';
        
    case 'thermal_model'
        ts = tic;
        
    case 'fm'
        ts = tic;
        
    case 'dryer'
        ts = tic;
        
    case 'pick',
        ts = tic;

    case 'mi_servo',
        ts = tic;
        
otherwise
        error('Unknown tracestore ID.');
end

for i=1:numVars,
    ind = i+2;
    if isfield(data,'colheaders'),
        var{i}.name = data.colheaders{ind};
    else
        var{i}.name = ['Variable ' num2str(i)];
    end
    var{i}.data = data.data(:,ind);
end

% Create channels.
chanman;
for i=1:numVars,
    % Remove '_' from names
    name = var{i}.name;
    ind  = find(name == '_');
    name(ind+1) = upper(name(ind+1));
    name(ind) = '';
    
    % Clean up name
    name = strrep(name,'.',' ');
    
    chan = channel(name,var{i}.data,tic,'','metadata',chanmetadata);
    chanman('AddChannel',chan);
end

pos = chanman('GetChannelByName','ServoInfoPosition');

% No additional channels for the service recorder
if strcmp(recorder,'service'),
    pos = channel('Actual Position',get(pos,'data'),ts,posUnits,...);
        'metadata',chanmetadata);
    chanman('AddChannel',pos);

    % Vel in physical units
    vel = diff(pos);
%     vel = convert(vel,'IPS');
    set(vel,'Name','Actual Velocity');
    chanman('AddChannel',vel);

    cpos = chanman('GetChannelByName','ServoInfoPositionCmd');
    cpos = channel('Commanded Position',get(cpos,'data'),ts,posUnits,...
        'PlotColor','r',...
        'MetaData',chanmetadata);
    chanman('AddChannel',cpos);
    
    cvel = diff(cpos);
%     cvel = convert(cvel,'IPS');
    set(cvel,'Name','Commanded Velocity');
    set(cvel,'PlotColor','r');
    chanman('AddChannel',cvel);
    
    
    return;
end

if(isa(pos,'channel'))
     offset = 0; %pos(1);
     pos = pos - offset;
    pos = channel('Actual Position',get(pos,'data'),ts,posUnits,...);
        'metadata',chanmetadata);
    chanman('AddChannel',pos);
    
    pos = convert(pos,'in');  
    chanman('AddChannel',pos);
    
    % Vel in physical units
    vel = diff(pos);
    vel = convert(vel,'IPS');
    set(vel,'Name','Actual Velocity');
    chanman('AddChannel',vel);
    
    switch recorder
        case 'carriage'
            acc = diff(vel);
            acc = convert(acc,'G');
            acc = bobfilt(acc);
            set(acc,'Name','Actual Acceleration');
            set(acc,'PlotColor','g');
            chanman('AddChannel',acc); 
            acc = acc*10;
            set(acc,'Name','Actual Acceleration*10');
            chanman('AddChannel',acc);
        case 'paper'
            % Vel in controller units
            convert(pos,'pEU');
            mvel = channel('Measured Velocity',diff(double(pos)),...
                    ts(1:end-1),'pEU/pInt','MetaData',chanmetadata);
            chanman('AddChannel',mvel);
         
        otherwise
            error('Unknown tracestore ID.');
    end
end

cpos = chanman('GetChannelByName','ServoInfoPositionCmd');
if(isa(cpos,'channel'))
    cpos = cpos - offset;
    chanmetadata.axis = recorder;
    cpos = channel('Commanded Position',get(cpos,'data'),ts,posUnits,...
        'PlotColor','r',...
        'MetaData',chanmetadata);
    chanman('AddChannel',cpos);
    
    cvel = diff(cpos);
    cvel = convert(cvel,'IPS');
    set(cvel,'Name','Commanded Velocity');
    set(cvel,'PlotColor','r');
    chanman('AddChannel',cvel);
    
    switch recorder
        case 'carriage'
            cacc = diff(cvel);
            cacc = convert(cacc,'G');
            
            data = double(cacc);
            data(abs(data)>10) = NaN;
            set(cacc,'Data',data);
            
            set(cacc,'Name','Commanded Acceleration');
            set(cacc,'PlotColor','r');
            chanman('AddChannel',cacc); 
        
        case 'paper'
            convert(cpos,'pEU');
            cvel = diff(double(cpos));
            cvel = channel('Target Velocity',cvel,ts(1:end-1),'pEU/pInt',...
                    'MetaData',chanmetadata);
            set(cvel,'PlotColor','r');
            chanman('AddChannel',cvel);
    end    
end

if isa(cpos,'channel') && isa(pos,'channel'),
    epos = cpos-pos;
    set(epos,'Name','Position Error');
    chanman('AddChannel',epos);
    
    evel = diff(epos);
    set(evel,'Name','Velocity Error');
    convert(evel,'IPS');
    chanman('AddChannel',evel);
end

switch recorder
    case 'carriage'
        pwm = chanman('GetChannelByName','ServoInfoPwm');
    case 'paper'
        pwm = chanman('GetChannelByName','ServoInfoHwPwm');
    case 'service'
        pwm = chanman('GetChannelByName','ServoInfoPwm');
end

if exist('pwm','var') && isa(pwm,'channel')
    pwm = channel('PWM Value',get(pwm,'data'),ts,'','MetaData',chanmetadata);
    pwm = set(pwm,'PlotColor','k');
    chanman('AddChannel',pwm);
    pwm = pwm/10;
    chanman('AddChannel',pwm);
end

switch recorder
    case 'carriage'
        pwm_cap = chanman('GetChannelByName','ServoInfoPwmCap');
end

if exist('pwm_cap','var') && isa(pwm_cap,'channel')
    pwm_cap = channel('PWM Cap Value',get(pwm_cap,'data'),ts,'','MetaData',chanmetadata);
    pwm_cap = pwm_cap * sign(pwm);
    pwm_cap = set(pwm_cap,'Name','PWM Cap Value');
    pwm_cap = set(pwm_cap,'PlotColor','r');
    chanman('AddChannel',pwm_cap);
    pwm_cap = pwm_cap/10;
    chanman('AddChannel',pwm_cap);
end

sneak = chanman('GetChannelByName','SneakInfoCmdOffset');
if isa(sneak,'channel')
    svel = diff(double(sneak));
    sneak = channel('Sneak Cmd Vel',svel,ts(1:end-1),'pEU/pInt',...
        'PlotColor','k',...
        'MetaData',chanmetadata);
    chanman('AddChannel',sneak);
end

state = chanman('GetChannelByName','ServoInfoState');
if isa(state,'channel')
    state = channel('State',get(state,'data'),ts,'','MetaData',chanmetadata);
    chanman('AddChannel',state);
    state = state*10;  % For plotting.
    chanman('AddChannel',state);
    
    % Correct Velocity Errors for 300Hz states
    if isa(vel,'channel'),
        v = double(vel);
        s = double(state);
        v(s==1) = v(s==1)/4;
        v = unit(v,'IPS');
        set(vel,'data',v);
        chanman('DeleteChannelByName',get(vel,'Name'));
        chanman('AddChannel',vel);
        
    end
end

trigger = chanman('GetChannelByName','ServoInfoTrigger');
if isa(trigger,'channel')
    trigger = channel('Trigger',get(trigger,'data'),ts,'','MetaData',chanmetadata);
    chanman('AddChannel',trigger);
end

slewPWMSum = chanman('GetChannelByName','SlewPwmSum');
if isa(slewPWMSum,'channel')
    slewPWMSum = channel('Slew PWM Sum',get(slewPWMSum,'data'),ts,'',...
        'MetaData',chanmetadata);
    chanman('AddChannel',slewPWMSum);
end


% Temperature Estimation
power = chanman('GetChannelByName','power');
if isa(power,'channel'),
    % Convert onboard power estimate to W
    p = get(power,'data');
    p = p/2^6;
    power = channel('Winding Power Dissipation',p,5,'W','MetaData',chanmetadata);
    chanman('AddChannel',power);
end

temp = chanman('GetChannelByName','caseTemperatureRise');
if isa(temp,'channel');
    t = get(temp,'data');
    t = t/2^6;
    temp = channel('Estimated Case Temperature Rise',t,5,'Metadata',chanmetadata);
    chanman('AddChannel',temp);
end

velSum = chanman('GetChannelByName','VelSum');
if isa(velSum,'channel'),
    moveCnt = chanman('GetChannelByName','MoveCount');
    if isa(moveCnt,'channel'),
        speed = velSum/moveCnt;
        set(speed,'Name','Move Speed');
        set(speed,'Units','IPS');
        chanman('AddChannel',speed);
    end
end

% Computed Torque Vars
cmdveldelayed = chanman('GetChannelByName','CmdVelFilteredDelayed');
if isa(cmdveldelayed,'channel')
    cmdveldelayed = channel('Cmd Vel Filtered Delayed',...
        double(cmdveldelayed),...
        ts,...
        'cEU/cInt',...
        'MetaData',chanmetadata);
    
    vel = chanman('GetChannelByName','Actual Velocity');
    if(isa(vel,'channel'))
%         cmdveldelayed = cmdveldelayed * sign(vel);
        set(cmdveldelayed,'Name','Cmd Vel Filtered Delayed');
    end
    set(cmdveldelayed,'PlotColor','r');
    chanman('AddChannel',cmdveldelayed);
    
    cmdveldelayed = convert(cmdveldelayed,'IPS');
    chanman('AddChannel',cmdveldelayed);
end    

ve = chanman('GetChannelByName','FbVelErr');
if isa(ve,'channel')
    ve = channel('Velocity Error',...
        double(ve),...
        ts,...
        'cEU/cInt',...
        'MetaData',chanmetadata);
    chanman('AddChannel',ve);
end    

ve = chanman('GetChannelByName','FbVelErrInt');
if isa(ve,'channel')
    ve = channel('Velocity Error Integral',...
        double(ve),...
        ts,...
        'cEU',...
        'MetaData',chanmetadata);
    chanman('AddChannel',ve);
end    


% If no output, don't pass chanmetadata
if nargout == 0,
    clear chanmetadata
end


% End main function


% User Interface messages
function [] = UIMessage(handle,msg)

if handle,
    if handle == -1,
        chanman('SetMessage',msg);
    else
        set(handle,'string',msg);
    end
else
    disp(msg);
    drawnow
end   