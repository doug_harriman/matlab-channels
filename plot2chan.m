%PLOT2CHAN  Creates a channel from plot data.
%

% DLH
% 09/07/05 - Created

function [chan] = plot2chan()

% Get currently selected line.
han = gco;

% If don't have one, error out
if ~strcmp(get(han,'type'),'line'),
    error('Please select a line on a plot.');
end

% Extract data
time = get(han,'xdata');
data = get(han,'ydata');
name = get(han,'DisplayName');
if isempty(name),
    name = 'Plot Data';
end

axhan = get(han,'parent');
ylabel = get(get(axhan,'ylabel'),'string');
re = '\[\w*\**\/*\w*\]';
str = regexp(ylabel,re,'match');
if isempty(str),
    units = '';
else
    str = str{1};
    units = str(2:end-1);
end

chan = channel(name,data,time,units);

% Export if no output
if nargout == 0,
    assignin('base','chan1',chan);
    clear chan;
end