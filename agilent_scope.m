%AGILENT_SCOPE Imports CSV data file from Agilent Scopes.
%

% DLH
% 11/30/04 - Created

function [] = agilent_scope(file_name)

% Error check inputs
if nargin > 1, error('Too many inputs.'); end

% If file name provided, error check.
if nargin == 1,
    if ~isstr(file_name), error('File name must be a string.'); end
    if exist(file_name) ~= 2, error(['File not found: ' file_name]); end
end

% If no file name, prompt user
if nargin == 0,
    [file_name, pathName] = uigetfile('*.csv','Select Agilent Scope Data File');

    if file_name == 0,
        error('No file selected.');
    end

    file_name = [pathName filesep file_name];
end

% Open the file
fid = fopen(file_name,'r');
if fid < 0,
    error(['Unable to open file: ' file_name]);
end

% Read header info.
% Last line of header is "Data:"
% Header fields of form: <label>: <value>
line = fgetl(fid);
line_num = 1;
config = [];
while 1
    % Parse the header line
    if line == -1,
        % Hit EOF inside header, just bail.
        error(['End of file found before data in file: ' file_name]);
    end

    % Break header line into label and value
    colon_ind = find(line == ':');
    if isempty(colon_ind),
        error(['Unknown data format file "' file_name '", line ' num2str(line_num)]);
    end
    colon_ind = colon_ind(1);
    label = line(1:colon_ind-1);
    value = line(colon_ind+1:end);

    % Clean up label names
    label(label==' ') = '_';

    % Bail when we get to the data field.
    if strcmp(label,'Data'),
        break;
    end

    % Clean up value
    value = deblank(fliplr(deblank(fliplr(value))));
    temp  = str2num(value);
    if ~isempty(temp), value = temp; end

    % Assign config structure fields of name <label> with value of <value>.
    config = setfield(config,label,value);

    % Read next line
    line = fgetl(fid);
    line_num = line_num + 1;
end

% Read the data
% Pre-allocate data storage
if isfield(config,'Points'),
    num_pts = getfield(config,'Points');
else,
    num_pts = 2^16;
end
data = zeros(num_pts,2);

% Read loop
pnt_cnt = 0;
for i=1:num_pts,
    line = fgetl(fid);

    % Bail early if hit end of file
    if line == -1,
        break;
    end
    pnt_cnt = pnt_cnt + 1;

    % Extract the numbers
    data(i,:) = str2num(line);
end

% Close the file
fclose(fid);

% Shorten data if needed
if pnt_cnt < num_pts,
    data = data(1:pnt_cnt,:);
end

% Handle time information.
% Make sure time in seconds
if isfield(config,'XUnits'),
    if ~strcmp(config.XUnits,'second'),
        error(['Unkown time units: ' config.XUnits]);
    end
end

% Use sample time instead of time vector if available.
if isfield(config,'XInc'),
    time = config.XInc;
else
    % See if there is low varition in sample rate.
    time = data(:,1);
    dt   = diff(time);
    if std(dt)/mean(dt) < 1e-6,
        % There is low variation, so just use average time.
        time = mean(dt);
    end
end

% Data units 
unit_str = '';
if isfield(config,'YUnits'),
    unit_str = config.YUnits;

    % Quick and dirty unit name conversion dictionary
    unit_list.Ampere  = 'A';
    unit_list.AmpereV = 'W';
    unit_list.Volt    = 'V';

    % Convert Agilent unit string to Units Toolbox unit symbol string.
    if isfield(unit_list,unit_str),
        unit_str = getfield(unit_list,unit_str);
    end
end

% Create data channel
test.type = 'Agilent Scope';
test.name = '';
if isfield(config,'Date'), meta.date = config.Date; end
if isfield(config,'Time'), meta.time = config.Time; end

[pathstr,name] = fileparts(file_name);
chan = channel(name,data(:,2),time,unit_str,'MetaData',meta);
chanman('AddChannel',chan);