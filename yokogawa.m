%YOKOGAWA  Imports data from Yokogawa scope to channels.
%

% To Do:
% - Data scaling (volts to whatever units)
% - Test data/time to channel

% DLH
% 08/28/03 - Modified from original script for decreased load time.

function [] = yokogawa(fileName,uiHandle)

% File name
if nargin < 1, fileName = []; end

% UI 
if (nargin<2), uiHandle=0; end

% Argument Check
if isempty(fileName),
   [fileName,pathName] = uigetfile('*.asd','Load Yokogawa Data File');
   
   if fileName == 0,
      % User cancelled
      UIMessage(uiHandle,'No file selected');
      return;
   end
   fileName = [pathName fileName];
end

UIMessage(uiHandle,['Loading data file: ' fileName]);

% Load file
fid = fopen(fileName,'r');
if fid == -1,
   error(['Unable to open data file: ' fileName]);
end

% Read the first 14 Lines to extract metadata
headerlines = 14;
for i = 1:headerlines,
   line{i} = fgetl(fid);
end
fclose(fid);

% Parse the header lines for important metadata
% Default metadata values.
numVars    = 0;
traceName  = {};
sampleRate = 0;
testDate   = '';
testTime   = '';
for i = 1:headerlines,
   ind = findstr(line{i},'"');
   fieldName = lower( line{i}(ind(1)+1:ind(2)-1) );
   
   switch fieldName,
      case 'tracename',
         numVars = length(ind)/2 - 1;
         for j = 1:numVars,
            traceName{j} = line{i}(ind(j*2+1)+1:ind(j*2+2)-1);
            traceName{j} = lower(deblank(traceName{j}));
            traceName{j}(1) = upper(traceName{j}(1));
         end
         
      case 'samplerate'
         tempInd = findstr(line{i},',');
         sampleRate = line{i}(tempInd(1)+1:tempInd(2)-1);
         sampleRate = str2num(sampleRate);
         
      case 'date'
         testDate = line{i}(ind(3)+1:ind(4)-1);
         
      case 'time'
         testTime = line{i}(ind(3)+1:ind(4)-1);

   end
end

% Error check metadata
if numVars == 0, error('No variable definitions found.'); end
if sampleRate == 0, error('No sample rate definition found.'); end
if isempty(traceName), error('No trace names found.'); end

% Create the parse string for the fast data loader.
outVar = '[';
parseVar = '';
for i = 1:numVars,
   outVar = [outVar 'data{' num2str(i) '}'];
   parseVar = [parseVar '%f'];
   
   if i<numVars,
      outVar = [outVar ','];
   end
end
outVar = [outVar ']'];

% Build up function call string
fcnStr = [outVar '=textread(''' fileName ''',''' parseVar ''',''delimiter'','','',''headerlines'',' num2str(headerlines+1) ');'];

% Load the data
try
   eval(fcnStr);
catch
   error(['Unable to read numeric data from file: ' fileName]);
end


% Yokogawa ASD (ASCII Data) file output.
% Outputs raw voltage measured.  Has nothing to do with
% [V/div] setting on screen.  That is for screen only.
% For current measurement, need to figure out the voltage
% output of the current amplifier [V/A].
ampSetting = inputdlg('Current/Division Setting [A/div]','Current Amplifier Setup');
if isempty(ampSetting),
   UIMessage(uiHandle,'Invalid Current Amplifier Setup');
	return;   
end

ampSetting = str2num(ampSetting{1});  % [A/div]
scopeSetting = 10; % [mV/div] as requested by current amplifier.
scopeSetting = scopeSetting /1000;  % [V/div] unit conversion.
amps_per_volt = ampSetting/scopeSetting; % [A/V]


% Vibrac torque heads put out 10V full range.  Need to know the size of the
% torque head to know what the calibration factor is.
torqueHead = inputdlg('Torque Head Size [oz*in]','Torque Transducer Used');
if isempty(torqueHead),
   UIMessage(uiHandle,'Invalid Torque Head Specification');
   return;
end

torqueHead = str2num(torqueHead{1}); % [oz*in]
ozin_per_volt = torqueHead/10;  % Full scale is 10 V.



% Build channels
UIMessage(uiHandle,'Building channels...');
for i=1:numVars,
   
   chanUnits = '';
   switch lower(traceName{i}),
      case 'torque'
         chanUnits = 'oz*in';
         data{i} = data{i} * ozin_per_volt;
      case 'current'
         chanUnits = 'A';
         data{i} = data{i} * amps_per_volt;
   end
   
   chan = channel(traceName{i},data{i},1/sampleRate,chanUnits); %,testDef);
% 	UIMessage(uiHandle,'Filtering Data...');
% 	chan=bobfilt(chan);
	chanman('AddChannel',chan);
end   
   
% Return to original path
UIMessage(uiHandle,'Done importing Yokogawa data file');

%-------------------------------------------------------------------------
% User Interface messages
function [] = UIMessage(handle,msg)

if handle,
    if handle == -1,
        chanman('SetMessage',msg);
    else,
        set(handle,'string',msg);
    end
else,
    disp(msg);
    drawnow
end   