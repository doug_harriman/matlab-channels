% delta  Returns distance in x and y between two points.
% Delta will accept 'x' or 'y' as an argument
% if only one coordinate distance is desired.

function [delx, dely] = delta(instr)

% Get points
disp('Select First Point')
[x1,y1,han1] = getpoint ;
disp('Select Second Point')
[x2,y2,han2] = getpoint ;

if nargin == 1,
   % The use wants just one output
   if strcmp(instr,'x'),
      delx = x2 - x1 ;
   elseif strcmp(instr,'y'),
      delx = y2 - y1 ;
   else,
      error('Unknown message')
   end
else,
   % User wants both
   delx = x2 - x1 ;
   dely = y2 - y1 ;
   
   % Provide 1 or 2 outputs
   if nargout < 2,
      delx = [delx dely] ;
   end
end

% Clear plotted text
delete(han1,han2);

