%LSIM Uses a channel for input to a linear system.
%     See 'help lsim' for more information.
% LSIM(CHAN,SYS,X0) Simulates 'SYS' with 'CHAN' as
% the input.
%

% DLH 7/9/98

function [out] = lsim(in,sys,X0)

% Extract data and simulate
if nargin < 3
    X0 = []; 
end

% Handle discrete and continuous time systems appropriately
if isct(sys)
    % Continuous
    y = lsim(sys,double(get(in,'data')),get(in,'time'),X0);
    out = channel('Simulation output',y',get(in,'SampleTime'));

else
    % Discrete
    ts = get(sys,'ts');
    in = resample(in,1/ts);
    
    u = double(get(in,'data'));
    t = [0:ts:ts*(length(u)-1)]';
        
    y = lsim(sys,u,t,X0);
    out = channel('Simulation output',y',ts);
end
    
if nargout == 0,
    plot(out);
    clear out;
end