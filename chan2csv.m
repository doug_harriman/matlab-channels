%CHAN2CSV  Writes channel data to a CSV file.
%  CHAN2CSV(FILE_NAME,  CHAN1);
%  CHAN2CSV(FILE_NAME,{CHAN1,CHAN2...});

function [] = chan2csv(filename,chan)

% Error check inputs
if nargin ~= 2, error('Two inputs expected.'); end
if ~ischar(filename), error('File name must be specified by a string.'); end
if ~iscell(chan), 
    chan = {chan};
end

% Make sure got channels.
n_chan = length(chan);
for i = 1:n_chan,
    if ~isa(chan{i},'channel'),
        error('All data passed must be channel objects.');
    end
end

% All channels must have same number of data points.
npts = Inf;
for i = 1:n_chan
    % Get point count
    npts = min(npts,length(chan{i}));
end
for i = 1:n_chan
    % Shorten channel if needed.
    if length(chan{i}) > npts
        % UI
        disp(['Reducing length of channel: ' get(chan{i},'Name')]);
        
        % shorten
        chan{i} = chan{i}(1:npts);
    end
end

% All channels must have same timebase
time = get(chan{1},'Time');

% Loop on all channels to figure out how many points to write
n_pts  = Inf;
for i = 1:n_chan
    n_pts = min(n_pts,length(chan{i}));
end

% Make sure all channels have same time base.
time = time(1:n_pts);
for i = 1:n_chan
    t_check = get(chan{i},'Time');
    t_check = t_check(1:n_pts);
    err = abs(time - t_check);
    if any(err > 2*eps),
        error('All channels must have same time base');
    end
end

% Form a matrix of time vector and all channel data
M = zeros(n_pts,n_chan+1);
M(:,1) = reshape(time,n_pts,1);
header = 'Time [sec],';
for i = 1:n_chan
    % Append data
    data = double(chan{i});
    data = data(1:n_pts);
    data = reshape(data,n_pts,1);
    M(:,i+1) = data;
    
    % Append header
    str = char(chan{i});
    unit_str = ['[' get(chan{i},'Units') ']'];
    str = [str ' ' unit_str ','];
    header = [header str];
end
header(end) = '';    

% Remove any NaN's
M(isnan(M)) = 0;

% Write the file
fid = fopen(filename,'w');
if fid<0, error(['Unable to open file for writing:' filename]); end
fprintf(fid,'%s\n',header);
fclose(fid);
dlmwrite(filename,M,'-append');

