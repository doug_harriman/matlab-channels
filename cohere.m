%COHERE  Coherence and frequency response functions.
%
% >>[gamma,H1,H2] = cohere(input,output,FFT_Size)
%
% input, output := Input and Output signals, respectively.
% gamma         := coherence of the two signals.
% H1            := Frequency response function, H1(jw) = Suy/Suu.
% H2            := Frequency response function, H2(jw) = Syy/Syu.
% FFT_Size      := Size of FFT to use, see 'spectave' for default.
%

% DLH, 11/09/98

function [gamma, H1, H2] = cohere(in,out,fftSize)

% Do FFT's
if nargin < 3,
   Sy = spectave(out) ;
   Su = spectave(in)  ;
else,
   Sy = spectave(out,fftSize) ;
   Su = spectave(in,fftSize)  ;
end   

% Calculate cross- and auto- spectra.
Syu = (Sy*conj(Su)) ;
Suy = (Su*conj(Sy)) ;
Syy = (Sy*conj(Sy)) ;
Suu = (Su*conj(Su)) ;

% Calculate coherence function
gamma = (abs(Syu)^2)/Syy/Suu ;
set(gamma,'Name',['Coherence between ' get(in,'name') ' & ' get(out,'name')]) ;

% Calculate the FRF's
if (nargout > 1) | (nargout == 0),
   H1 = Suy/Suu; set(H1,'Name',['H1 FRF: ' get(in,'name') '->' get(out,'name')]);
   H2 = Syy/Syu; set(H1,'Name',['H2 FRF: ' get(in,'name') '->' get(out,'name')]);
end

