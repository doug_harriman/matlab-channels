%DRAGONFLY   Reads a DragonFly query file and writes channels
%            to the channel manager.
%
%>>dragonfly(queryFileName,testName, uiHandle)
%
% All arguments are optional.  If 'queryFileName' is not
% specified, the user will be prompted for a name.
% If a uiHandle is specified, all output will be written
% to the given UI element.  If not specified, message
% will be written to the Matlab window.
% 

% DLH

function [] = dragonfly(fileName,testName, uiHandle);

% UI
if (nargin<3)
   uiHandle=0;
end

if (nargin<2),
    testName=[];
end

if isstr(uiHandle),
   keyboard
end

if nargin == 0,
   fileName = [];
end

% Haven't opened the file yet.
fid = 0;

% Default values
haveTic = 0;

% Open the dragonfly file
thisPath = cd;
if isempty(fileName),
   thisPath = cd;
   
   defaultPath = 'c:\';
   UIMessage(uiHandle,['Looking for Dragonfly Query file in ' defaultPath]);
   cd(defaultPath);
   
   % See if there is more than one file to chose from
   dirInfo = dir('*.qry');
   
   if length(dirInfo) > 1,
      [fileName, filePath] = uigetfile('*.qry;*.dat','DragonFly Query File');
      if fileName == 0,
         UIMessage(uiHandle,'No file selected.');
         % Return to the previous path
         cd(thisPath);
         return;
      end
      
   elseif length(dirInfo) == 1,,
      UIMessage(uiHandle,'File found at default location');
      % Get file info
      fileName = dirInfo(1).name;
      filePath = cd;%[cd '\'];
      
   else,
      cd(thisPath);
      UIMessage(uiHandle,['Looking for DragonFly Query files in ' thisPath]);
      [fileName, filePath] = uigetfile('*.qry;*.dat','DragonFly Query File');
      if fileName == 0,
         UIMessage(uiHandle,'No file selected.');
         return;
      end
      
   end
   
   % Return to the previous path
   cd(thisPath);
   
else,
   filePath = '';
end

% Make sure we have a non-zero length file
dirCheck = dir([filePath fileName]);
if ~dirCheck.bytes,
    UIMessage(uiHandle,'Selected file is 0 bytes long.  Unable to open.');
    return;
end

% Open the file
fid = fopen([filePath fileName]);
if fid < 0,
    UIMessage(uiHandle,'Unable to open query file');
    return
end


% Attempt to read data
try
   % Look for date line
   readLine = fgetl(fid);
   ind = findstr(readLine,'udw.rec_retrieve');
   while isempty(ind),
      readLine = fgetl(fid);
      ind = findstr(readLine,'udw.rec_retrieve');
   end
   
   % See what type of servo we have 
   ind = findstr(readLine,'paper');
   servoType = 'PAPER';
   if isempty(ind)
      ind = findstr(lower(readLine),'carriage');
      servoType = 'CARRIAGE';
   end
   if isempty(ind)
      ind = findstr(readLine,'dc_service');
      servoType = 'SERVICE';
   end
   
   % Make sure we found a valid servo type designator
   if isempty(ind),
      error('No valid servo type identifier found.');
   end
   
   % Parse date and time info
   dateLine  = readLine;
   test.date = [dateLine(6:10) '/' dateLine(1:4)];
   test.time = dateLine(12:19);
   test.time = test.time(1:5);
   hour = str2num(test.time(1:2));
   if (hour > 12),
      hour = hour - 12;
      test.time = [num2str(hour) test.time(3:length(test.time))];
      test.time = [test.time ' PM'];
   else
      test.time = [test.time ' AM'];
   end
   test.type = 'Flight Recorder';
   [tempPath,test.name] = fileparts(fileName);
   if isempty(testName),
      test = edtest(test);
      if isempty(test),
          UIMessage(uiHandle,'Dragonfly Aborted.');
          return;
      end
   else,
      if isempty(testName)
         test.name = testName;
      else,
	      test = edtest(test);
          if isempty(test),
              UIMessage(uiHandle,'Dragonfly Aborted.');
              return;
          end
      end
   end
   
   % Look for sample size line
   readLine = fgetl(fid);
   ind = findstr(readLine,'sample_size');
   while isempty(ind),
      readLine = fgetl(fid);
      ind = findstr(readLine,'sample_size');
   end
   
   % Parse size info
   readLine = readLine(find(abs(readLine)>47 & abs(readLine)<58));
   sampleSize = str2num(readLine);
   
   % Burn the 'tag value' line, not using TIC any more
   fgetl(fid);
   
   % Start reading data
   UIMessage(uiHandle,'Reading Data...');
   last_completed = 0;
   data = zeros(sampleSize,3);  % no tic value
   
   % Allow for shorter reads
   try,
	   for i = 1:sampleSize,
	      % Read line
	      readLine = fgetl(fid);
	      
	      % Parse it
	      ind = findstr(readLine,']');
	      readLine = readLine(ind+1:length(readLine));
	      readData = sscanf(readLine,'%f');
	      
	      % Store data
	      data(i,:) = readData';
	      
	      completed = round(i/sampleSize*100);
	      if (completed > last_completed) & (mod(completed,5) == 0),
	         UIMessage(uiHandle,sprintf('Reading Data - %d%% Done',completed));   
	         last_completed = completed;
	      end
	      
   	end
      
   catch,
      if i > 1,
         data = data(1:i-1,:);
         sampleSize = i-1;
      else,
         error('Error in QRY file');
      end
   end
   
      
   UIMessage(uiHandle,'Parsing Data...');
   
   % Extract time
   if (data(1,2) ~= data(1,3))
      % Drop 1st 5 points, as these seem to be at 300Hz.
      % Want everything at 1200Hz.
%      data = data(8:end,:);
      
      % Have TIC data
      tic  = [data(:,1) data(:,2)];
%      haveTic = 1;

% Not working so well, as channels are getting slightly
% different sample times.
      
   else
      % Don't have TIC data so will assume a sample rate
      haveTic = 0;
   end
   
   % Extract data
   data = [data(:,1) data(:,3)];
   
   % Close file
   fclose(fid);
   
catch
   
   % Close file
   if (fid ~= 0) & (fid ~= -1),
      fclose(fid);
   end
   
   UIMessage(uiHandle,'Error reading query file.');
   return;
   
end


% Set up the database for the type of recorder we have 
if strcmp(servoType,'CARRIAGE'),
   % Data values from "carriage_recorder.h", 01/06/99
   % POSITION            1
   % VELOCITY            2
   % INCREMENT           4
   % POSITION_CMD        8
   % POSITION_ERR       16
   % POSITION_ERR_DERIV 32
   % PWM                64
   % Servo State       128
   % Destination       256
   % Trigger           512  Move on/Move off
   % Padout Distance  1024
   sampleRate=inv(1200);
   dataBase{1} = {'Actual Position','cEU'};
   dataBase{2} = {'Actual Velocity','cEU/cInt'}; 
   dataBase{3} = {'Commanded Velocity','cEU/cInt'};
   dataBase{4} = {'Commanded Position','cEU'}; 
   dataBase{5} = {'Position Error','cEU'};
   dataBase{6} = {'Velocity Error','cEU/int300'};
   dataBase{7} = {'PWM Value',''};
   dataBase{8} = {'State',''};
   dataBase{9} = {'Destination','cEU'};
   %dataBase{10} = {'Trigger',''};
   dataBase{10} = {'Disturbance Equivalent PWM',''};
   dataBase{11} = {'Padout Distance','cEU'};
   dataBase{12} = {'Accel PWM Sum',''};
   dataBase{13} = {'PWM^2 Sum',''};
   dataBase{14} = {'PWM*Vel Sum',''};
   dataBase{15} = {'Vel^2 Sum',''};
   
elseif strcmp(servoType,'PAPER'),
   % Data values from "paper_recorder.h", 01/26/99
   % Not full record, doesn't include any sneak info.
   % POSITION            1
   % VELOCITY            2
   % INCREMENT           4
   % POSITION_CMD        8
   % POSITION_ERR       16
   % POSITION_ERR_DERIV 32
   % PWM                64
   sampleRate=inv(600);
   dataBase{1} = {'Actual Position','pEU'};
   dataBase{2} = {'Actual Velocity','pEU/int600'};
   dataBase{3} = {'Commanded Velocity','pEU/int600'};
   dataBase{4} = {'Commanded Position','pEU'};
   dataBase{5} = {'Position Error','pEU'};
   dataBase{6} = {'Velocity Error','pEU/int600'};
   dataBase{7} = {'PWM Value',''};
elseif strcmp(servoType,'SERVICE'),
   % Added 10/05/99, DLH
   % POSITION            1
   % VELOCITY            2
   % INCREMENT           4
   % POSITION_CMD        8
   % POSITION_ERR       16
   % POSITION_ERR_DERIV 32
   % PWM                64
   % STATE					128
   sampleRate=inv(138.2);
   dataBase{1} = {'Motor Actual Position','sEU'};
   dataBase{2} = {'Motor Actual Velocity','sEU/sInt'};
   dataBase{3} = {'Commanded Motor Velocity','sEU/sInt'};
   dataBase{4} = {'Commanded Motor Position','sEU'};
   dataBase{5} = {'Motor Position Error','sEU'};
   dataBase{6} = {'Motor Velocity Error','sEU/sInt'};
   dataBase{7} = {'PWM Value',''};
   dataBase{8} = {'State',''};
else,
   error('Unknown servo type.');
end
   
% Check for unknown signals
knownTag = 2.^[0:length(dataBase)-1] ;
recvdTag = unique(data(:,1))';
if ~isempty(setdiff(recvdTag,knownTag)),
   warning('Dragonfly data set contains unknown signals which will be ignored.');
end

% Parse the data for individual channels
chanman
for i = 1:length(dataBase),
   
   % Look for the TAG
   ind  = find(data(:,1)==2^(i-1));
   
   if ~isempty(ind),
      % Found the tag, make the channel
         temp = data(ind,2);
         ind2  = find(temp>2^31);
         temp(ind2) = temp(ind2)-2^32;
         data(ind,2) = temp;
      % UI
      UIMessage(uiHandle,['Channel found: ' dataBase{i}{1}]);
      
      % Create the channel
      if haveTic,
         % Have TIC values, so actual time stamps
         time = tic(ind,2);
         time = time - time(1);
         time = time/1e6;
         
         % If sample time standard deviation < 5% of mean sample time,
         % then we can safely assume that all points were sampled at the
         % same sample rate.  If so, convert to that sample rate.
         %dt = diff(time);
         %meanDt = mean(dt);
         %if (std(dt)/meanDt) < 0.05,
         %   time = meanDt;
         %end
         
	      chan = channel(dataBase{i}{1},data(ind,2),time,dataBase{i}{2},test);
      else,
	      chan = channel(dataBase{i}{1},data(ind,2),sampleRate,dataBase{i}{2},test);
      end
         
      if (i==2),  % Handle Velocity data
%         chan = chan/2^4;
%         set(chan,'Name','PD Controller Output');
         chan = -chan;
         chan = convert(chan,'IPS');
      end
      
      chanman('AddChannel',chan);
   end
   
end

% See if we should add any channels
if strcmp(servoType,'CARRIAGE'),
   auxchan;
elseif strcmp(servoType,'PAPER'),
   auxchan;
elseif strcmp(servoType,'SERVICE'),
   ss_auxchan;
end

% Delete the file
UIMessage(uiHandle,'DragonFly Finished');
%delete([filePath fileName]);
cd(thisPath);


% User Interface messages
function [] = UIMessage(handle,msg)

if handle,
   %set(handle,'string',msg);
   chanman('SetMessage',msg);
else,
   disp(msg);
   drawnow
end   
