%CHANMANPLUGIN  Channel Manager Plugin Class

classdef chanmanplugin
    
    properties
        type  = ''; % Plugin Type - Parser, Tool
        menu  = []; % Sub menu.  Top level if empty.
        label = ''; % Menu label
        fcn   = []; % Function to call
        args  = {}; % Default fcn arguments.
    end % properties - public
    
    properties (Constant=true)
        validtypes = {'parser','tool'};
    end % properties - constant
    
    methods
        
        function tf = valid(obj)
            %VALID  Validates plugin
            
            % Type
            obj.type = lower(obj.type);
            if ~ismember(obj.type,obj.validtypes)
                error(['Invalid Channel Manager Plug-in type: ' upper(obj.type)]);
            end
            tf = true;
            
        end % valid
        
    end % methods - public
    
end  % classdef

