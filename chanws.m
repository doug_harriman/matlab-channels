%CHANWS  Channel Workspace import/export UI.
%

%TODO - Buttons to select all or clear all
%TODO - Properly size window based on name sizes.
%TODO - Always open dialog box centered on screen 1.
%TODO - Make dialog modal.
%TODO - Don't display figure until everything is sized.
%TODO - Disable window resize.
%TODO - Validate variable names user name change.

classdef chanws < hgsetget
    properties
        
    end % properties - public
    
    properties (SetAccess=protected)
        mode = 'input';  % Input to ChanMan or output from ChanMan
    end % properties - setaccess protected

    properties (SetAccess=protected,GetAccess=protected)
        han_figure    = [];
        han_uitable   = [];
        list_chan     = {};
    end % properties - protected

    
    properties (Constant=true)
        version = '23-JUN-2008';
    end % properties - Constant

    methods
        function obj = chanws(mode,list_chan)
            %CHANWS  Channel Workspace object constructor.
            %  CHANWS(MODE,CHAN_LIST)  
            %
            
            % Check mode.
            mode = lower(mode);
            if ~ismember(mode,{'input','output'})
                error('I/O mode must be either "input" or "output"');
            end
            obj.mode = mode;

            % Figure name & list of channels
            wsvar = [];
            switch(obj.mode)
                case 'input'
                    name = 'Import Channels from Workspace';
                    dir  = 'Import';
                    col_edit = [false false];
                    callback = @obj.Import;
                    [obj.list_chan, wsvar] = obj.wschans;
                case 'output'
                    name = 'Export Channels to Workspace';
                    dir = 'Export';
                    col_edit = [false true];
                    callback = @obj.Export;

                    % Check channel list
                    tf = cellfun(@(x)ischannel(x),list_chan);
                    if ~all(tf)
                        error(['Non channel objects passed in.']);
                    end
                    obj.list_chan = list_chan;
                otherwise
                    error('Unsupported I/O mode');
            end
            
            % Bail if no channels
            if isempty(obj.list_chan)
                warning(['No channels available for ' obj.mode]);
                return;
            end
            
            % Generate WS var names if needed.
            n_ch = length(obj.list_chan);
            if isempty(wsvar)
                for i=1:n_ch
                    % Base name
                    channame = genvarname(get(obj.list_chan{i},'name'));
                    
                    % Cleanups
                    channame = strrep(channame,'0x2D','');
                    channame = strrep(channame,'0x2F','Per');
                    
                    % Store it
                    wsvar{i} = channame;
                end
            end
            
            % Create figure
            obj.han_figure = figure('NumberTitle','Off',...
                'MenuBar','None',...
                'Name',name);
            
            % Object sizing & location
            dy = 0.025;
            dx = dy;
            
            
            % Create the table
            col_names  = {'Channel Name','Var Name',dir};
            col_format = {'char','char','logical'};
            col_edit   = [col_edit true];
            y = 0.85;
            obj.han_uitable = uitable('ColumnName',col_names,...
                'ColumnEditable',col_edit,...
                'ColumnFormat',col_format,...
                'RearrangeableColumn','on',...
                'RowName','numbered',...
                'Units','normalized',...
                'Position',[0.025 (1-y)-dy 0.95 y],...
                'RowStriping','on');
            
            % Fill the table.
            data = cell(n_ch,3);
            for i = 1:n_ch
                data{i,1} = get(obj.list_chan{i},'name');
                data{i,2} = wsvar{i};
                data{i,3} = true;
            end
            set(obj.han_uitable,'Data',data);
            
            % Set name column width
            len1 = cellfun(@(x)length(x),data(:,1));
            len1 = max(len1);
            len2 = cellfun(@(x)length(x),data(:,2));
            len2 = max(len2);
            set(obj.han_uitable,'FontUnits','Pixels');
            fs = get(obj.han_uitable,'FontSize')/1.7;
            set(obj.han_uitable,'ColumnWidth',{fs*len1 fs*len2 'auto'});
            
            % Show the figure
            set(obj.han_figure,'Visible','On');
            
            % Buttons.
            uicontrol(obj.han_figure,'Style','pushbutton',...
                'String','OK',...
                'Units','normalized',...
                'Position',[.8-dx dy .2 .1-dy],...
                'Callback',callback);

            uicontrol(obj.han_figure,'Style','pushbutton',...
                'String','Cancel',...
                'Units','normalized',...
                'Position',[.6-dx*2 dy .2 .1-dy],...
                'Callback',@obj.delete);

            uicontrol(obj.han_figure,'Style','pushbutton',...
                'String','Clear All',...
                'Units','normalized',...
                'Position',[.4-dx*3 dy .2 .1-dy],...
                'Callback',@obj.ClearAll);

            uicontrol(obj.han_figure,'Style','pushbutton',...
                'String','Select All',...
                'Units','normalized',...
                'Position',[.2-dx*3 dy .2 .1-dy],...
                'Callback',@obj.SelectAll);

            
        end % constructor

        function [] = delete(obj,varargin)
            %DELETE  CHANWS destructor
            if ishandle(obj.han_figure)
                delete(obj.han_figure);
            end
            
        end % delete
        
        
        function [] = Export(obj,varargin)
            %EXPORT  Export the channels
            data = get(obj.han_uitable,'Data');
            n_ch = length(obj.list_chan);
            
            % Export each one as needed
            for i = 1:n_ch
                if data{i,3}
                    % Validate name
                    name = data{i,2};
                    name = genvarname(name);
                    assignin('base',name,obj.list_chan{i});
                end
            end
            
            % Close the window.
            delete(obj.han_figure);
        end
        
        function [] = Import(obj,varargin)
            %IMPORT  Import the channels
            data = get(obj.han_uitable,'Data');
            n_ch = length(obj.list_chan);
            
            % Import each one as needed
            for i = 1:n_ch
                if data{i,3}
                    chanman('AddChannel',obj.list_chan{i});
                end
            end
            
            % Close the window.
            delete(obj.han_figure);
        end

        function [list,wsvar] = wschans(obj)
            %WSCHANS  Returns list of channels in workspace.
            %

            % Get vars in WS
            vars = evalin('base','whos');

            % Loop through vars, testing for type
            list = {};
            wsvar = {};
            for i = 1:length(vars)
                ch = evalin('base',vars(i).name);
                if isa(ch,'channel')
                    list{end+1,1} = ch; %#ok<AGROW>
                    wsvar{end+1,1} = vars(i).name;
                end
            end
            
        end % wschans
            
        function [obj] = ClearAll(obj,varargin)
            %CLEARALL  Clears all selected channels.
            %
            
            % Get the current data
            data = get(obj.han_uitable,'Data');

            % Update the data
            val = num2cell(false(size(data,1),1));
            data(:,3) = val;
            
            % Write it
            set(obj.han_uitable,'Data',data);
            
        end % ClearAll

        function [obj] = SelectAll(obj,varargin)
            %SELECTALL  Selects all channels.
            %
            
            % Get the current data
            data = get(obj.han_uitable,'Data');

            % Update the data
            val = num2cell(true(size(data,1),1));
            data(:,3) = val;
            
            % Write it
            set(obj.han_uitable,'Data',data);

        end % SelectAll
        
        
    end % methods - public
    
    
end % classdef