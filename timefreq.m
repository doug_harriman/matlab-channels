%TIMEFREQ  Time/frequency plot of channel.
%           [magnitude, frequency, time] = timefreq(channel)
%

% DLH 10/13/98

function [psdData,f,t] = timefreq(chan)

% Constants
FFTSIZE = 512;

% Error check DT / sample freq agreement
chanName = get(chan,'Name');
dataDT = get(chan,'SampleTime');
DT = dataDT; 
sampleRatio = round(DT/dataDT);
if mod(sampleRatio,2),
  % error('Waterfall sample time must be an integral multiple of the data sample time.');
end

%UI
if nargout == 0,
   disp(['Using a ' num2str(FFTSIZE) ' point FFT.']);
   disp(['Frequency Resolution: ' num2str(inv(DT*FFTSIZE)) ' Hz.']);
   disp(['Time      Resolution: ' num2str(DT) ' sec.']);
   drawnow;
end

% Pad front and back with zeros
oldChan=chan;
data = get(chan,'data');
data = [zeros(FFTSIZE,1); data; zeros(FFTSIZE,1)];
set(chan,'Data',data);

% Number of time samples
numSamples = length(chan)-FFTSIZE;

% Preallocate memory
psdData = zeros(numSamples,FFTSIZE/2);
for i=1:numSamples,
   temp = zeromean(chan(i:i+FFTSIZE-1));
   temp = hanning(temp);
   temp = psd(temp);
   psdData(i,:) = get(temp,'Data')';
end

% Truncate data to get rid of zero pad areas
psdData = psdData(FFTSIZE/2:i-FFTSIZE/2,:)';

% Get axis data
f = get(temp,'Freqs');
t = [0:DT:(size(psdData,2)-1)*DT];

% Do the plot or output
if nargout == 0,
   figure;
   subplot(2,1,1);
   h=imagesc(t,f,psdData);
   set(gca,'ydir','normal');
   xlabel('Time (sec)');
   ylabel('Frequency (Hz)');
   title(['Time/Frequency Plot of ' chanName])
   subplot(2,1,2);
   plot(oldChan);
   clear psdData 
end   
