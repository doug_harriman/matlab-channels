% xversus  Extended channel plot of channel 1 vs channel 2
% Syntax: xversus(channel_1, channel_2,str)
% Str is an optional string that will be tacked on to the test
%  name.

function [] = xversus(chan1,chan2,str)

% See if we have string option
if nargin == 3,
   test = get(chan1,'test');
   test.name = [test.name ': ' str];
   set(chan1,'test',test);
end

% New figure set to take up full page
fig1 = figure;
set(fig1,'paperposition',[.25 .27 8 10.5],...
   		'position',[10   265   623   712]);

% Create the center plot
versus(chan1,chan2,'b.');
title('');
mainax = gca;
set(mainax,'position',[ .35 .3 .3 .5],...
   'dataaspectratio',[1 1 1]); 
hold on;
%axis([-.5 .5 -1 2])
axis([-1 1 -1 1]*.4)
plot(0,0,'r+');
grid;

pos = get(mainax,'position');
if pos(3)>.3,
   set(mainax,'position',[pos(1:2) .3 pos(4)*(.3/pos(3))]);
end
if pos(4)>.6,
   set(mainax,'position',[pos(1:2) pos(3)*(.6/pos(4)) .6]);
end

% Create channel 1 histogram
ch1hist = axes('position',[.15 .85 .7 .1]);
[n,x]=hist(chan1,100);
bar(x,n);grid;
xlabel('');ylabel('');
title(['Histogram' ...
      ', Mean = ' num2str(mean(chan1)) ...
      ', \sigma = ' num2str(std(chan1))]);
set(gca,'xlim',get(mainax,'xlim'));
%set(gca,'xlim',[-1 1]*.3+mean(chan1));

% Create channel 1 psd
ch1hist = axes('position',[.15 .1 .7 .14]);
psd(chan1);grid;
title('');
set(gca,'xlim',[0 300]);
set(gca,'ylim',[0 15]);
label(chan1);

% Create channel 2 histogram
ch1hist = axes('position',[.15 .3 .125 .5]);
[n,x]=hist(chan2,100);
%[n,x]=hist(chan2,50);
barh(x,n);grid;
set(gca,'xdir','reverse',...
   'ylim',get(mainax,'ylim'));
xlabel('');title('');
ylabel(['Histogram' ...
      ', Mean = ' num2str(mean(chan2)) ...
      ', \sigma = ' num2str(std(chan2))]);

% Create channel 2 psd
ch2psd = axes('position',[.725 .3 .125 .5]);
w=psd(chan2);
plot(get(w,'data'),get(w,'freqs'));;grid
set(gca,'yaxislocation','right',...
   		'xdir','reverse');
ylabel('Frequency (Hz)');
title('')
%set(gca,'xlim',[0 1.1*max(w(2:get(w,'points')))]);
set(gca,'xlim',[0 15]);
set(gca,'ylim',[0 300]);
