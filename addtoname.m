% addtoname
% Prepends the give string to all channel names.

function [chan] = reduce(in)

% Keep string as a global
global str

% If called with str begin
if ~isa(in,'channel'),
   
   % Cache the string
   str = in;
   
   % Map the fcn
   chanman('Map','addtoname');
   
else
   
   % Called with a channel name argument, so perform op 
   % and return channel.
   
   % Get channel and delete it from list
   chan = in;
   chanman('DeleteChan',get(chan,'name'));
   
   % Prepend the name
   set(chan,'name',[str get(chan,'name')]);
   
end
