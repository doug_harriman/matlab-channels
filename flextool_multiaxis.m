%FLEXTOOL_MULTIAXIS  Reads Flextool tracestore file and writes channels 
%          to the channel manager.
%

% DLH
% 06/17/04 - Handle multiple components gracefully;
% 11/13/02 - Error handling for files with no data.
% 10/18/02 - Got tic fixing correct.
% 05/13/02 - Created

function [] = flextool_multiaxis(fileName,uiHandle)

% UI stuff
if nargin < 2,
    uiHandle = 0;
end

% If not provided a file name, first look in current dir, then at c:\
if (nargin < 1) || (fileName == 0)
    % Try this dir
    thisPath = cd;
    dirInfo = dir('*.trc');
    
    if isempty(dirInfo)
        UIMessage(uiHandle,'Looking for TRC file in default location.');
        % Try default dir
        thisPath = cd;
        if ispc
            cd('c:\');
        elseif isunix
            cd('~');
        end
        dirInfo = dir('*.trc');
    end

    % If didn't find anything in default dir, go back to working dir
    if isempty(dirInfo)
        UIMessage(uiHandle,'None found.  Returning to working dir.');
        cd(thisPath)
        dirInfo = dir('*.trc');
    end
    
    
    % If number of choices == 1, then open it, otherwise, show dialog.
    if length(dirInfo) ~= 1,
        [fileName,filePath] = uigetfile('*.trc;*.trs','Flextool Tracestore File');
        if fileName == 0,
            return;
        end
        fileName = [filePath fileName];
    else
        % Found name
        fileName = [cd filesep dirInfo.name];
    end
    
    % Return to original dir
    cd(thisPath);
end

% Base metadata
meta = chanmeta;
[meta.sourcepath,fn,ext] = fileparts(fileName);
meta.sourcefile      = [fn ext];
meta.importtimestamp = datestr(now);
dirinfo = dir(fileName);
meta.sourcetimestamp = datestr(dirinfo.datenum);

% Load the data the hard way
disp(['Parsing TRC file: ' fileName]);
fid = fopen(fileName,'r');
if (fid < 0)
    error(['Unable to open file: ' fileName]);
end
onCleanup(@()fclose(fid));  % Make sure file is closed on exit.


% Read header info
line = fgetl(fid);
while all(line ~= -1) &&  isempty(findstr(line,'Tic	Component'))
    % Header info
    line = fgetl(fid);
    if isempty(line), line = ' '; end
end
%line = fgetl(fid);

% Abort if reached end of file
if line == -1,
    error(['File contains no tracestore data: ' fileName]);
end

% Found var declaration
varNames = words(line);
nVars = length(varNames);

nPts = 2^18;
data = ones(nPts,nVars)*NaN;
pntCnt = 1;
line = fgetl(fid);
% Data read loop
while all(line ~= -1) && (pntCnt <= nPts),
    line = strrep(line,[char(9) char( 9)],' NaN ');
    line = strrep(line,[char(9) char(32)],' NaN ');
    line = str2num(line); %#ok<ST2NM>
    if ~isempty(line)
        data(pntCnt,1:length(line)) = line;
        pntCnt = pntCnt+1;
    end
    line = fgetl(fid);
    
    % Double data size if need more space.
    if (pntCnt > nPts) && (ischar(line))
        data = [data; ones(nPts,nVars)*NaN]; %#ok<AGROW>
        nPts = length(nPts);
    end
    
end
fclose(fid);

% Get just the points we wanted
pntCnt = pntCnt - 1;
UIMessage(uiHandle,['Data points read: ' int2str(pntCnt)]);
data = data(1:pntCnt,:);

% Fix timer rollover
tic = data(:,1)/1e6;
ind = find(diff(tic) < 0);
if ~isempty(ind),
    UIMessage(uiHandle,'Fixing TIC data.');
    for i = 1:length(ind),
        tic(ind(i)+1:end) = tic(ind(i)+1:end) + tic(ind(i)) - tic(ind(i)+1);
    end
end
tic = tic - tic(1);

% Rip into individual variables
comp = data(:,2);
for i = 3:nVars,

    % Eliminate NaN's
    d  = data(:,i);
    ind = ~isnan(d);

    if ~isempty(find(ind, 1))

        d  = d(ind);        % Data
        t  = tic(ind);      % Time
        c  = comp(ind);  % Component
        c  = c(1);

        % Remove '_' from names
        name = varNames{i};
        ind  = find(name == '_');
        name(ind+1) = upper(name(ind+1));
        name(ind) = '';

        % Get rid of other garbage characters.
        name = strrep(name,'.',' ');
        name = strrep(name,'->',' ');
        name = strrep(name,'buf','');
        name = strrep(name,'prof','');

        % Set Position Units
        axisName = GetRecorder(c);
        if ~isempty(findstr(name,'Position'))
            unitStr = GetPosUnits(axisName);
        else
            unitStr = '';
        end

        % Add component name to data set
        name = [axisName ' ' name]; %#ok<AGROW>

        % Package the channel information
        trace_var{i-2}.name  = name; %#ok<AGROW>
        trace_var{i-2}.data  = d; %#ok<AGROW>
        trace_var{i-2}.time  = t; %#ok<AGROW>
        trace_var{i-2}.units = unitStr; %#ok<AGROW>
        trace_var{i-2}.id    = c; %#ok<AGROW>

    end
end


% Warn user if won't be able to use real variable names.
UIMessage(uiHandle,'Creating channels.');

% Define the test info
d = dir(fileName);
file_date = datenum(d.date);

% Create channels.
chanman;
nVars = length(trace_var);
for i=1:nVars,
    chan = channel(trace_var{i}.name,trace_var{i}.data,trace_var{i}.time,trace_var{i}.units);
    chan = set(chan,'ChanMeta',meta);
    chanman('AddChannel',chan);
end


% Cycle through all channels, converting general thermal data to individual
% motor axes.
axisNameList = {'Carriage','Paper'};
chanList = chanman('GetChannelAll');
for i = 1:length(chanList),
    chan = chanList{i};
    name = get(chan,'Name');

    if strcmp(name, 'Thermal Model motor'),
        % Extract indeces
        data     = get(chan,'Data');
        axesVals = unique(data);
        numAxes  = length(axesVals);
        axesInd  = {};
        
        for j = 1:numAxes,
            axesInd{j} = data==axesVals(j); %#ok<AGROW>
        end
        
        chanman('DeleteChannelByName',name);
        
    elseif ~isempty(findstr(name,'Thermal Model')),
        % Apply indeces
        numAxes = length(axesInd);
        for j = 1:numAxes,
           axisName = [axisNameList{j} ' ' name];  % Data is 0 based index
           data = get(chan,'Data');
           data = data(axesInd{j});
           time = get(chan,'Time');
           time = time(axesInd{j});
           meta = get(chan,'ChanMeta');
           
           % Handle power data
           if ~isempty(findstr(name,'power')),
               data = data/32/5;  % Power data stored scaled up by 3 bits
               data = unit(data,'W');
           end
           
           newCh = channel(axisName,data,time);
           newCh = set(newCh,'ChanMeta',meta);
           
           % Plot color
           if ~isempty(findstr(name,'Tw')), set(newCh,'PlotColor','r'); end
           if ~isempty(findstr(name,'Tm')), set(newCh,'PlotColor','b'); end
           if ~isempty(findstr(name,'Tc')), set(newCh,'PlotColor','g'); end
           
           chanman('AddChannel',newCh);
        end
        
        chanman('DeleteChannelByName',name);
    end
   
end
chanman('SortChannels');

% Calculate velocity
CalcVel;

% Del Sol Temperatures
try %#ok<TRYNC>
    calcdelsoltemp('icTemp1');
    calcdelsoltemp('icTemp2');
end

% Overlap data
try %#ok<TRYNC>
    mech_rec_parse;
end

% Octane dryer
ch = chanman('GetChannelByName','Dryer HeaterInfo temperature');
if ~isempty(ch)
    name = get(ch,'Name');
    ch = ch / 8;  % Reading is 1/8 DegC
    ch = set(ch,'Name',name);
    chanman('DeleteChannelByName',name);
    chanman('AddChannel',ch);
end

ch = chanman('GetChannelByName','Dryer HeaterInfo cmdTemperature');
if ~isempty(ch)
    name = get(ch,'Name');
    ch = ch / 8;  % Reading is 1/8 DegC
    ch = set(ch,'Name',name);
    ch = set(ch,'PlotColor','r');
    chanman('DeleteChannelByName',name);
    chanman('AddChannel',ch);
end


return;

% User Interface messages
function [] = UIMessage(handle,msg)

if handle,
    if handle == -1,
        chanman('SetMessage',msg);
    else
        set(handle,'string',msg);
    end
else
    disp(msg);
    drawnow
end   


function [recorder] = GetRecorder(id)

%ID to recorder map
idLib = [93 95 100 117 882 408 388 1760 1835];
if ~any(id==idLib), error(['Unknown component ID: ' num2str(id)]); end
recLib = {'Mech','Carriage','Paper','Service','Thermal Model','Pick','ADF','Dryer','MI_Servo'};
recorder = recLib{id==idLib};

function [posUnits] = GetPosUnits(axisName)

switch lower(axisName);
    case 'carriage'
        posUnits = 'cEU';
        
    case 'paper'
        posUnits = 'pEU';    
        
    case 'service'
        posUnits = 'sEU';
        
    case 'mech'
        posUnits = 'mU';
        
    case 'thermal model',
        posUnits = '';

    case 'pick',
        posUnits = 'pickEU';
        
    case 'adf',
        posUnits = '';

    case 'mi_servo'
        % This is for MI Servo recorders that don't assign an axis.
        posUnits = '';
        
    otherwise
        error(['Unknown axis Name: ' upper(axisName)]);
end

function [] = CalcVel()

axis = 'Carriage';
pos = chanman('GetChannelByName',[axis ' ServoInfoPosition']);
if(isa(pos,'channel'))
%     offset = 0; %pos(1);
%     pos = pos - offset;

    % Vel in physical units
    vel = diff(pos);
    vel = convert(vel,'IPS');
    set(vel,'Name',[axis ' Velocity']);
    chanman('AddChannel',vel);

%     acc = diff(vel);
%     acc = convert(acc,'G');
%     acc = bobfilt(acc);
%     set(acc,'Name',[axis 'Actual Acceleration']);
%     chanman('AddChannel',acc);

end

% case 'paper'
%     % Vel in controller units
%     convert(pos,'pEU');
%     mvel = channel('Measured Velocity',diff(double(pos)),ts(1:end-1),'pEU/pInt',test);
%     set(mvel,'Axis',recorder);
%     chanman('AddChannel',mvel);


