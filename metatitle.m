%METATITLE  Title current figure channel metadata.
%

function metatitle(ch)

error(nargchk(0,1,nargin));

% If no channel provided, grab selected one from Channel Manager.
if nargin < 1
    ch = chanman('GetChannelSelected');
    if isempty(ch)
        error('No channel selected or provided.');
    end
    
    ch = ch{1};
end

if ~isa(ch,'channel')
    error('Input data must be a channel object.');
end

% Get metadata
meta = get(ch,'metadata');
if isempty(meta)
    error('No metadata present in channel object.');
end

if ~isa(meta,'chanmeta')
    error('METATITLE only works with CHANMETA data.');
end
title(meta.sourcefile,'Interpreter','None');
