%TOCHANNEL  Converts Simulink data to a channel object.
%
%

% DLH 7/5/98

function [sys,x0,str,ts] = tochannel(t,~,u,flag,...
   chanName, sampleRate, units,...
   testName, toChanMan, toWorkspace)

switch flag

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0
     % System sizes
     sizes = simsizes;
     sizes.NumContStates  = 0;
     sizes.NumDiscStates  = 0;
     sizes.NumOutputs     = 0;
     sizes.NumInputs      = 1;
     sizes.DirFeedthrough = 0;
     sizes.NumSampleTimes = 1;   % at least one sample time is needed
     sys = simsizes(sizes);
     
     % Initial conditions
     x0  = [];
     
     % str is always an empty matrix
     str = [];
     
     % Initialize the array of sample times
     ts  = [inv(sampleRate) 0];
%      ts = [-1 1];
     
     % Make sure the names match
     set_param(get_param(gcb,'Parent'),'Name',chanName);
     
     % Setup storage space
     if (toWorkspace || toChanMan),
        
        parent = get_param(gcb,'Parent');
        parent = parent(1:find(parent=='/')-1);
        data = zeros(fix(eval(get_param(parent,'StopTime'))*sampleRate),1);
        set_param(gcb,'UserData',data);
     end
     
  case 2
     sys = [];
     
     % Store points
     if (toWorkspace || toChanMan),
        data = get_param(gcb,'UserData');
        data(round(t*sampleRate)+1) = u;
        set_param(gcb,'UserData',data);
     end
     
  case 3
      % No-op
      
  case 9
     
     % Build the test struct
     if (toWorkspace || toChanMan),
        parent = get_param(gcb,'Parent');
        parent = parent(1:find(parent=='/')-1);
        test.type = [get_param(parent,'Name') ' Simulation'];
        test.name = testName;
        test.time = datestr(now,14);
        test.date = datestr(now,2);
        
        % Output the data
        data = get_param(gcb,'UserData');
        if isempty(data)
            warning('To Channel block empty'); %#ok<WNTAG>
        end
        
        chan = channel(chanName,data,...
           inv(sampleRate),units,'Metadata',test);
        
        % Send to Channel Manager
        if toChanMan
           chanman;
           chanman('AddChannel',chan);
        end
        
        % Send to Workspace
        if toWorkspace
           assignin('base',chanName,chan);   
        end
     end
     
  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error(['Unhandled flag = ',num2str(flag)]);

end

% end ToChannel


