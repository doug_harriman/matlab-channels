%CHANMAN_TOOLS  Tools menu configuration.
%
% This function returns an array:
% toolDef(1).Label
% toolDef(1).Callback
%      .
% toolDef(N).Label
% toolDef(N).Callback

function toolDef = chanman_tools()
toolDef = [];

index = 1;
if (exist('dragonfly') == 2),
	toolDef(index).Label    = 'Get &DragonFly Data';
	toolDef(index).Callback = ['dragonfly([],[],1);'];
	index = index + 1 ;   
end

if (exist('kahuna') == 2),
	toolDef(index).Label    = 'Get &Kahuna Data';
	toolDef(index).Callback = 'kahuna';
	index = index + 1 ;   
end

if exist('yokogawa','file'),
	toolDef(index).Label    = 'Get &Yokogawa Scope Data';
	toolDef(index).Callback = 'yokogawa([],[],1);';
	index = index + 1 ;   
end

if exist('thermocouple','file'),
	toolDef(index).Label    = 'Get &Thermocouple Data';
	toolDef(index).Callback = 'thermocouple';
	index = index + 1 ;   
end

if exist('to_sptool','file'),
	toolDef(index).Label    = 'To S&P Tool';
	toolDef(index).Callback = 'to_sptool';
	index = index + 1 ;   
end

if (exist('matdaq') == 2),
	toolDef(index).Label    = '&MatDAQ';
	toolDef(index).Callback = 'matdaq';
	index = index + 1 ;   
end


%	toolDef(index).Label    = ;
%	toolDef(index).Callback = ;
%	index = index + 1 ;   
