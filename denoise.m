% denoise
% Subtracts out zero meaned noise from the channels in the 
% Channel manager list.
%

function [chan] = denoise(chan)

% Keep noise as a global
global n

% If called with no arguments, begin the process
if nargin == 0,
   
   % Get the noise data
	disp('Removing system noise from all channels.')
	
	% Get noise
	n = chanman('GetChan','Noise') ;
	chanman('DeleteChan','Noise');
	
	% Zero mean noise
	n = n - mean(n) ;
	
   % Map this function over all channels
   chanman('Map','denoise');
   
else
   
   % Called with a channel name argument, so perform op 
   % and return channel.
   
   % Only denoise if channel has units of mils
   if strcmp(get(chan,'units'),'mils')
   
	   % Store its name
	   name = get(chan,'name');
	   
	   % Delete the channel so we don't have multiple copies
	   chanman('DeleteChan',name);
	      
	   % Set the noise units so we don't lose the channel units
	   set(n,'units',get(chan,'units'));
	   
	   % Subtract noise
	   chan = chan - n;
	   
	   % Reset name
      set(chan,'name',name);
      
   else
      % Return empty channel to signal no change
      chan = channel('No data',[],1);
      
   end
   
end
