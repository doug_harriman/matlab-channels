%SS_AUXCHAN

function [] = ss_auxchan()

driveRatio = unit(0.02297583,'mm/sEU');

% Get Data that should be there
pos  = chanman('GetChan','Motor Actual Position');
if ~isa(pos,'channel'),
   pos = chanman('GetChan','Actual Position'); 
end

cpos = chanman('GetChan','Commanded Motor Position');
if ~isa(cpos,'channel'),
   cpos = chanman('GetChan','Commanded Position'); 
end

% Add signals we know aren't there
vel = diff(pos)*driveRatio;
velData = get(vel,'data');
negInd = find(velData<0);
for i=1:length(negInd),
	velData(negInd(i)) = 0;
end
set(vel,'Data',velData);
vel = convert(vel,'IPS');
set(vel,'Name','Sled Actual Velocity');
chanman('AddChannel',vel);

spos = pos*driveRatio;
spos = convert(spos,'mm');
set(spos,'Name','Sled Actual Position');
chanman('AddChannel',spos);

cvel = diff(cpos)*driveRatio;
cvelData = get(cvel,'data');
negInd = find(cvelData<0);
for i=1:length(negInd),
	cvelData(negInd(i)) = 0;
end
set(cvel,'Data',cvelData);
cvel = convert(cvel,'IPS');
set(cvel,'Name','Sled Commanded Velocity');
chanman('AddChannel',cvel);

epos = cpos - pos;
set(epos,'Name','Motor Position Error');
chanman('AddChannel',epos);

evel = diff(epos);
evel = convert(evel,'sEU/sInt');
set(evel,'Name','Motor Velocity Error');
chanman('AddChannel',evel);

evel = diff(epos)*driveRatio;
evel = convert(evel,'IPS');
set(evel,'Name','Sled Velocity Error');
chanman('AddChannel',evel);

acc = diff(vel);
acc = bobfilt(acc);
set(acc,'Name','Sled Actual Acceleration');
acc = convert(acc,'G');
chanman('AddChannel',acc);

mvel = diff(pos);
set(mvel,'Name','Motor Velocity');
mvel = convert(mvel,'sEU/sInt');
chanman('AddChannel',mvel);