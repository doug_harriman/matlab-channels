% Datalink v1.5
% Datalink creates a window that holds the data necessary
% to transfer data between VB and Matlab.
%
% If called originally from a GUI, Datalink will attach itself
% to that GUI.
% Calling Sytax:
% output = Datalink(message,arg1, arg2,...)
% Message:
%   Create - creates a datalink menu
%          arg1 - Optional. Callback to be executed when data is passed
%          arg2 - Optional. Handle to display messages in.  If left out messages
%                    displayed in Matlab Command Window.
%   Link   - Establish link with VB DataLink.
%   Unlink - Terminate link with VB Datalink.
%   Data   - Get data that has been passed.
%          output - cell array containing channel objects.
%

function [out] = datalink(msg,varargin) 

% Make sure we get some input
if nargin == 0, msg = 'Create'; end

% Message response structure
switch msg
case 'Create'
   
   % Attach menu structure to current window.
   % This will create a new one if necessary.
   mnu = uimenu('tag','mnuDataLink',...
      'label','DataLink') ;
   uimenu('tag','Link',...
      'label','&Link',...
      'parent',mnu,...
      'callback','Datalink(''Link'');') ;
   
   % See if we got a user defined callback 
   if nargin > 1,
      % Error check it
      call = varargin{1} ;
      if isstr(call),
         % Store 
         setuprop(gcf,'DataLinkCallback',call) ;
      else,
         error('Second argument to Datalink must be a string');
      end
   end
   
   % See if we got a display handle
   if nargin == 3,
      % Got something, test it
      arg = varargin{2} ;
      
      if ishandle(arg),
         % It was a handle for display
         setuprop(gcf,'DataLinkDisplay',arg) ;
      else,
         % Error
         error('Third argument to Datalink must be a handle') ;
      end
   else,
      setuprop(gcf,'DataLinkDisplay',0) ;
   end
   
% End 'Create'
%---------------------------------------------------   
   
case 'Link'
   
   % See if we already have a channel open
   chan = getuprop(datalinkfigure,'DataLinkChannel') ;
   if ~isempty(chan),
      % We thought we had a channel open, so try to close it
      ddeterm(chan) ;
   end
   
   % Initiate DDE link to VB program
   chan = ddeinit('Datalink','DataLink')  ;
   
   % Make sure we got it open
   if ~chan,
      % Link didn't open
      % UI
      uimessage('Unable to initiate DDE Convesation') ;
      
      % Break out of here
      break ;
   else,
      % Set up advisory link
      rc = ddeadv(chan,'DataSignal','datalink(message)','message',[1 1]) ;
   
      % Make sure we got it open
      if ~rc,
         % HotLink didn't open
         % UI
         uimessage('Unable to set up hot link with DataLink') ;
         
         % Break out of here
         break ;
      end
   end

   % Store channel
   setuprop(datalinkfigure,'DataLinkChannel',chan) ;
   
   % Change menu item
   set(findobj('tag','Link'),...
      'label','Un&link',...
      'callback','Datalink(''Unlink'');') ;
   
   % UI
   uimessage('Link Established') ;
   
% End 'Link'
%---------------------------------------------------   

case 'Unlink'
   
   % Get channel to close
   chan = getuprop(datalinkfigure,'DataLinkChannel') ;
   if ~isempty(chan),
      % We thought we had a channel open, so try to close it
      ddeterm(chan) ;
   end
   
   % Change menu item
   set(findobj('tag','Link'),...
      'label','&Link',...
      'callback','Datalink(''Link'');') ;
   
   % UI
   uimessage('Link Terminated') ;
   
% End 'Unlink'
%---------------------------------------------------   

case 'Transfer'
   
   % Get start time
   t0 = clock ;
   
   % UI
   uimessage('Receiving data from DataLink') ;
   
   % Get channel
   DDEChannel = getuprop(datalinkfigure,'DataLinkChannel') ;

   % First data passed, this initiates the conversation from the VB end.
   % The data passed is the number of channels that are going to be sent.
   ddepoke(DDEChannel,'DataRequest','File')   ;  % Requests next data
   
   % See if next data has been written
   while ~strcmp(msg,'File')
      msg = ddereq(DDEChannel,'DataSignal',[1 1]) ;
   end
   
   FileName = ddereq(DDEChannel,'DataPass',[1 1]) ;  % Gets this data
   
   % Signal end of conversation
   ddepoke(DDEChannel,'DataRequest','Done') ;
   
   % Import the file that DataLink Wrote
   DataLink('Import',FileName) ;
   
   % Delete the temporary data file
   delete(FileName) ;
   
   % Clock stuff
   elapsed = num2str(etime(clock,t0)) ;
   
   % UI
   uimessage(['Transfer time: ' elapsed ' sec']) ;
   
% End 'Transfer'
%---------------------------------------------------   
case 'Import'
   
   % Get file name
   filename = varargin{1} ;
   
   % Open file
   fid = fopen(filename,'r') ;

   % Error check
   if fid == -1,
      uimessage(['Error opening file: ' filename ]) ;
   end

   % Get Test Type
   Test.type = fgetl(fid) ;
   
   % Get Test Name
   Test.name = fgetl(fid) ;
   
   % Get Sample Rate
   Test.rate = str2num(fgetl(fid)) ;
   
   % Get Test Date
   Test.date = fgetl(fid) ;
   
   % Get Test Time
   Test.Time = fgetl(fid) ;
   
   % Get Test Duration
   TestDuration = str2num(fgetl(fid)) ;
   
   % Number of data points
   NumPoints = Test.rate*TestDuration ;
   
   % Channel Number
   ChNum = 1 ;
   
   % Read Channel Name
   str = fgetl(fid) ;
   
   % Loop To Read Channels
   while ~isempty(str) & isstr(str),
      % Set channel name and units
      Test.chan(ChNum).name  = str ;
      
      % Read Channel Units
      str = fgetl(fid) ;
      Test.chan(ChNum).units = str ;
      
      % Read data points
      [a,cnt] =  fscanf(fid,'%f',NumPoints) ;
      Test.chan(ChNum).data = a ;
      
      % Increment Channel count
      ChNum = ChNum + 1 ;
      
      % Burn 2 Lines
      fgetl(fid);fgetl(fid);
      
      % Read Channel Name
      str = fgetl(fid) ;
      
   end
   
   % Close stream
   fclose(fid) ;
   
   % Output if asked
   if nargout
      out = Test ;
   end
   
   % Write data to window 
   setuprop(datalinkfigure,'DataLinkData',Test) ;
   
   % Execute user defined callback if exists.
   call = getuprop(datalinkfigure,'DataLinkCallback') ;
   if ~isempty(call),
      eval(call) ;
   end
      
% End 'Import'
%---------------------------------------------------   

case 'Data'  % Return data to caller
   
   % Get data
   data = getuprop(datalinkfigure,'DataLinkData') ;
   
   % Error check
   if isempty(data),
      uimessage('No data available from Datalink') ;
      break ;
   end
   
   % Build test structure
   test.type = data.type ;
   test.name = data.name ;
   test.date = data.date ;
   test.time = data.Time ;
   
   % Loop through all channels
   for i = 1:length(data.chan),
      out{i} = channel(data.chan(i).name,...
         data.chan(i).data,...
         inv(data.rate),...
         data.chan(i).units,...
         test) ;
   end
   
   % Clear data to save space
   setuprop(datalinkfigure,'DataLinkData',[]) ;
   
% End 'Import'
%---------------------------------------------------   

end
% End Main program

%***************************************************
% Internally used functions

function out = datalinkfigure()

% Find figure with the datalink menus
out = get(findobj('tag','mnuDataLink'),'parent') ;


%***************************************************


function uimessage(str) ;

% Get UI handle
ui = getuprop(datalinkfigure,'DataLinkDisplay') ;

if ui,
   set(ui,'string',str) ;
else,
   disp(str)
end
 
 
%***************************************************

% Creates DataLink message box.
function handle = DataLink_Message(message)

% Create Window
handle = figure('menubar','none',...
   'numbertitle','off',...
   'position',[502   502   287   65],...
   'color',[1 1 1]*.8,...
   'windowstyle','modal',...
   'resize','off',...
   'name','DataLink Message',...
   'tag','DataLinkMessage') ;

% Create text area
h=uicontrol('style','text',...
   'units','normalized',...
   'position',[.1 .1 .8 .8],...
   'horizontalalignment','center',...
   'fontsize',10,...
   'backgroundcolor',[1 1 1]*.8,...
   'string',message) ;

   