% buildfilt
% Syntax
% [b,a] = buildfilt(frequency,poles,sample)
% [sys] = buildfilt(frequency,poles,sample)
% Outputs - [b,a] Numerator, Denominator of polynomial
%          [sys] LTI system
%
% Inputs - Frequency: Break frequency (Hz)
%          Poles:     Number of poles
%          Sample:    Optional Sample time.  If not specified,
%                       a continuous time system will be returned.
%

% Builds a filter

function [b,a] = builfilt(freq,poles,samp) ;

% Convert to r/s
freq = freq*2*pi ;

% First order filter
b0 = freq ;
a0 = [1 freq] ;
b = b0 ;
a = a0 ;

% Convolve first order filters 
if poles > 1,
   for i = 2:poles,
      b = b * b0 ;
      a = conv(a,a0) ;
   end
end

% Build lti system
sys = tf(b,a) ;

% Convert to discrete if necessary
if nargin == 3,
   sys = c2d(sys,samp) ;
end

% Output
if nargout == 2,
   [b,a] = tfdata(sys,'v') ;
elseif nargout == 1,
   b = sys ;
end
