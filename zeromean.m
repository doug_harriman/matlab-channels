% zeromean.m  Makes the mean of a channel zero.
%

% DLH  2/27/98

function out = zeromean(in)

% Chache name, do the mean thing, then reset name
name = get(in,'name');
in   = in - mean(in) ;
set(in,'name',name)  ;

if nargout==0
	% Change value in caller's workspace
   assignin('caller',inputname(1),in) ;
else
   out = in;
end