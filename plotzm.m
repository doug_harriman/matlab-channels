% Plotzm: Plots a channel shifted to zero mean.

function han = plotzm(chan)

nm = get(chan,'name') ;
chan = chan - mean(chan) ;
set(chan,'name',nm) ;
han = plot(chan) ;