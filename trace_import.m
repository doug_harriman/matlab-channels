%TRACE_IMPORT  Turbine trace data import.

function trace_import(fn)

if nargin < 1
    [fn,pn] = uigetfile('*.csv','Trace Data File');
    if fn == 0
        warning('No file selected.');
        return;
    end
    
    fn = [pn filesep fn];
end

% Read in file.
str = file2str(fn);

% Expect data format of:
% <tic>, <trace type>,<1 to N data columns>
res = regexp(str,'(?<tic>\d+), (?<tag>\w{4}),(?<data>.+\n)','names',...
    'dotexceptnewline','lineanchors');

% Convert to individual arrays.
res = scalarstruct(res);
tic = cell2mat(res.tic');
tic = str2num(tic);

% Make sure we only have one trace in this file.
tag = unique(res.tag);
if length(tag) > 1
    error('Trace files with multiple recorders not supported.');
end
tag = tag{1};
tag = lower(tag);

% Convert data to numbers.
data = str2num(cat(2,res.data{:})); %#ok<ST2NM>

% Lookup tag for data fields.
names = dict;
units = dict;
scale = dict;

% Database entries.  Info pulled from code.
var = 'bctl';
names(var) = {'Vdc','Vboost','prog_current','i_inv1','i_inv2'};
scale(var) = [10 10 1 10 10];
units(var) = {'V','V','','A','A'};

var = 'bost';
names(var) = {'Vtgt','Vboost','err_deriv','prog_current'};
scale(var) = [10 10 1 1];
units(var) = {'V','V','',''};

var = 'freq';
names(var) = {'Vac','AC Frequency','Hilbert RPM','Crossing Pt RPM'};
scale(var) = [10 100 100 100];
units(var) = {'V','Hz','RPM','RPM'};

var = 'dpwm';
names(var) = {'Freq','Delta Freq','Integral','PWM','df'};
scale(var) = [100 100 1 18432/100 1];
units(var) = {'Hz','Hz','','','Hz'};


% Time data
tic = tic / 977;
tic = tic - tic(1);

% Create channels
names = names(tag);
scale = scale(tag);
units = units(tag);

for i = 1:size(data,2)
    ch = channel(names{i},data(:,i)/scale(i),tic,units{i});
    chanman('AddChannel',ch);
end
