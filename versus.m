% versus.m
% Plots one channels data versus anothers
% Syntax: versus(chan1,chan2)
%

function [handle] = versus(chan1,chan2,str)

% Error check input
if nargin <3,
   str = '';
end

% Handle different sample rates
rate1 = get(chan1,'SampleRate');
rate2 = get(chan2,'SampleRate');
if rate1 < rate2,
    disp(['Down sampling channel "' get(chan2,'Name') '"']);
    chan2 = resample(chan2,rate1);
end
if rate2 < rate1,
    disp(['Down sampling channel "' get(chan1,'Name') '"']);
    chan1 = resample(chan1,rate2);
end


% Handle different vector lengths
len1 = length(chan1);
len2 = length(chan2);
if len1 < len2,
    disp(['Shortening channel "' get(chan2,'Name') '" for plotting']);
    chan2 = chan2(1:len1);
end
if len2 < len1,
    disp(['Shortening channel "' get(chan1,'Name') '" for plotting']);
    chan1 = chan1(1:len2);
end

% Plot em
handle = plot(get(chan1,'data'),get(chan2,'data'),str);
xlabel([get(chan1,'name') ' [' get(chan1,'units') ']']);
ylabel([get(chan2,'name') ' [' get(chan2,'units') ']']);
title([get(chan2,'name') ' vs. ' get(chan1,'name')]) ;
grid
pt