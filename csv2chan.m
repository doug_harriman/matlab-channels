%CSV2CHAN  Import CSV file to Channel Manager.
%  CSV2CHAN(FILENAME)
%  CSV2CHAN(FILENAME,TICK_FREQ)  Allows specification of 'tick' frequency.
%

function [] = csv2chan(filename,tickfreq)

% Load csv structure
if nargin < 1
    [S,filename] = csv2struct;
else
    S = csv2struct(filename);
end

% Make sure ChannelManager is open.
chanman;

if nargin < 2
    tickfreq = 1;
end

% Potential time vector names, in order of acceptance.
t_names = {'Time','time','tick','ticks','t','T'};

% Look for the time vector.
fields = fieldnames(S);
t = [];
for i = 1:length(t_names)
    if ismember(t_names{i},fields)
        % Found our time vector candidate.
        name = t_names{i};
        t = S.(name);
        
        % Remove from data
        S = rmfield(S,name);
        
        % Break out
        break;
    end
end

% Drop filename field if exists.
if isfield(S,'filename')
    S = rmfield(S,'filename');
end

% If no time vector, just use the tickfreq.
if isempty(t)
    t = 1/tickfreq;
else
    % Update time by tick frequency.
    t = t / tickfreq;
    t = t - t(1);
end

if any(diff(t) < 0)
   dt = mode(diff(t));
   t = dt;
end

% Convert time vector to sample time if possible.
if length(t) > 1
   % Have a time vector.
   dt = diff(t);
   if range(double(dt)) < 1e6*eps
       % Sample time pretty consistent.
       t = dt(1);
   end
end

% Bail if no time vector
if isempty(t)
    error('No time vector found.');
end

% Capture file metadata
meta = chanmeta(filename);

% Set the window title
cm = chanman;
cm.window_title = meta.sourcefile;

% Add channels for rest of fields.
fields = fieldnames(S);
for i = 1:length(fields)
    % Create channel
    name = fields{i};
    
    % Undo GENVARNAME mangling.
    name_nice = strrep(name,'0x2D',' - ');
    
    ch = channel(name_nice,S.(name),t,[],'Metadata',meta);
    chanman('AddChannel',ch);
end
