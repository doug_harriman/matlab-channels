%SCOPE2CHAN  Loads any scope output structures into the Channel Manager.
%  Searches base workspace for Scope 'Structure with time' data and creates
%  channels from any structures found.  After channel creation, the
%  structure is deleted so that duplicate channels are not created on
%  subsequent invocations.
%

function scope2chan

% Make sure we have a Channel Manager Window.
chanman;

% ws = 'base';
ws = 'caller';
vars = evalin(ws,'whos');
idx  = arrayfun(@(x)strcmp(x.class,'struct'),vars);
idx  = find(idx);

% Process each structure looking for a valid scope structure.
for i = 1:length(idx)
   % Get the var.
   varname = vars(idx(i)).name;
   var = evalin(ws,varname);
   
   % Look for fields
   if ~all(isfield(var,{'time','signals','blockName'}))
       % Skip ahead
       continue;
   end

   % Extract block name.
   str = var.blockName;
   str = strrep(str,'//','|');  % Hack to handle '/' in units.
   str = words(str,'/');
   str = str{end};
   str = strrep(str,'|','/');
   
   % Extract name & units
   s = regexp(str,'(?<name>[^\[]+)\[(?<units>[^\]]+)','names');
   if ~isempty(s)
       name  = strtrim(s.name);
   else
       name = str;
   end
   
   % Eliminate CR's
   name = strrep(name,char(10),' ');
   
   % Create channel.
   ch = channel(name,var.signals.values,var.time,s.units);
   
   % Add the channel
   chanman('AddChannel',ch);
   
   % Clear the value.
   evalin('base',['clear(''' varname ''');']);
end
