% label.m
% Puts test label on figure
% Syntax: handle = label(channel,figure)
%   handle  - handle of axes containing text.
%   channel - channel that was plotted.
%   figure  - Optional, figure to add channel to.
%             If not provided, current figure will
%             be used.

function axhan = label(chan,fig) 

% Get figure
if nargin == 1,
   fig = gcf ;
else,
   figure(fig) ;
end

% Get current axes
plotax = gca ;

% Create label axes
axhan = axes('position',[0 0 1 1],...
   'visible','off') ;

% Build Strings
chan = struct(chan) ;
test = chan.test ;
str1 = [test.type char(10) test.name] ;
str2 = [test.time char(10) test.date] ;
str3 = 'HP Confidential'          ;

% Shift current plot
pos = get(plotax,'position');
pos(2) = pos(2) + 0.04 ;
pos(4) = pos(4) - 0.04 ;
set(plotax,'position',pos) ;

% Place strings
h = text(.025,.05,str1);
%set(h,'interpreter','tex');
%text(.85,.05,str2);
text(0.45,0.025,str3);

% Switch back to original axes
axes(plotax) ;

% Hide the axes
set(axhan,'HandleVisibility','off');
