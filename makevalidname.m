%MAKEVALIDENAME  Makes a valid Matlab variable name 
%                from a general string.  All illegal
%                characters are deleted.
%

% DLH 9/22/99

function validName = makevalidname(inStr)

inStr = double(inStr);

inStr(inStr<48) = '';
inStr( (inStr>57) & (inStr<65) ) = '';
inStr( (inStr>90) & (inStr<95) ) = '';
inStr( (inStr>95) & (inStr<97) ) = '';
inStr(inStr>122) = '';

validName = char(inStr);