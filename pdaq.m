%PDAQ  Imports pDAQ text file with metadata.
%

% DLH 
% 06/24/04 - Auto move to correct dir.
% 05/19/04 - Created

function [] = pdaq_import(fileName,daytime);

if nargin < 2,
    daytime = 1;
end

if nargin == 0,
    curDir = pwd;
    cd('C:\data\ASCII');
    
    % Autoload if only 1 file exists.
    d = dir('*.txt');
    
    if length(d) == 1,
        % Autoload
        fileName = [pwd filesep d.name];
    else
        % Query user
        [fileName, pathName] = uigetfile('*.txt','Select pDAQ Data File');

        % Handle UI cancel
        if fileName == 0,
            cd(curDir);
            return;
        end

        % Full file name
        fileName = [pathName filesep fileName];
    end

    % Return to previous directory.
    cd(curDir);
end

% Read the 2 header lines
fid = fopen(fileName,'r');
if fid < 0,
    error(['Unable to open file: ' fileName]);
end
header = fgetl(fid);
unitstr  = fgetl(fid);
fclose(fid);

% Provide minimal chanmetadata
% Define the test info
d = dir(fileName);
meta.file_name = fileName;
file_date = datenum(d.date);
meta.date = datestr(file_date,23);
meta.time = datestr(file_date,16);
meta.test_name = 'Thermocouple';

% Break up header
header  = words(header,char(9));
if isempty(header{end}),
    header = header(1:end-1);
end

% Break up units str.
unitstr = words(unitstr,char(9));
if isempty(unitstr{end}),
    unitstr = unitstr(1:end-1);
end
numvariables = length(unitstr);

% Start building up textread call string
outvariables = {};
formatStr = '';

% Look for timestamp components
ind = strmatch('Time',header);
if ~isempty(ind),
    % Have HH:MM:SS timestamp
    timeVars = {'hourval','minuteval','secondval'};
    formatStr = [formatStr '%d:%d:%d\t'];
    outvariables  = {outvariables{:},timeVars{:}};
end

ind = strmatch('ms',header);
if ~isempty(ind),
    % Have millisecond timestamp
    timeVars = {'millisecval'};
    formatStr = [formatStr '%d\t'];
    outvariables  = {outvariables{:},timeVars{:}};
end

ind = strmatch('Date',header);
if ~isempty(ind),
    timeVars = {'monthval','dayval','yearval'};
    formatStr = [formatStr '%d/%d/%d\t'];  % 'MM/DD/YY date format
%     formatStr = [formatStr '%d-%3c-%d\t'];   % 'DD-MMM-YY date format
    outvariables  = {outvariables{:},timeVars{:}};
end

% Build up rest of strings
outvariables = {outvariables{:}, ['variable{1:' num2str(numvariables) '}']};
variableStr = ones(numvariables,1) * ['\t%f'];
variableStr = char(reshape(variableStr',1,numel(variableStr)));
formatStr = [formatStr variableStr];
formatStr = ['''' formatStr ''''];

% Final command string
% Output variableiables
cmdStr = '[';
for i = 1:length(outvariables)-1,
    cmdStr = [cmdStr outvariables{i} ','];
end
cmdStr = [cmdStr outvariables{end} ']'];

% Function call
cmdStr = [cmdStr ' = textread(''' fileName ''',' formatStr ',''headerlines'',2,''delimiter'',''\t'');'];
eval(cmdStr);

% Use time stamp data if present.
if exist('hourval'),
    time = [hourval*3600 + minuteval*60 + secondval];
    header = header(2:end);
end

% Drop date data if present.
if exist('monthval'),
    header = header(2:end);
end

% Use millisecond stamp if exists
if exist('millisecval'),
    warning('Millisecond values returned from PDAQ are bogus');
%     time = time + millisecval/1000;
    header = header(2:end);
end

% Start time at zero.
time = time - time(1);

% Build up output channels
chanman;
for i = 1:numvariables,
    % Make sure unitstr a legal char
    if unitstr{i}(1) == '�',
        unitstr{i} = '';
    end
    
    % Make sure header is a legal Matlab variable name
    if isnum(header{i}(1)),
        header{i} = ['Data ' header{i}];
    end
    
    % Clean up name
    header{i} = strrep(header{i},'_',' ');
    
    chan = channel([header{i} ' Thermocouple Data'],variable{i},time,unitstr{i},...
        'PlotColor','r',...
        'Metadata',meta);
    chanman('AddChannel',chan);
end