% channels.m  Displys a string matrix of all channels
% 

function [chanNameList] = channels()

% Get all variables
vars = evalin('base','whos');

% Setup string placeholders
varStr  = str2mat('Variable ','-------- ');
nameStr = str2mat('Name ','----- ');
freqStr = str2mat('Frequency','---------- ');
pntsStr = str2mat('Size ','----- ');
unitStr = str2mat('Units ','----- ');

% Channels exists flag
channelsExist = 0;

% Cycle through
chanCount = 0;
if ~isempty(vars)
   for i = 1:length(vars)
      % Test it
      if strcmp(vars(i).class,'channel'),
         chan    = evalin('base',vars(i).name);
         varStr  = str2mat(varStr,[vars(i).name '  ']);
         nameStr = str2mat(nameStr,[get(chan,'name') ' ']);
         try
            freqStr = str2mat(freqStr,[num2strp(get(chan,'SampleRate')) 'Hz  ']);
         catch
            freqStr = str2mat(freqStr,[' ']);
         end
         pntsStr = str2mat(pntsStr,[num2str(get(chan,'points')) ' Points  ']);
         unitStr = str2mat(unitStr,['[' get(chan,'units') ']  ']);
         
         channelsExist = 1;
         
         chanCount = chanCount + 1;
         chanNameList{chanCount} = get(chan,'name');
      end
   end
   
   % Display if we had any channels.
   if channelsExist
      disp([varStr nameStr unitStr freqStr pntsStr]);
      disp(' ') ;
   else
      disp('No Channel objects exist in the Workspace.');
   end
   
else
   disp('Workspace is empty.');
end

if nargout == 0, clear chanNameList; end