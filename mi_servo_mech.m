%MI_SERVO_MECH  Get's data on current mech.
%  Note: Expects that hw board name matches mi_servo mech name.
%
% Commands:
%  MechList - Lists all mechs currently defined.
%  MechGet  - Returns currently set mech.
%  MechSet  - Sets current mech.
%  GUI      - Open mech selection GUI.
%  AxisList - Returns list of axes for current mechanism.
%  Update   - Re-reads configuration from source code repository.
%             See REPO for more information.
%

% Doug Harriman (doug.harriman@hp.com)

function [val] = mi_servo_mech(cmd,val)

if nargin == 0
    cmd = 'gui';
    help(mfilename);
end

% Constants
GRP = 'MI_SERVO';
% varname = 'CurrentMech';
%
% if nargin == 0
%     % Get call
%     if ~ispref(grp,varname)
%         warning('No mech set.  Set with: >> mi_servo_mech <mech>');
%         val = [];
%         return;
%     end
%
%     val = getpref(grp,varname);
%     return;
% end
%
% % Set call
% setpref(grp,varname,val);

switch(lower(cmd))
    case 'update'
        def = Update;

        % Store it out
        VAR = 'dict';
        setpref(GRP,VAR,def);

        % UI
        disp('Mechanism list updated from source code repo:')
        disp(cc_repo)

    case 'mechlist'
        try
            val = getpref(GRP,'dict');
            val = val.keys;
        catch
            mi_servo_mech('Update');
            val = mi_servo_mech('MechList');
        end

    case 'mechset'
        % Error check inputs
        if nargin < 2
            error('No mechanism specified');
        end

        % Make sure on list
        val = lower(val);

        list = mi_servo_mech('MechList');
        if ~ismember(val,list)
            error(['Specified mechanism invalid: ' upper(val)]);
        end

        % Set it
        setpref(GRP,'CurrentMech',val);

    case 'mechget'
        if ~ispref('MI_SERVO','CurrentMech')
            error('No mech currently set.');
        end
        
        val = getpref(GRP,'CurrentMech');

    case 'gui'
        % List of mechs
        list_mech = mi_servo_mech('MechList');
        try 
            cur_mech  = mi_servo_mech('MechGet');

            % Convert mech name to index
            [tf,idx] = ismember(cur_mech,list_mech);

        catch
            idx = 1;
        end

        % Dialog
        [sel,ok] = listdlg('PromptString','Select Active Mech',...
            'Name','MI Servo Mech',...
            'ListString',list_mech,...
            'InitialValue',idx,...
            'SelectionMode','single');

        % Update data
        if ok
            mi_servo_mech('MechSet',list_mech{sel});
        end

    case 'axislist'
        def  = getpref(GRP,'dict');
        mech = getpref(GRP,'CurrentMech');
        val  = def(mech);

    otherwise
        disp(' ');
        disp(['Unsupported command: ' upper(cmd)]);
        disp(' ');
        help(mfilename);
end


function [def] = Update

% Get path
filepath = [cc_repo 'hw' filesep 'src' filesep];

% Get list of boards
boards = dir([filepath 'board*']);
boards = scalarstruct(boards);
boards = boards.name;

% Remove non-mech boards
boards = setdiff(boards,'board_ifc');
boards = setdiff(boards,'board_emu_empty');

% Biff any boards that don't contain our file
idx = true(size(boards));
for i = 1:length(boards)
    % Build the file name
    board = boards{i};
    filename = [filepath board filesep 'src' filesep 'ifc' filesep ...
        'lib_motorsystem_decl.h'];

    if ~exist(filename)
        idx(i) = false;
    end
end
boards = boards(idx);

% Start building the dictionaries
def = dict;
for i = 1:length(boards)
    % Build the file name
    board = boards{i};
    filename = [filepath board filesep 'src' filesep 'ifc' filesep ...
        'lib_motorsystem_decl.h'];

    % Read all config lines.
    cfg = readcfg(filename);

    % Extrace the first entry;
    list = cellfun(@(x)x{1},cfg,'UniformOutput',false);

    % Store into our mech dictionary.
    board = strrep(board,'board_','');
    def(board) = list;
end
