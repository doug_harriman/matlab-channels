% bobincrease.m
% Interpolates data.
%

function out = bobincrease(in,factor) ;

% Error check factor
if ~rem(factor,1) & factor ~= 1,
   error('Factor must be integer valued');
end

% Get data
x = get(in,'time');
y = get(in,'data');
del_x = mean(diff(x));

% Interpolate it
j=1;
for i = 1:length(in),
   for k = 0:factor,
      out_data(j) = interp1(x,y,x(i)+k/(factor+1)*del_x);
      j=j+1;
   end
end

% Eliminate NaN's from interpolation
out_data = out_data(~isnan(out_data));

% Setup output
out = in;
set(out,'samplerate',(factor+1)*get(in,'samplerate'));
set(out,'data',out_data);
