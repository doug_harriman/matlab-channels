%MATDAQ  Matlab Data Acquisition.
% This function provides an interface to a 
% National Instruments Data Acquisition card
% through ComponentWorks software.
%

% Doug Harriman
% Hewlett-Packard VCD
% doug_harriman@hp.com
% (360) 212-6522
% May 8, 1998

function [varargout] = matdaq(msg,varargin)

% Determine MatDAQ handle
set(0,'ShowHiddenHandles','on');
MatDAQFig = findobj(get(0,'children'),'Tag','MatDAQFig');
set(0,'ShowHiddenHandles','off');

% Create window if necessary
if isempty(MatDAQFig)
   
   % Figure
   figWidth  = 560;
   figHeight = 245;
   MatDAQFig = figure('NumberTitle','off',...
      'Position',[700 725 figWidth figHeight],...
      'MenuBar','None',...
      'Tag','MatDAQFig',...
      'Resize','Off',...
      'DeleteFcn','matdaq(''cleanup'');',...
      'Name','MatDAQ');
%   
   % Test Definition Frame
   frmWidth  = 250;
   frmHeight = 180;
   frmX      = 3;
   frmY      = figHeight - frmHeight;
   uicontrol('Style','Frame',...
      'Position',[frmX frmY frmWidth frmHeight]);
   
   thisWidth  = frmWidth - 6;
   thisHeight = 20;
   thisX      = frmX + 3;
   thisY      = frmY + frmHeight - thisHeight - 3;
   uicontrol('Style','Text',...
      'String','Test Definition',...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   delta      = 25;
   thisWidth  = 30;
   thisHeight = 20;
   thisX      = thisX + 10;
   thisY      = thisY - delta;
   uicontrol('Style','Text',...
      'String','Type:',...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisY      = thisY - delta;
   uicontrol('Style','Text',...
      'String','Name:',...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisY      = thisY - delta - 3;
   uicontrol('Style','Text',...
      'String','Time:',...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisY      = thisY - delta;
   uicontrol('Style','Text',...
      'String','Date:',...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisY      = thisY - delta;
   thisWidth  = 100;
   uicontrol('Style','Text',...
      'String','Sample Rate:',...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisY      = thisY - delta;
   uicontrol('Style','Text',...
      'String','Duration:',...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   % Edit boxes
   thisWidth  = 195;
   thisHeight = 20;
   thisX      = frmX + 48;
   thisY      = frmY + frmHeight - thisHeight - delta;
   uicontrol('Style','Edit',...
      'Tag','TestType',...
      'String','Not Defined',...
      'HorizontalAlignment','Left',...
      'BackgroundColor',[1 1 1],...
      'Position',[thisX thisY thisWidth thisHeight]);

   thisY      = thisY - delta;
   uicontrol('Style','Edit',...
      'Tag','TestName',...
      'String','Not Defined',...
      'HorizontalAlignment','Left',...
      'BackgroundColor',[1 1 1],...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisY      = thisY - delta - 6;
   uicontrol('Style','Text',...
      'Tag','TestTime',...
      'String',datestr(now,16),...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisY      = thisY - delta;
   uicontrol('Style','Text',...
      'Tag','TestDate',...
      'String',datestr(now,2),...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisY      = thisY - delta + 5;
   thisX      = thisX + 40;
   thisWidth  = 70;
   uicontrol('Style','Edit',...
      'Tag','TestRate',...
      'String','4000',...
      'HorizontalAlignment','Left',...
      'BackgroundColor',[1 1 1],...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisY      = thisY - delta;
   uicontrol('Style','Edit',...
      'Tag','TestDuration',...
      'String','1',...
      'HorizontalAlignment','Left',...
      'BackgroundColor',[1 1 1],...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   % Labels
   thisY      = thisY + delta - 2;
   thisWidth  = 20;
   thisX      = thisX + 75 ;
   uicontrol('Style','Text',...
      'String','Hz',...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisY      = thisY - delta;
   uicontrol('Style','Text',...
      'String','sec',...
      'HorizontalAlignment','Left',...
      'Position',[thisX thisY thisWidth thisHeight]);
%   
   
   % Acquire button frame and button
   frmWidth  = 250;
   frmHeight = 25;
   frmY      = frmY - frmHeight - 5;
   uicontrol('Style','Frame',...
      'Position',[frmX frmY frmWidth frmHeight]);
   
   thisWidth  = frmWidth/2 - 9;
   thisHeight = 20;
   thisX      = frmX + 6;
   thisY      = frmY + frmHeight - thisHeight - 3;
   uicontrol('Style','PushButton',...
      'String','Acquire Data',...
      'Callback','matdaq(''acquire'');',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   thisX      = thisX + thisWidth + 6;
   uicontrol('Style','PushButton',...
      'Tag','DumpFile',...
      'String','Dump File',...
      'ToolTipString','Copy binary file to printer',...
      'Enable','Off',...
      'Callback','matdaq(''DumpFile'');',...
      'Position',[thisX thisY thisWidth thisHeight]);
   
   % Channel extended button
   thisY      = 6;
   uicontrol('Style','ToggleButton',...
      'Tag','ChannelBtn',...
      'String','Channels',...
      'ToolTipString','Show/Hide channel definitions',...
      'Value',1,...
      'Callback','matdaq(''HideChannels'');',...
      'Position',[thisX thisY thisWidth thisHeight]);   
      
%   
   % Channel listbox
   % Context menu
   channelListMnu = uicontextmenu('Tag','ChannelListMenu');
   uimenu(channelListMnu,'Label','Edit',...
      'Tag','EditChannelMenu',...
      'Enable','Off');
   uimenu(channelListMnu,'Label','Enable',...
      'Tag','EnableChannelMenu',...
      'Enable','Off');
   uimenu(channelListMnu,'Label','Copy',...
      'Separator','On',...
      'Tag','CopyChannelMenu',...
      'Enable','Off');
   uimenu(channelListMnu,'Label','Paste',...
      'Tag','PasteChannelMenu',...
      'Enable','Off');
   
   
   thisWidth  = 300 ;
   thisHeight = 243 ;
   thisX      = frmWidth + 10;
   thisY      = frmY - 30;
   uicontrol('Style','ListBox',...
      'Tag','ListBox',...
      'FontName','r_ansi',...
      'Callback','matdaq(''EditChannel'',get(gco,''value''));',...
      'UIContextMenu',channelListMnu,...
      'Position',[thisX thisY thisWidth thisHeight]);
   matdaq('DefaultChannels');
   matdaq('DisplayChannels');
   
   % Menus
   fileMnu = uimenu('Label','&File');
   uimenu(fileMnu,'Label','&Open',...
      'Callback','matdaq(''OpenTest'');');
   uimenu(fileMnu,'Label','&Save',...
      'Callback','matdaq(''SaveTest'');');   
   uimenu(fileMnu,'Label','E&xit',...
      'Callback','matdaq(''Exit'');',...
      'Separator','On');
   
   settingsMnu = uimenu('Label','&Settings');
   uimenu(settingsMnu,'Label','&Dump File',...
      'Callback','matdaq(''SetDumpFile'');');
   uimenu(settingsMnu,'Label','&Output',...
      'Enable','Off',...
      'Callback','');   
   
   % Create ActiveX Control Container Figure
   screenSize = get(0,'ScreenSize');
   width  = 205 ;
   height = 20  ;
   positionX = (screenSize(3) - width)/2;
   positionY = (screenSize(4) - height)/2;
   MatDAQControlFig = figure('NumberTitle','off',...
      'Position',[positionX positionY width height],...
      'MenuBar','None',...
      'Tag','MatDAQControlFig',...
      'Resize','off',...
      'Visible','off',...
      'Name','Data Acquisition');
   
   
   % Create ActiveX Control
   MatDAQControl = actxcontrol('Matdaq.DAQ',...
      [0 0 205 20],MatDAQControlFig,'matdaq');
         
   % Store MatDAQControl handles
   file = [pwd '\MatDAQ.temp'] ;
   set(MatDAQControl,'DataFile',file);
   setuprop(MatDAQFig,'TempFile',file);
   setuprop(MatDAQFig,'MatDAQControl',MatDAQControl);
   setuprop(MatDAQFig,'MatDAQControlFig',MatDAQControlFig);
   setuprop(MatDAQFig,'DumpFile','');
   
   % Hide the figure's handles
   set(MatDAQFig,'HandleVisibility','callback');
   set(MatDAQControlFig,'HandleVisibility','off');
   
end

% Call MatDAQ to the front
figure(MatDAQFig);

% Parse input arguments
if nargin < 1,
   msg = '';
end

switch lower(msg)
%----------------------------------------------------------   
case 'editchannel'
   
   % Get data
   channels = getuprop(MatDAQFig,'Channels');
   index    = varargin{1};
   chan     = channels{index};
   
   % Setup for dialog box
   prompt  = {'Name:','Units:','Enabled(0/1):','Scaling:','Offset:'};
   default = {chan.name,chan.units,num2str(chan.enabled),...
         num2str(chan.scaling),num2str(chan.offset)};
   title   = 'Channel Definition';
   lineNo  = 1;
   
   % Get new test and parse to output
   newChan = inputdlg(prompt,title,lineNo,default);
   
   if ~isempty(newChan)
      % Parse it
      chan.name    = newChan{1};
      chan.units   = newChan{2};
      chan.enabled = str2num(newChan{3});
      chan.scaling = str2num(newChan{4});
      chan.offset  = str2num(newChan{5});
      
      % Store it
      channels{index} = chan;
      setuprop(MatDAQFig,'Channels',channels);
      
      % Write it
      matdaq('displaychannels');   
   end
   
      
%----------------------------------------------------------   
case 'defaultchannels'
   
   % Generate channels
   for i = 1:16, 
      channels{i}.name  = 'Not Defined';
      channels{i}.units = 'None';
      channels{i}.enabled = 0 ;
      channels{i}.scaling = 1 ;
      channels{i}.offset  = 0 ;
   end
      
   % Store it
   setuprop(MatDAQFig,'Channels',channels);
   
%----------------------------------------------------------   
case 'displaychannels'
   
   % Get handle
   handleList = get(MatDAQFig,'Children');
   listBoxHan = findobj(handleList,'Tag','ListBox'); 
   
   % Get data
   channels = getuprop(MatDAQFig,'Channels');
   
   % Generate string matrices
   chanNum='';chanName='';chanUnits='';chanScaling='';chanOffset='';
   for i = 1:16,
      chanNum  = str2mat(chanNum,[num2str(i) ')']);
      chanName = str2mat(chanName,channels{i}.name);
      if channels{i}.enabled
         chanUnits   = str2mat(chanUnits,channels{i}.units);
         chanScaling = str2mat(chanScaling,num2str(channels{i}.scaling));
         chanOffset  = str2mat(chanOffset,num2str(channels{i}.offset));
      else   
         chanUnits   = str2mat(chanUnits,'Disabled');
         chanScaling = str2mat(chanScaling,'');
         chanOffset  = str2mat(chanOffset,'');
      end
   end
   
   % Convert to cell array
   for i = 2:17,
      string{i-1} = [chanNum(i,:) '  ' chanName(i,:) '  ' chanUnits(i,:),...
            '  ' chanScaling(i,:) '  ' chanOffset(i,:)];
   end
   
   % Write it
   set(listBoxHan,'String',string);
   
%----------------------------------------------------------   
case 'tochanman'
   
   % Open Channel Manager if necessary
   if isempty(chanmanhan),
      chanman;
   end
   
   % Send data to chanman
   data = varargin{1};
   MatDAQControl = getuprop(MatDAQFig,'MatDAQControl');
   sampleFreq = get(MatDAQControl,'SampleFrequency');
   handleList = get(MatDAQFig,'Children');
   channels = getuprop(MatDAQFig,'Channels');
   
   % Get test definition
   test.type = get(findobj(handleList,'tag','TestType'),'String');
   test.name = get(findobj(handleList,'tag','TestName'),'String');
   test.time = get(findobj(handleList,'tag','TestTime'),'String');
   test.date = get(findobj(handleList,'tag','TestDate'),'String');
toc   
   % Loop through all channels
disp('Writing data to Channel Manager...');drawnow
   for i = 1:16,
      if channels{i}.enabled
         chan = channel(channels{i}.name,...
            data(:,i)*channels{i}.scaling+channels{i}.offset,...
            inv(sampleFreq),channels{i}.units,test) ;
         chanman('AddChannel',chan);
      end
   end
disp('Done');drawnow    
toc   
%----------------------------------------------------------   
case 'acquire'
tic   
   % Update test time and date
   set(findobj('Tag','TestTime'),'String',datestr(now,16));
   set(findobj('Tag','TestDate'),'String',datestr(now,2));
   
   % Set up acquisition parameters
   MatDAQControl = getuprop(MatDAQFig,'MatDAQControl');
   set(MatDAQControl,'SampleFrequency',...
      str2num(get(findobj(get(MatDAQFig,'children'),'Tag','TestRate'),'String')));
   set(MatDAQControl,'Duration',...
      str2num(get(findobj(get(MatDAQFig,'children'),'Tag','TestDuration'),'String')));
   set(MatDAQControl,'DataFile',getuprop(MatDAQFig,'TempFile'));
   
   % Make the status bar visible
   set(getuprop(MatDAQFig,'MatDAQControlFig'),'visible','on');
   
   % Start the test
   invoke(MatDAQControl,'AcquireData');
   
%----------------------------------------------------------   
case 'readdata'
   
   % Data set info
   MatDAQControl = getuprop(MatDAQFig,'MatDAQControl');
   numChans      = 16 ; % Hard coded into ActiveX control
   pointsPerChan = get(MatDAQControl,'SampleFrequency')* ...
      get(MatDAQControl,'Duration');
   numPoints     = numChans * pointsPerChan;
   
   % File open, read, and close
   disp('Reading data file...');drawnow
   file = getuprop(MatDAQFig,'TempFile');
   fid = fopen(file,'r');
   data = zeros(numPoints,1);  % Preallocate storage
   [data,cnt] =  fscanf(fid,'%f',numPoints);
   fclose(fid);
   
   % Reshape data
   varargout{1} = reshape(data,pointsPerChan,numChans);
 
%----------------------------------------------------------   
case '1'
   % Data acquired
   % Hide acquisition window
   set(getuprop(MatDAQFig,'MatDAQControlFig'),'Visible','off');
   drawnow
   
   % Get the data
   data = matdaq('ReadData');
   
   % Delete the temp file
   delete(getuprop(MatDAQFig,'TempFile'));
   
   % Plot the data 
   matdaq('ToChanman',data);
%----------------------------------------------------------   
case 'exit'
   close(MatDAQFig);
   
%----------------------------------------------------------   
case 'cleanup'
   
   % Cleans up things created by MatDAQ
   % Called on exit
   
   % Delete MatDAQControl and fig
   delete(getuprop(MatDAQFig,'MatDAQControl'));
   delete(getuprop(MatDAQFig,'MatDAQControlFig'));
   

%----------------------------------------------------------   
case 'savetest'
   % Get file name
   [fileName, pathName] = uiputfile('*.test','Save Test Definition');
   
   % Get relevant data 
   handleList    = get(MatDAQFig,'Children');
   test.type     = get(findobj(handleList,'tag','TestType'),'String');
   test.duration = get(findobj(handleList,'tag','TestRate'),'String');
   test.rate     = get(findobj(handleList,'tag','TestDuration'),'String');
   channels      = getuprop(MatDAQFig,'Channels');
   
   % Save it
   save([pathName '\' fileName],'test','channels');%,'dumpFile');
   
%----------------------------------------------------------   
case 'opentest'
   
   % Get file name
   [fileName, pathName] = uigetfile('*.test','Load Test Definition');
   
   % Save it
   load([pathName fileName],'-mat');
   
   % Get relevant data 
   handleList = get(MatDAQFig,'Children');
   set(findobj(handleList,'tag','TestType'),'String',test.type);
   set(findobj(handleList,'tag','TestRate'),'String',test.duration);
   set(findobj(handleList,'tag','TestDuration'),'String',test.rate);
   setuprop(MatDAQFig,'Channels',channels);
   dumpFileBtn = findobj(handleList,'Tag','DumpFile');
   set(dumpFileBtn,'Enable','On');
   
   matdaq('DisplayChannels');
   
%----------------------------------------------------------   
case 'setdumpfile'
   % Get file name
   [fileName, pathName] = uigetfile('*.scp;*.p*','Dump File');
   
   % Store it
   setuprop(MatDAQFig,'DumpFile',[pathName fileName]);
   
   % Enable dumpfile button
   handleList  = get(MatDAQFig,'Children');
   dumpFileBtn = findobj(handleList,'Tag','DumpFile');
   set(dumpFileBtn,'Enable','On');
   
%----------------------------------------------------------   
case 'dumpfile'
   % Get file
   dumpFile = getuprop(MatDAQFig,'DumpFile');
   
   % Copy to LPT1
   str = lower(['! copy /b ' dumpFile{1} ' lpt1']);
   eval(str);
   
%----------------------------------------------------------   
case 'hidechannels'
   % Change the button label and callback
   handleList  = get(MatDAQFig,'Children');
   hideHan = findobj(handleList,'Tag','ChannelBtn');
   set(hideHan,'Callback','matdaq(''ShowChannels'');');
   
   % Change the figure size
   pos = get(MatDAQFig,'Position');
   pos(3) = 255;
   set(MatDAQFig,'Position',pos);
   
%----------------------------------------------------------   
case 'showchannels'
   % Change the button label and callback
   handleList  = get(MatDAQFig,'Children');
   hideHan = findobj(handleList,'Tag','ChannelBtn');
   set(hideHan,'Callback','matdaq(''HideChannels'');');
   
   % Change the figure size
   pos = get(MatDAQFig,'Position');
   pos(3) = 560;
   set(MatDAQFig,'Position',pos);
   
%----------------------------------------------------------   

end

   