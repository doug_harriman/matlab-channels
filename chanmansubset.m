%CHANMANSUBSET  Reduces Channel data by keeping only subset of provided indecies.
%  CHANMANSUBSET prompts for index values.
%  CHANMANSUBSET(INDEX) uses provided index values.
%

function [] = chanmansubset(index)

% Error checks & defaults
error(nargchk(0,1,nargin));

% Prompt for indecies if not provided.
if nargin < 1
    % GUI prompt
    prompt  = 'Enter data indecies';
    title   = 'Data Subset Specification';
    n_lines = 1;
    def     = {''};
    res = inputdlg(prompt,title,n_lines,def);

    % Check for 'cancel'
    if isempty(res)
        warning('No data index values provided.');
        return;
    end

    % Parse response
    index = res{1};
    if isempty(index)
        warning('No data index values provided.');
        return;
    end

    % Allow variables for index by evaluating in the base workspace.
    index = evalin('base',index);
end 

% Validate
if ~isnumeric(index)
    error('Numeric value expected.');
end
if ~isvector(index)
    error('Index vector expected.');
end

% Get channel list from Channel Manager
try 
    ch_list = chanman('GetChannelAll');
catch
    error('Channel Manager query failed.  Is the Channel Manager open?');
end
if isempty(ch_list)
    error('No channel data in Channel Manager');
end

% Flip list so channels get added back in correct order.
chanman('ClearAll');

% Map over all channel in channel manager.
ch_cnt = length(ch_list);
for i = 1:ch_cnt
    % Get current channel
    ch = ch_list{i};

    % Extract subsets.
    try
        ch = ch(index);
    catch
        % If unable to extract, then user has provided bogus channel data.
        % Warn, but put channel back as is.
        warning(['Unable to exctract subset for channel: "' get(ch,'Name') '"']);
        chanman('AddChannel',ch);
        continue;
    end

    % Zero out time offset.
    ch = set(ch,'TOffset',0);

    % Put the channel back into the Channel Manager.
    chanman('AddChannel',ch);

end % ch_cnt
