%NUM2STRP
%

function str = num2strp(num,msg) 

% Pick long or short
if nargin == 1, 
   msg = 'short' ;
end

% Convert to test string
teststr = num2str(num,'%5.4e') ;

% Find the exponent
ind = findstr(teststr,'e') ;

% Break it down
mant = teststr(1:ind-1) ;
expon = teststr(ind+1:length(teststr)) ;

% Convert back
expon = str2double(expon);
if expon >= 0,
   mant = str2double(mant)*10^(mod(abs(expon),3)) ;
else
   mant = str2double(mant)/10^(abs(mod(abs(expon),3))) ;
end

% Change expon to a mult of 3
expon = fix(expon/3) ;

% Lookup tables
val = [-1 0 1 2 3 4] ;
switch msg
    case 'long'
        str = str2mat('milli','','kilo','mega','giga') ;
    case 'short'
        str = str2mat('m','','k','M','G') ;
end

% Create output string
str = [num2str(mant) ' ' deblank(str(find(expon==val),:))] ;

