%CLEGEND  Channels legend. 
%   H=CLEGEND(LOCATION) creates a legend for 
%   channels plotted in the current axes.  H is
%   the legend handle, and LOCATION sets the 
%   placement location of the axes. See 
%   HELP LEGEND for more information.
%

% DLH
% 06/11/04 - Created

function [han] = clegend(loc)

% Error check
if nargin > 1, error('One input expected.'); end

% Get channel strings from plot
lineHan = get(gca,'Children');
lineHan = findobj(lineHan,'Type','line');
nLines = length(lineHan);
for i = 1:nLines,
    if isappdata(lineHan(i),'ChannelName'),
        str{nLines-i+1} = getappdata(lineHan(i),'ChannelName'); %#ok<*AGROW>
    elseif ~isempty(get(lineHan(i),'DisplayName'))
        str{nLines-i+1} = get(lineHan(i),'DisplayName');
    else
        str{nLines-i+1} = ['data ' num2str(i)];
    end
end

if nargin == 1
    clicklegend(str,loc);
else
    clicklegend(str,'Location','Best');
end