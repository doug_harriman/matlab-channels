%HANNING  Generates hanning window of a given size.
% 
% >> window = hanning(num_points)
%

% DLH, 11/05/98

function win = hanning(pnts)

w = 1;  % One cycle per in the window.
delta = 2*pi/pnts;
time = [delta:delta:2*pi];
win = -0.5*cos(w*time)+0.5;
win = win;

