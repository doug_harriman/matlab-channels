%SS_TORQUE_IMPORT  Imports SS torque data from 
%                  Yokogawa scope to a channel.
%

function [] = ss_torque_import(fileName,testDef)

persistent filt1
if isempty(filt1),
   load vibrac_filter.mat
end

% Constants
sampleRate = 5000;
defaultPath = 'c:\';
chanName = 'SS Drive Torque';
chanUnits = 'oz*in';

originalPath = cd;

% Argument Check
if nargin < 1,
	% Move to default path
	cd(defaultPath);
   
   [fileName,pathName] = uigetfile('*.asd','Load Yokogawa Data File');
   
   if fileName == 0,
      % User cancelled
      disp('No file selected');
      return;
   end
   fileName = [pathName fileName];
end

if nargin < 2,
   testDef.type = 'SS Drive Torque';
   testDef.time = datestr(now,16);
   testDef.date = datestr(now,2);
   testDef = edtest(testDef);
end


% Load file
disp(['Loading data file: ' fileName]);
dataSize = 1e5;
data = zeros(dataSize,1);  % Preallocate storage
i=1;

% Strip Header
fid = fopen(fileName,'r');
l=fgetl(fid);
continueFlag = 1;
while (continueFlag)
   if ~isempty(findstr(l,'Time')),
		continueFlag = 0;      
   else,
		l=fgetl(fid);
   end
end

% Read the data
l=fgetl(fid);
l=fgetl(fid);
while (l~=-1),
   data(i) = sscanf(l,'%f');
   
   if (mod(i,10000)==0),
   	disp([num2str(i) ' points read']);   
   end
   
   i = i + 1;
   l = fgetl(fid);
   
   if (i>dataSize)
      data = [data; zeros(dataSize,1)];
   end
end
fclose(fid);

% Truncate vector to just read data
data = data(1:i);

% Build channel
disp('Building channel...');
chan = channel(chanName,data,1/sampleRate,chanUnits,testDef);
disp('Filtering Data...');
chan=filter(chan,filt1);
chanman('AddChannel',chan);

% Return to original path
cd(originalPath);
disp('Done importing SS torque data file');