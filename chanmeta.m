%CHANMETA  Channel metadata.
%  Channel metadata storage class.  The intent of this class is to store
%  and display metadata about a channel.
% 

% Written by: Doug Harriman

classdef chanmeta
    properties
        % Original non-Matlab source of data.
        sourcefile; 
        
        % Path to original source file
        sourcepath;
        
        % Source file time stamp.
        sourcetimestamp;
        
        % Import time stamp.
        importtimestamp;
        
        % Project with which data is associated.
        project;
        
        % General notes.
        notes;
        
        % Version of object.
        version;
        
    end % properties - public
    
    methods
        function obj = chanmeta(filename)
            %CHANMETA  Channel metadata constructor.
            %  OBJ=CHANMETA(FILENAME) creates a CHANMETA object with
            %  data for FILENAME.
            %
        
            % Set version.
            obj.version = 1;
            
            if nargin < 1
                return;
            end
            
            % Try to get info on file.
            if exist(filename,'file') ~= 2
                warning(['Unable to find file: ' filename]);
                return;
            end
            
            % Grab the data.
            d = dir(filename);
            [pn,fn,ext] = fileparts(filename);
            fn = [fn ext];
            if isempty(pn)
                pn = pwd;
            end
            
            % Soure file name.
            obj.sourcefile = fn;
            
            % Path to original source file
            obj.sourcepath = pn;
            
            % Source file time stamp.
            obj.sourcetimestamp = d.date;
            
            % Import time stamp.
            obj.importtimestamp = datestr(now);
            
            % Project with which data is associated.
            obj.project = setproj;
            
        end % constructor.
        
        function display(obj)
            %DISPLAY  Full display of object.
            
            str = char(obj);
            disp(str);
            
        end % display
            
        function disp(obj)
            %DISP  Full display of object.
            %
            
            display(obj);
        
        end % disp
        
        function str = char(obj)
            %CHAR  Convert to a character array.
            %
            
            error(nargchk(1,1,nargin));

            CR  = char(10);
            str = '';
            str = [str 'Project    : ' obj.project CR];
            str = [str 'Source path: ' obj.sourcepath CR];
            str = [str 'Source file: ' obj.sourcefile CR];
            str = [str 'Source time: ' obj.sourcetimestamp CR];
            str = [str 'Imported   : ' obj.importtimestamp CR];
            str = [str 'Notes:' CR];
            str = [str obj.notes CR];
            
        end % char
        
        function dispshort(obj)
            %DISPSHORT  Shortened display of object.
            %
            
            % Individual project checks.
            if ~isempty(obj.project)
                disp(['Project     : ' obj.project]);
            end
            
            if ~isempty(obj.sourcefile)
                disp(['Source file : ' obj.sourcefile]);
            end
            
        end % dispshort
        
    end % methods - public
    
end % classdef



