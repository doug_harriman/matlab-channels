% moveave Moving sum of a signal.
% syntax: out = movesum(channel, points)
%	out = moving sum signal.
%	channel = input signal.
%	points  = number of points for sum.
%

function out = moveave(in,pts)

% Get data
data = get(in,'data')';
totpts = get(in,'points');

% Loop throug all data points
for i = 1:totpts,
   
   % Case i<pts
   if i==1,
      vec = [zeros(1,(pts-1)/2) data(1:(pts-1)/2+1)] ;
   elseif i+(pts-1)/2+1 > totpts,
   	% Update vector
	   vec = vec(2:length(vec));
   else,
   	% Update vector
	   vec = [vec(2:pts) data(i+(pts-1)/2+1)];
	end

   % Calculate the ave
   out(i)=sum(vec);
   
end   
   
out = channel([num2str(pts) ' point moving average of ' get(in,'name')],...
   out,get(in,'sampletime'),get(in,'units'),get(in,'test'));

   
   
   