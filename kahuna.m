%KAHUNA      Reads a Kahuna respsone file and writes channels
%            to the channel manager.
%
%>>dragonfly(queryFileName,testName)
%
% Both arguments are optional.  If 'queryFileName' is not
% specified, the user will be prompted for a name.
%

% DLH

function [] = dragonfly(fileName,testName);

% Open the dragonfly file
thisPath = cd;
if nargin == 0,
   thisPath = cd;
   
   searchPath = ['c:\'];
   disp(['Looking for Kahuna response file in ' searchPath]);
   cd(searchPath);
   
   % See if there is more than one file to chose from
   dirInfo = dir('*.rsp');
   
   if length(dirInfo) ~= 1,
      [fileName, filePath] = uigetfile('*.rsp','Kahuna Response File');
      cd(thisPath);
      if fileName == 0,
         disp('No file selected.');
         return;
      end
      
   else,
      disp('File found at default location');
      % Get file info
      fileName = dirInfo(1).name;
      filePath = [cd '\'];
   end
   
   % Return to the previous path
   cd(thisPath);
   
else,
   filePath = '';
end
fid = fopen([filePath fileName]);

% Attempt to read data
try
   % Look for recorder retrieve line
   readLine = fgetl(fid);
   if readLine == -1, error('End of file reached'); end

   ind = findstr(readLine,'udw.rec_retrieve');
   while isempty(ind),
      readLine = fgetl(fid);
      if readLine == -1, error('End of file reached'); end
      ind = findstr(readLine,'udw.rec_retrieve');
   end
   
   % See what type of servo we have 
   ind = findstr(readLine,'paper');
   servoType = 'PAPER';
   if isempty(ind)
      ind = findstr(lower(readLine),'carriage');
      servoType = 'CARRIAGE';
   end
   if isempty(ind)
      ind = findstr(readLine,'dc_service');
      servoType = 'SERVICE';
   end
   
   % Make sure we found a valid servo type designator
   if isempty(ind),
      error('No valid servo type identifier found.');
   end
   
   % Parse date and time info
   dateLine  = readLine;
   test.date = [dateLine(6:10) '/' dateLine(1:4)];
   test.time = dateLine(12:19);
   test.time = test.time(1:5);
   hour = str2num(test.time(1:2));
   if (hour > 12),
      hour = hour - 12;
      test.time = [num2str(hour) test.time(3:length(test.time))];
      test.time = [test.time ' PM'];
   else
      test.time = [test.time ' AM'];
   end
   test.type = 'Flight Recorder';
   test.name = fileName;
   if nargin<2,
      test = edtest(test);
   else,
      test.name = testName;
   end
   
   % Look for sample size line
   readLine = fgetl(fid);
   if readLine == -1, error('End of file reached'); end
   ind = findstr(readLine,'sample_size');
   while isempty(ind),
      readLine = fgetl(fid);
      if readLine == -1, error('End of file reached'); end
      ind = findstr(readLine,'sample_size');
   end
   
   % Parse size info
   readLine = readLine(find(abs(readLine)>47 & abs(readLine)<58));
   sampleSize = str2num(readLine);
   
   % Burn the 'tag value' line, not using TIC any more
   fgetl(fid);
   
   % Start reading data
   disp('Reading Data...');drawnow
   data = zeros(sampleSize,3);  % no tic value
   for i = 1:sampleSize,
      % Read line
      readLine = fgetl(fid);
      if readLine == -1, error('End of file reached'); end
   
      % Parse it
      ind = findstr(readLine,']');
      readLine = readLine(ind+1:length(readLine));
      readData = sscanf(readLine,'%f');
      
      % Store data
      data(i,:) = readData';
      
   end
   disp('Calculating values...');drawnow
   
   
   % Extract time
   if (data(1,2) ~= data(1,3))
      % Have TIC data
      tic  = [data(:,1) data(:,2)];
      haveTic = 1;
   else
      % Don't have TIC data so will assume a sample rate
      haveTic = 0;
   end
   
   % Extract data
   data = [data(:,1) data(:,3)];
   
   % Close file
   fclose(fid);

catch
   
   % Close file
   fclose(fid);
   disp('Error reading query file.');
   return;
   
end


% Set up the database for the type of recorder we have 
if strcmp(servoType,'CARRIAGE'),
   % Data values from "carriage_recorder.h", 01/06/99
   % POSITION            1
   % VELOCITY            2
   % INCREMENT           4
   % POSITION_CMD        8
   % POSITION_ERR       16
   % POSITION_ERR_DERIV 32
   % PWM                64
   sampleRate=inv(300);
   dataBase{1} = {'Actual Position','cEU'};
   dataBase{2} = {'Actual Velocity','cEU/int300'};
   dataBase{3} = {'Commanded Velocity','cEU/int300'};
   dataBase{4} = {'Commanded Position','cEU'};
   dataBase{5} = {'Position Error','cEU'};
   dataBase{6} = {'Velocity Error','cEU/int300'};
   dataBase{7} = {'PWM Value',''};
elseif strcmp(servoType,'PAPER'),
   % Data values from "paper_recorder.h", 01/26/99
   % Not full record, doesn't include any sneak info.
   % POSITION            1
   % VELOCITY            2
   % INCREMENT           4
   % POSITION_CMD        8
   % POSITION_ERR       16
   % POSITION_ERR_DERIV 32
   % PWM                64
   sampleRate=inv(600);
   dataBase{1} = {'Actual Position','pEU'};
   dataBase{2} = {'Actual Velocity','pEU/int600'};
   dataBase{3} = {'Commanded Velocity','pEU/int600'};
   dataBase{4} = {'Commanded Position','pEU'};
   dataBase{5} = {'Position Error','pEU'};
   dataBase{6} = {'Velocity Error','pEU/int600'};
   dataBase{7} = {'PWM Value',''};
elseif strcmp(servoType,'SERVICE'),
   % Added 10/05/99, DLH
   % POSITION            1
   % VELOCITY            2
   % INCREMENT           4
   % POSITION_CMD        8
   % POSITION_ERR       16
   % POSITION_ERR_DERIV 32
   % PWM                64
   % STATE					128
   sampleRate=inv(168.4);
   dataBase{1} = {'Motor Actual Position','sEU'};
   dataBase{2} = {'Motor Actual Velocity','sEU/sInt'};
   dataBase{3} = {'Commanded Motor Velocity','sEU/sInt'};
   dataBase{4} = {'Commanded Motor Position','sEU'};
   dataBase{5} = {'Motor Position Error','sEU'};
   dataBase{6} = {'Motor Velocity Error','sEU/sInt'};
   dataBase{7} = {'PWM Value',''};
   dataBase{8} = {'State',''};
else,
   error('Unknown servo type.');
end
   
% Check for unknown signals
knownTag = 2.^[0:length(dataBase)-1] ;
recvdTag = unique(data(:,1))';
if ~isempty(setdiff(recvdTag,knownTag)),
   warning('Dragonfly data set contains unknown signals which will be ignored.');
end

% Parse the data for individual channels
chanman
for i = 1:length(dataBase),
   
   % Look for the TAG
   ind  = find(data(:,1)==2^(i-1));
   
   if ~isempty(ind),
      % Found the tag, make the channel
%      if (i==7) | (i==2),  % Handle PWM data
         temp = data(ind,2);
         ind2  = find(temp>2^31);
         temp(ind2) = temp(ind2)-2^32;
         data(ind,2) = temp;
%      end
      % UI
      disp(['Channel found: ' dataBase{i}{1}]);drawnow
      
      % Create the channel
      chan = channel(dataBase{i}{1},data(ind,2),sampleRate,dataBase{i}{2},test);
      
      if (i==2),  % Handle Velocity data
         chan = -chan;
         chan = convert(chan,'IPS');
      end
      
      chanman('AddChannel',chan);
   end
   
end

% See if we should add any channels
if strcmp(servoType,'CARRIAGE'),
   auxchan;
elseif strcmp(servoType,'PAPER'),
   auxchan;
elseif strcmp(servoType,'SERVICE'),
   ss_auxchan;
end

% Delete the file
disp('Done');drawnow
%delete([filePath fileName]);
cd(thisPath);