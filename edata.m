%EDATA  Graphically select region of plot and send data to Excel.
%

% DLH
% 06/09/04 - Created.  Started from gdata.m

function [] = edata

error('Not currently working.');

% Get region of current plot
ind = extract;

lineHan = get(gca,'Children');
lineHan = findobj(lineHan,'Type','line');

% Look through all plotted lines.
nLines = length(lineHan);
for i = 1:nLines,
    data{i}.xData = get(lineHan(i),'xData');
    data{i}.yData = get(lineHan(i),'yData');
    
    data{i}.xData = data{i}.xData(ind)';
    data{i}.yData = data{i}.yData(ind)';
    
    if isappdata(lineHan(i),'ChannelName'),
        data{i}.name = [getappdata(lineHan(i),'ChannelName') ' [',...
                getappdata(lineHan(i),'ChannelUnits') ']'];
    else
        data{i}.name = ['Y' num2str(i)];
    end
end

% Assume all lines have same X data.  This is probably wrong often,
% but for now we'll do the easy thing.

% Pick a file name
% First try plot title, then figure title, then figure number.
str = get(get(gca,'Title'),'String');
if isempty(str),
    str = get(gcf,'Name');
end
if isempty(str),
    str = ['Figure ' num2str(gcf) ' Data'];
end
fileName = [str '.xls'];
disp(['Writing Excel file: ' fileName]);

% Write data to Excel
% Time vector
exl_setmat({'Time [sec]'},fileName,'R1C1');
exl_setmat(data{1}.xData,fileName,'R2C1');

col = 2;
row = 1;
for i = 1:nLines,
    disp(['Writing data set ' num2str(i) ' of ' num2str(nLines)]);
    
    % Header
    RC = ['R' num2str(row) 'C' num2str(col)];
    exl_setmat({data{i}.name},fileName,RC);
    
    % Data
    RC = ['R' num2str(row+1) 'C' num2str(col)];
    exl_setmat(data{i}.yData,fileName,RC);

    col = col + 1;
end
disp('Done');