%SPECTAVE  Spectral averaged FFT of a channel.
%
% >>[FFT_Data] = spectave(channel, FFT_size, overlap)
%
% Default arguments:
%   overlap := 0.75  75% overlap is optimal for Hanning windows.
%   FFT_size:= Picked for 4 data sets to averege.
%

% DLH, 11/05/98

function [mag,ph] = spectave(in, fftSize, overlap)
%function [fft_data] = spectave(in, fftSize, overlap)

% Basic data
dataSize = length(in);
name     = get(in,'Name');

% Default arguments
if nargin < 3, overlap = 0.75; end
if nargin < 2, fftSize = fix(dataSize/4); end

% Error check arguments
if fftSize > dataSize,
   error('FFT Size must be less than number of data points.');
end
if (overlap < 0) | (overlap > 1),
   error('Overlap must be on range [0,1].');
end

% Pad out data with zeros so that we get an integer number
% of FFT's to average together.
if (fftSize ~= dataSize),
   in = [in zeros(fftSize-mod(dataSize,fftSize),1)];
end
dataSize = length(in);

% Loop through data doing PSD's
delta = fftSize*(1-overlap);
loops = (dataSize-fftSize)/delta;
mag = 0;
ph  = 0;
%fft_data = 0;
for i = 1:1,%loops,
   % Get window of data
   temp = hanning(in((i-1)*delta+1:(i-1)*delta+fftSize));
%   fft_data = fft_data + psd(temp);

   % Calculate the magnitude an phase of this window
   fft_data = fft(temp);
   this_mag = abs(fft_data);
   this_ph  = angle(fft_data);
   
   % Calculate phase offset on first loop.
   if (i == 1),
      % Build phase offset vector
      f  = sampvec(fft_data);
      dt = get(in,'SampleTime')*delta;
      dPhi = channel('Phase Shift',rem(2*pi*dt*f,2*pi),...
         abs(get(fft_data,'SampleTime'))*j,'rad');
   else,
      % Phase offset the data
      this_ph = this_ph + dPhi*(i-1);
   end
     
   % Sum them in 
   mag = mag + this_mag;
   ph  = ph  + this_ph ;
   
end

% Average the signals
mag = mag/loops;
ph  = ph /loops;
%fft_data = fft_data/loops;

% Build output signals
set(mag,'Name',['Spectrally Averaged Magnitude of ' name]);
set(ph ,'Name',['Spectrally Averaged Phase of ' name]);
%set(fft_data,'Name',['Spectrally Averaged FFT of ' name]);