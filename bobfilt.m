% bobfilt.m
% Implements Bob C's wonderfilter.
%

function out = bobfilt(in,coeff)

% Filter coefficients
if nargin < 2,
   coeff = [-1 -5 -5 20 70 98 70 20 -5 -5 -1]/256 ;
end

% Convolve them
out = conv(in,coeff);
l = (length(coeff)-1)/2;
out = out(l:length(out)-l);


if 0
% Make sure we have a row vector
if size(in,1)>size(in,2),
  in = in';
  flip = 1 ;
else
  flip = 0 ;
end

% Apply filter window
offset = (length(coeff)-1)/2;
for i = 1+offset:length(in)-offset,
  out(i-offset) = sum(in(i-offset:i+offset).*coeff) ;
end

% Convolve the filter and data
%out = conv(coeff,in);

% Reflip data if necessary
if flip,
  out = out';
end

end
