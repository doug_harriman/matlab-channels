%FLEXTOOL_MI  Reads Flextool tracestore file and writes channels
%          to the channel manager.
%

% TODO - Need to get units from the mech config data so that we can
% generate consistent units for comparing multiple axes.  Should be able to
% generate a configuration file that has SVN repo version, plus config data
% for all axes in all mechs.

% DLH
% 11/13/02 - Error handling for files with no data.
% 10/18/02 - Got tic fixing correct.
% 05/13/02 - Created

function [chanmetadata] = flextool_mi(fileName,uiHandle)

% Loop through all channels, separating out individual servos.

% Digital sensor names
sensor_list = dict;
sensor_list('servo testbed') = {'TOF OOPS',...
    'Singulation Main',...
    'Lift Tray Up Main',...
    'Lift Tray OOPS Main',...
    '5',...
    '6',...
    '7',...
    '8'};

sensor_list('blaze') = {'TOF OOPS'};

sensor_list('wildfire') = {'TOF OOPS'};

sensor_list('ampere') = {'TOF OOPS',...
    'PZ Exit Jam',...
    'Multi Pick',...
    'Multi Pick Lower Tray',...
    'D-Roller',...
    'D-Roller Lower Tray',...
    'Opt Diff'};

axis_list = dict;
axis_list('ampere') = {'SENSOR',...
    'PAPER',...
    'CARRIAGE',...
    'PICK'};

axis_list('pbss_tdv') = {'SENSOR',...
    'LIFT',...
    'CARRIAGE',...
    'WIPE'};
    
axis_list('pentane') = {'SENSOR',...
    'LIFT',...
    'SENS_CARRIAGE',...
    'DUPLEX',...
    'PICK',...
    'SS_SLED',...
    'SCANNER',...
    'FEED'};

axis_list('blaze') = {'SENSOR',...
    'DRYER_HEATER',...
    'DRYER_FAN',...
    'SERVICE_LIFT',...
    'SERVICE_SLIDE',...
    'SERVICE_WEB',...
    'SPITTOON',...
    'CARRIAGE',...
    'PAPER',...
    'ISS_PUMP'};

axis_list('wildfire') = {'SENSOR',...
    'SERVICE_LIFT',...
    'SERVICE_SLIDE',...
    'SERVICE_WEB',...
    'CARRIAGE',...
    'PAPER',...
    'AERO_FAN',...
    'EJECT_ROLLER',...
    'SPITOON',...
    'ISS_PUMP',...
    'ISS_PUMP_PWM',...
    'ISS_SOLENOID'};

proj = lower(setproj);
if isempty(proj)
    % GUI selection
    sel = listdlg('ListString',axis_list.keys,...
        'SelectionMode','Single',...
        'Name','SETPROJ GUI',...
        'PromptString','Select Project');
    if isempty(sel)
        error('No project set.  Use SETPROJ');
    end
    
    % Set the project
    keys = axis_list.keys;
    setproj(keys{sel});
    proj = setproj;
    
end
if ~axis_list.iskey(lower(proj))
    error(['No axis list definition for project: ' upper(proj)]);
end
axis_list = axis_list(proj);
sensor_list = sensor_list(proj);

% Hack for blaze
% axis_list = {};
% axis_list{13}='PAPER';

% warning('Using hard coded sensor list for SERVO TESTBED');
% sensor_list = sensor_list('Servo Testbed');

% Clean up axis names
axis_list = cellfun(@(x)strrep(x,'_',' '),axis_list,...
    'UniformOutput',false);

% Load with normal multiple axis flextool.
if nargin > 0
    flextool_multiaxis(fileName);
else
    flextool_multiaxis;
end

chan_list = chanman('GetChannelAll');
chanman('ClearAll');

% Handle sensors.
% fcn = @(x)strcmp(get(x,'Name'),'MI_Servo sensorName');
fcn = @(x)strcmp(get(x,'Name'),'MI_Servo sensor');
tf  = cellfun(fcn,chan_list);
if any(tf)
    % Have sensor data
    name = chan_list{tf};
    chan_list(tf) = [];
    
    % Sensor state
    fcn = @(x)strcmp(get(x,'Name'),'MI_Servo sensorState');
    tf  = cellfun(fcn,chan_list);
    state = chan_list{tf};
    chan_list(tf) = [];
    
    % Parse into individual sensor traces.
    id_sense = unique(double(name));
    n_sense  = length(id_sense);
    
    for i = 1:n_sense
        % Extract each data value
        id = id_sense(i);
        ch = state(name == id);
        ch = set(ch,'Name',['Sensor ' sensor_list{id+1}]);
        chanman('AddChannel',ch);
    end
    
    % Sensor state raw
    fcn = @(x)strcmp(get(x,'Name'),'MI_Servo sensorStateRaw');
    tf  = cellfun(fcn,chan_list);
    if any(tf)
        state = chan_list{tf};
        chan_list(tf) = [];
        
        for i = 1:n_sense
            % Extract each data value
            id = id_sense(i);
            ch = state(name == id);
            ch = set(ch,'Name',['Sensor ' sensor_list{id+1} ' Raw']);
            chanman('AddChannel',ch);
        end
    end
    
    % Sensor debounce counter
    fcn = @(x)strcmp(get(x,'Name'),'MI_Servo debounceTimeCnt');
    tf  = cellfun(fcn,chan_list);
    if any(tf)
        state = chan_list{tf};
        chan_list(tf) = [];
        
        for i = 1:n_sense
            % Extract each data value
            id = id_sense(i);
            ch = state(name == id);
            ch = set(ch,'Name',['Sensor ' sensor_list{id+1} ' Debounce Cnt']);
            chanman('AddChannel',ch);
        end
    end
    
end


% Look for 1 multi axis servoAxis channel
fcn = @(x)strcmp(get(x,'Name'),'MI_Servo servoAxis');
tf  = cellfun(fcn,chan_list);
tf  = find(tf);
del_idx = [];
idx = [];
for i = 1:length(tf)
    chan = chan_list{tf(i)};
    data = double(chan);
    
    if length(unique(data)) == 1
        del_idx = tf(i);
        if isempty(idx)
            idx = data;  % In case only one
        end
    else
        idx = data;
    end
    
    % Delete this channel
    del_idx(end+1) = tf(i);
end
chan_list(del_idx) = [];

% Separate out any multi-axis channels
axes_list_idx = unique(idx);
axes_cnt = length(axes_list_idx);
n_chan = length(chan_list);
del_idx = [];
for i = 1:n_chan
    chan = chan_list{i};
    if length(chan) == length(idx)
        for j = 1:axes_cnt
            name     = get(chan,'Name');
            chan_new = chan(idx == axes_list_idx(j));
            name     = [axis_list{axes_list_idx(j)+1} ' ' name];
            chan_new = set(chan_new,'Name',name);
            chan_list{end+1} = chan_new;
            del_idx(end+1) = i;
        end % j
    end % if
end % chan_list
chan_list(del_idx) = [];

for i = 1:length(chan_list)
    chan = chan_list{i};
    name = get(chan,'Name');
    
    % Update channel metadata
    meta = get(chan,'ChanMeta');
    meta.project = proj;
    chan = set(chan,'ChanMeta',meta);
    
    % Skip SERVO_SENSOR as a 'normal' servo.
    if strncmp(name,'SENSOR',length('SENSOR'))
        continue;
    end
    
    name = strrep(name,'MI_Servo ','');
    chan = set(chan,'Name',name);
    
    % Set any command signals plot color
    if ~isempty(strfind(name,'pwm'))
        chan = set(chan,'PlotColor','k');
    end
    if ~isempty(strfind(name,'Cmd'))
        chan = set(chan,'PlotColor','r');
    end
    if ~isempty(strfind(name,'Cap'))  % PWM caps
        chan = set(chan,'PlotColor','r');
        
        % Add in the negative side cap
        c2 = chan;
        c2 = set(c2,'Name',['Negative ' name]);
        c2 = set(c2,'Data',-get(c2,'Data'));
        chanman('AddChannel',c2);
    end
    
    if ~isempty(strfind(name,'Exp'))  % Expected velocity
        chan = set(chan,'PlotColor','r');
    end
    
    if ~isempty(strfind(name,'target'))
        chan = set(chan,'PlotColor','r');
    end
    
    chanman('AddChannel',chan);
    
    % Add velocities
    if ~isempty(strfind(name,'position')) && (length(chan) > 1)
        vel = diff(chan);
        name = strrep(name,'position','velocity');
        plotcolor = 'b';
        if ~isempty(strfind(name,'Cmd'))
            name = [name ' cmd']; %#ok<AGROW>
            plotcolor = 'r';
        end
        
        set(vel,'Name',name,'PlotColor',plotcolor);
%         chanman('AddChannel',vel);
        
        % Basic velocity [eu/int]
        t = get(chan,'Time');
        x = double(chan);
        v = diff([x(1); x]);
        
        % Handle position sets.
        v(abs(v)>1e4) = 0;
        
        vel = channel([name ' - eu/int'],v,t);
        set(vel,'PlotColor',plotcolor);
        chanman('AddChannel',vel);
    end
    
    % Create expected velocity
    if ~isempty(strfind(name,'posExp')) && (length(chan) > 1)
        name = strrep(name,'posExp','velExp');
        plotcolor = 'r';
        
        % Basic velocity [eu/int]
        t = get(chan,'Time');
        x = double(chan);
        v = diff([x(1); x]);
        vel = channel([name ' - eu/int'],v,t);
        set(vel,'PlotColor',plotcolor);
        chanman('AddChannel',vel);
    end
    
    % Motor output torque
    if ~isempty(strfind(name,'torque')) && (length(chan) > 1)
        chan = set(chan,'Units','oz*mil');
        chan = convert(chan,'oz*in');
        
        % Color code torque limit.
        if ~isempty(strfind(name,'Limit'))
            chan = set(chan,'PlotColor','r');
        end
            
        chanman('AddChannel',chan);
    end
end

% Temperature sensor recorder
name = 'HeaterInfo tempAmbient';
ch = chanman('GetChannelByName',name);
if isempty(ch)
    name = 'Dryer Heater HeaterInfo tempAmbient';
    ch = chanman('GetChannelByName',name);
end
if ~isempty(ch)
    ch = ch/8;
    ch = set(ch,'Name','Temp. Ambient');
    ch = set(ch,'PlotColor','g');
    chanman('DeleteChannelByName',name);
    chanman('AddChannel',ch);
end

name = 'HeaterInfo tempCoilZone';
ch = chanman('GetChannelByName',name);
if isempty(ch)
    name = 'Dryer Heater HeaterInfo tempCoilZone';
    ch = chanman('GetChannelByName',name);
end
if ~isempty(ch)
    ch = ch/8;
    ch = set(ch,'Name','Temp. Coil Zone');
    ch = set(ch,'PlotColor','r');
    chanman('DeleteChannelByName',name);
    chanman('AddChannel',ch);
end

name = 'HeaterInfo tempDryZone';
ch = chanman('GetChannelByName',name);
if isempty(ch)
    name = 'Dryer Heater HeaterInfo tempDryZone';
    ch = chanman('GetChannelByName',name);
end
if ~isempty(ch)
    ch = ch/8;
    ch = set(ch,'Name','Temp. Dry Zone');
    chanman('DeleteChannelByName',name);
    chanman('AddChannel',ch);
end

name = 'HeaterInfo humidAmbient';
ch = chanman('GetChannelByName',name);
if isempty(ch)
    name = 'Dryer Heater HeaterInfo humidAmbient';
    ch = chanman('GetChannelByName',name);
end
if ~isempty(ch)
    ch = set(ch,'Name','Humidity Ambient');
    chanman('DeleteChannelByName',name);
    chanman('AddChannel',ch);
end

name = 'Dryer Heater servoAxis';
ch = chanman('GetChannelByName',name);
if ~isempty(ch)
    chanman('DeleteChannelByName',name);
end

name = 'this moveData pwm';
ch = chanman('GetChannelByName',name);
if ~isempty(ch)
    ch = set(ch,'Name','Heater PWM');
    ch = set(ch,'PlotColor','k');
    chanman('DeleteChannelByName',name);
    chanman('AddChannel',ch);
end


% Heater ISR recorder
ch = chanman('GetChannelByName','Dryer Heater position');
if ~isempty(ch)
    ch = ch/8;
    ch = set(ch,'Name','Dryer Heater Temperature Dry Zone');
    %     chanman('DeleteChannelByName','Dryer Heater position');
    chanman('AddChannel',ch);
end

ch = chanman('GetChannelByName','Dryer Heater positionCmd');
if ~isempty(ch)
    ch = ch/8;
    ch = set(ch,'Name','Dryer Heater Temperature Coil Zone Command');
    ch = set(ch,'PlotColor','r');
    %     chanman('DeleteChannelByName','Dryer Heater positionCmd');
    chanman('AddChannel',ch);
end

ch = chanman('GetChannelByName','Dryer Fan position');
if ~isempty(ch)
    ch = ch/2;
    ch = set(ch,'Units','rev');
    ch = set(ch,'Name','Dryer Fan position');
    chanman('DeleteChannelByName','Dryer Fan position');
    chanman('AddChannel',ch);
    ch = diff(ch);
    ch = convert(ch,'RPM');
    ch = set(ch,'Name','Dryer Fan Speed');
    chanman('AddChannel',ch);
end

ch = chanman('GetChannelByName','Dryer Fan positionCmd');
if ~isempty(ch)
    ch = ch/2;
    ch = set(ch,'Units','rev');
    
    ch = set(ch,'PlotColor','r');
    ch = set(ch,'Name','Dryer Fan positionCmd');
    chanman('DeleteChannelByName','Dryer Fan positionCmd');
    chanman('AddChannel',ch);
    
    ch = diff(ch);
    ch = convert(ch,'RPM');
    ch = set(ch,'Name','Dryer Fan Speed Command');
    chanman('AddChannel',ch);
end


% If no output, don't pass chanmetadata
chanman('SortChannels');
if nargout == 0,
    clear chanmetadata
end


% End main function


% User Interface messages
function [] = UIMessage(handle,msg)

if handle,
    if handle == -1,
        chanman('SetMessage',msg);
    else
        set(handle,'string',msg);
    end
else
    disp(msg);
    drawnow
end