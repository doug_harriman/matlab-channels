%CHANMAN_PLUGINS  Channel Manager Plugin manager.
%
%                 Plugins may be added to the channel manager automatically
%                 by placing in the 'plugin' directory.  Plugins must respond
%                 with a string label for the plugin menu when called with only
%                 the string 'GetLabel'.  Plugins should to their work when called
%                 with the string 'Run'.
%

% DLH
% 06/14/01 - Created

% This function returns an array:
% pluginDef(1).Label
% pluginDef(1).Callback
%      .
% pluginDef(N).Label
% pluginDef(N).Callback

function pluginDef = chanman_plugins()
pluginDef = [];

% Build path for plugin directory
chanmanPath = fileparts(which('chanman'));
pluginPath  = [chanmanPath filesep 'plugins'];

% Add this path.  Note: repeated additions don't matter
addpath(pluginPath);

% Search for plugins in the plugin directory
searchList = what(pluginPath);
searchList = searchList.m;

index = 1;
for i = 1:length(searchList),
    % Check for valid plugin
    [tmp, fileName] = fileparts(searchList{i});
    try
        label = feval(fileName,'GetLabel');
    catch   
        label = [];
    end
    
    % Add to list if we found a valid plugin
    if ~isempty(label),
        
        % Support single level submenu
        labelList = words(label,'|');
        if length(labelList) > 2,
            error('Submenus can only go 1 level deep.');
        elseif length(labelList) == 1,
            label = labelList{1};
            submenu = '';
        elseif length(labelList) == 2,
            label = labelList{2};
            submenu = labelList{1};
        end
        
        pluginDef(index).Label         = label;
        pluginDef(index).Callback      = [fileName '(''Run'');'];
        pluginDef(index).SubMenuLabel  = submenu;
        pluginDef(index).SubMenuName   = strrep(submenu,' ','');
        index = index + 1;
    end
    
end
