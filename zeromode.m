%ZEROMODE  Makes the mode of a channel zero.
%

% DLH  
% 07/22/02 - Modified from zeromean.m

function out = zeromode(in)

% Chache name, do the mean thing, then reset name
name = get(in,'name');
un   = get(in,'units');
data = double(get(in,'data'));
data = data - mode(data);
set(in,'data',data)  ;

if nargout==0
	% Change value in caller's workspace
   assignin('caller',inputname(1),in) ;
else
   out = in;
end