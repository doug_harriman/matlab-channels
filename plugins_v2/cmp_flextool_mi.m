%FLEXTOOL_MI_IMPORT  Channel manager Flextool Multi-Instance import plugin.
%

classdef cmp_flextool_mi < chanmanplugin
    methods
        function obj = cmp_flextool_mi(varargin)
            obj.type  = 'parser';
            obj.label = 'Flextool MI Tracestore';
            obj.fcn   = @flextool_mi;
            obj.args  = {0,-1};
        end
    end
end % classdef
