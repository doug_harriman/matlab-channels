%FLEXTOOL_IMPORT  Channel manager Flextool import plugin.
%

function [out] = flextool_import(msg,varargin)

switch(lower(msg))
case 'getlabel'
    out = 'Flextool Single Axis Tracestore';
case  'run'
   flextool(0,-1);
otherwise
    error(['Unknown message: ' msg])
end