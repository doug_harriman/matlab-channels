%FLEXTOOL_MULTIAXIS_IMPORT  Channel manager Flextool Overlap import plugin.
%

function [out] = flextool_multiaxis_import(msg,varargin)

switch(lower(msg))
case 'getlabel'
    out = 'Flextool Multiple Axis Tracestore';
case  'run'
   flextool_multiaxis(0,-1);
otherwise
    error(['Unknown message: ' msg])
end