%PDAQ_IMPORT  Channel manager pDAQ text file import plugin.
%

function [out] = pdaq_import(msg,varargin)

switch(lower(msg))
case 'getlabel'
    out = 'pDAQ Text File';
case  'run'
   pdaq;
otherwise
    error(['Unknown message: ' msg])
end