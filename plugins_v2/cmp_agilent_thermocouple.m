%THERMOCOUPLEDATA  Channel Manager Plug-in to read Agilent
%                  Benchlink data logger thermocouple output.
%

% DLH
% Changelog
% 01/02/03 - Changed CSV input format to have 1 column of time data.  Requires user cleanup.
% 01/03/03 - Set default plot color to red.

function [out] = thermocoupledata(msg,varargin)

switch(lower(msg))
case 'getlabel'
    out = 'Agilent Thermocouple Data';
    return;
case  'run'
    % Passthrough
otherwise
    error(['Unknown message: ' msg])
end



% Find the data file
fileName = GetFileName;
if ~max(fileName), return; end

% Open the file
fid = fopen(fileName);
if fid < 0,
    UIMessage('Unable to open thermocouple data file.');
    return
end

% Read header line
header = fgetl(fid);

% Read rest of data as a string
dataStr = fscanf(fid,'%c');

% Close file
fclose(fid);

% Convert string to numerical
dataVec = strread(dataStr,'%f','delimiter',',');

% Vector -> matrix
commaInd = findstr(',',header);
numCols = length(commaInd) + 1;
numRows = length(dataVec)/numCols;
dataMat = reshape(dataVec,numCols,numRows)';
dataVec = [];

% % Convert to channel data (time by every col)
% numChans = numCols/2;
% commaInd = [commaInd length(header)+1];
% channelIndex = 1;
% for i = 1:numChans,
%     % Try to eliminate bogus channels
%     % Max temp of 200C
%     if max(dataMat(:,i*2)) < 200,
%         % Create channel        
%         tempChan = channel(header(commaInd(i*2-1)+1:commaInd(i*2)-1),dataMat(:,i*2),dataMat(:,i*2-1));
%         
%         % Set constant sample rate.
%         time = get(tempChan,'time');
%         dt   = round(mean(diff(time)));
%         tempChan = resample(tempChan,1/dt);
%         
%         % Store out
%         channels{channelIndex} = tempChan;
%         channelIndex = channelIndex + 1;
%     end
% end

% Convert to channel data (time by every col)
numChans = numCols - 1;
commaInd = [commaInd length(header)+1];
channelIndex = 1;
time = dataMat(:,1);
for i = 1:numChans,
    % Try to eliminate bogus channels
    % Max temp of 200C
    if max(dataMat(:,i+1)) < 200,
        % Create channel       
        name = ['Unit ' header(commaInd(i)+1:commaInd(i+1)-1)];
        tempChan = channel(name,dataMat(:,i+1),time);
        
        % Set constant sample rate.
        time = get(tempChan,'time');
        dt   = round(mean(diff(time)));
        tempChan = resample(tempChan,1/dt);
        
        % Set plot color
        set(tempChan,'PlotColor','r');
        
        % Store out
        channels{channelIndex} = tempChan;
        channelIndex = channelIndex + 1;
    end
end

% Prep channel mappings
numChans = length(channels);
for i = 1:numChans,
    chanman('AddChannel',channels{i});
end


% End of main function


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Local Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%---------------------------------------------------------------------
% User Interface messages
%---------------------------------------------------------------------
function [] = UIMessage(msg)
chanman('SetMessage',msg);


%---------------------------------------------------------------------
% Data File Name Identification
%---------------------------------------------------------------------
function [fullFileName] = GetFileName()

% Haven't opened the file yet.
fid = 0;

% Open the dragonfly file
thisPath = cd;

defaultPath = 'i:\';
cd(defaultPath);
csvFiles = dir('*.csv');
if length(csvFiles) == 0,
    cd(thisPath);
end

[fileName, filePath] = uigetfile('*.csv','CSV Data Files');
if fileName == 0,
    UIMessage('No file selected.');
    % Return to the previous path
    cd(thisPath);
end
    
% Return to the previous path
cd(thisPath);

fullFileName = [filePath fileName];