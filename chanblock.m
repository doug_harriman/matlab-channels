%CHANBLOCK  Easily manipulate all Channel Object blocks in a Simulink Model.
%
% Commands: 'NoOutput'
%           'Workspace'
%           'ChanMan'
%           'SampleRate', <int>
%           'TestName', <string>
%

% NOT WORKING YET

% DLH
% Version 1.0 - 8/5/98   Initial Version
% Version 2.0 - 04/22/99 Compatibe with TOCHAN version 2.0

function [] = chanblock(msg,varargin)

% List of fields:
vars = {'Name';'SampleRate';'Units';'TestName';'ChanMan';'WorkSpace'};

% Get the blocks
blocks = find_system(bdroot,'Description','ToChannel');

% Loop through all of the blocks
for i = 1:length(blocks),
   
   % Get the struct
   data = get_param(blocks{i},'userdata');
   
   % Switch on message
   switch lower(msg)
   
      case 'nooutput'
         % Change the values
         data.toChannelManager = 0;
         data.toWorkspace = 0;
         
      case 'workspace'
         % Change the values
         data.toWorkspace = 1;
      
      case 'chanman'
         % Change the values
         data.toChannelManager = 1;
         
      case 'samplerate'
         % Change the values
         data.SampleRate = (varargin{1});
      case 'testname'
         % Change the values
         data.testName = varargin{1};
   end
   
   % Write the data
   set_param(blocks{i},'userdata',data);
   
   % Set the block name
   tochan('setblockname',blocks{i});
   
end

