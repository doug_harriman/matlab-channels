%METADATA  Converts text data from IMPORTDATA to a structure.
%   MEATADATA pulls the TEXTDATA field from the structure returned from
%   IMPORTDATA.  It then searches the strings for property value pairs of
%   the form "PROPERTY=VALUE".  Each value must be on it's own line.  The
%   structure returned has fields with names for each of the properties
%   found.  All values will be strings.
%

% DLH 
% 04/21/04 - Created

function [out] = metadata(data)

% Error check inputs.
if ~isstruct(data), error('Expecting structure input.'); end
if ~isfield(data,'textdata'), error('Input should be structure returned from IMPORTDATA.'); end

% Extract text cell and loop through it.
data    = data.textdata;
numRows = size(data,1);
out = [];
for i = 1:numRows,
    txt = data{i,1};
    
    % Only counts for metadata if text contains a property/value pair
    % separated by an "=" sign.
    ind = findstr(txt,'=');
    if ~isempty(ind),
        prop = txt(1:ind-1);
        prop = lower(prop);
        val  = txt(ind+1:end);
        
        % Convert numeric fields to numbers
        val = strconv(val);
        
        % Assign the fields
        out = setfield(out,prop,val);
    end
end
   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Converts string to number if possible.
function num_or_str = strconv(str)

% Looks for digits
radix_str = '.';
digit_ind = isstrprop(str,'digit');

% If all chars are digits, have a number.
if all(digit_ind),
    num_or_str = str2num(str);
    return;
end

% If have only 1 non-digit, and is '.', then have a floating point number.
non_digit_ind = find(~digit_ind);
if length(non_digit_ind) == 1,
    if str(non_digit_ind) == radix_str,
        num_or_str = str2num(str);
        return;
    end
end

% If we got here, treat as a string.
num_or_str = str;

