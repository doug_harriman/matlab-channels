% buildlowpass  Builds a low pass, non-causal digital filter.
% Syntax: filter = buildlowpass(frequency_ratio, size)
%         filter          - LTI object containing the filter.
%         frequency_ratio - Ratio of cutoff frequency to sampling
%                           frequency.  Must be less than 0.5.
%         size            - Number of points filter calculated over.
%                           This number should be odd.
%

function a = buildlowpass(f,N)

% Error check
if f>0.5,
   txt = 'Frequency Ratio must be less than 0.5';
   error(txt);
end

% Create indecies
k = [-(N-1)/2:(N-1)/2]
%k = [1:N];

% Create Coefficients
a = 2./(pi*k).*sin(2*f*pi*k);

% Sigma factors
sigma = sin(pi*k/N)./(pi*k/N);

% Use sigma to smooth the filter coefficients
a = sigma.^2.*a;

% Replace NaN's with 1's
a(isnan(a)) = 1 ;

