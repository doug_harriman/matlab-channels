function varargout = setupCh(varargin)
global  triggerSet

if nargin == 0  % LAUNCH GUI
    triggerSet = 0;
	fig = openfig(mfilename,'reuse');
	set(fig,'Color',get(0,'defaultUicontrolBackgroundColor'));	% Use system color scheme for figure
    handles = guihandles(fig);          %create a structure of handles
    handles.numChans=0;                 %add additional data to the structure
	handles.config = [];
    handles.config.trig=0;
    guidata(fig, handles);              %save the structure
    set(handles.chTitle,'String','Channel #    Name    Units');
    set(handles.update,'String','');
    set(handles.sampleRate,'String','1200');
    set(handles.duration,'String','5');
    set(handles.listChannels,'String','<empty>'); 
    set([handles.deleteChannel,handles.editChannel,...
            handles.clearChannel,handles.copyChannel,handles.getData],'Enable','off');
	if nargout > 0
		varargout{1} = fig;
	end    
elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK
	try
		[varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
	catch
		disp(lasterr);
	end
end

% --------------------------------------------------------------------
function varargout = addChannel_Callback(h, handles)
global cancel changeData
    
    set(handles.update,'string',['']);
    changeData = 0;
    
    %Open the Add Channel GUI
    addCh;
    if cancel==1, return; end
    
    currentVal = handles.numChans+1;
    handles.numChans = currentVal;  
    guidata(h,handles);
    
    [h,handles] = displayCh(h,handles,currentVal);
    guidata(h,handles);

    set([handles.deleteChannel,handles.editChannel,...
            handles.clearChannel,handles.copyChannel,handles.getData],'Enable','on');
                 
% --------------------------------------------------------------------
function varargout = editChannel_Callback(h, handles)
global  changeData  cancel2  data

    set(handles.update,'string',['']);
    currentVal = get(handles.listChannels,'Value');
    data = handles.config(currentVal);
    changeData = 1;
    addCh;
    
    if cancel2==1, return; end
    [h,handles] = displayCh(h,handles,currentVal);
    guidata(h,handles);

% --------------------------------------------------------------------
function varargout = copyChannel_Callback(h, handles)
global  copy  ch2copy   cancel3

    set(handles.update,'string',['']);
    total = length(handles.config);
    currentVal = get(handles.listChannels,'Value');
    ch2copy = handles.config(currentVal(1));
    copyCh;
    
    if cancel3==1, return; end
    if isempty(copy), return; end
    
    num = str2num(copy.num);
    
    for i=1:num
        handles.config(total+i).Units      = handles.config(currentVal).Units;
        handles.config(total+i).SensorMin  = handles.config(currentVal).SensorMin;
        handles.config(total+i).SensorMax  = handles.config(currentVal).SensorMax;
        handles.config(total+i).UnitsMin   = handles.config(currentVal).UnitsMin;
        handles.config(total+i).UnitsMax   = handles.config(currentVal).UnitsMax;
        handles.config(total+i).trig = 0;
        handles.config(currentVal).trigType   = handles.config(currentVal).trigType;
        handles.config(currentVal).trigVolts  = handles.config(currentVal).trigVolts;
        if copy.increase==1,    
            a=int2str(str2num(handles.config(currentVal).Channels) + i);
            handles.config(total+i).Channels   = a;
            if str2num(handles.config(total+i).Channels) > 15,
                handles.config(total+i).Channels = '15';
                errordlg('Only 16 Channels Available','Error');
            end
            handles.config(total+i).Name       = strcat(handles.config(currentVal).Name,num2str(i));
        else
            handles.config(total+i).Channels   = handles.config(currentVal).Channels;
            handles.config(total+i).Name       = strcat(handles.config(currentVal).Name,num2str(i));
        end
    end
    
    handles.numChans = total+num;
    guidata(h,handles);
    listboxDisplay(h,handles);
    copy = [];
    
% --------------------------------------------------------------------
function varargout = deleteChannel_Callback(h, handles)
global triggerSet

    set(handles.update,'string',['']);
    currentVal = get(handles.listChannels,'Value');
    listStr = get(handles.listChannels,'String');
    numResults = size(listStr,1);
    
    %Determine if the channel was used to trigger on
    if handles.config(currentVal).trig == 1,
        triggerSet = 0;
    end
    
    %Remove the data and list entry for the selected value
    listStr(currentVal) =[];
    handles.config(currentVal)=[];
    
    %If there are no other entries, disable Remove button and change list sting to <empty>
    if isequal(numResults,length(currentVal)),
        listStr = {'<empty>'};
        currentVal = 1;
        set([handles.deleteChannel,handles.editChannel,...
                handles.clearChannel,handles.copyChannel,handles.getData],'Enable','off')
        set(handles.update,'String','');    
    end
    
    % Ensure that list box Value is valid, then reset Value and String
    currentVal = min(currentVal,size(listStr,1));
    set(handles.listChannels,'Value',currentVal,'String',listStr)
    
    % Store the new ResultsData
    handles.numChans = handles.numChans-1;
    guidata(h,handles)
    
% --------------------------------------------------------------------
function varargout = clearChannel_Callback(h, handles)
global triggerSet

    set(handles.update,'string',['']);
    currentVal = 1;   
    listStr = {'<empty>'};    
    set(handles.listChannels,'Value',currentVal,'String',listStr);
    set(handles.update,'String','');
    set([handles.deleteChannel,handles.editChannel,...
            handles.clearChannel,handles.copyChannel,handles.getData],'Enable','off')     
    handles.config = [];
    handles.numChans = 0;
    triggerSet = 0;
    guidata(h,handles);
    
% --------------------------------------------------------------------
function varargout = getData_Callback(h, handles)
    Fs = str2num(get(handles.sampleRate,'string'));
    maxRate =  20000/(length(handles.config));
    if Fs > maxRate,
        set(handles.sampleRate,'string',maxRate);
        Fs = maxRate;
        errordlg('Maximum Sampling Rate = 20kHz/(# of channels)','Error');
    end    
    duration = str2num(get(handles.duration,'string'));
    sample_data3(Fs,duration,handles);
    
% --------------------------------------------------------------------
function varargout = open_file_Callback(h, handles)
global triggerSet    

    oldData = [];
    handles.config = [];
    set(handles.listChannels,'Value',1,'String','');
    set(handles.update,'string',['']);
    uiopen('*.mat');
    if isempty(oldData), return; end
        
    for i=1:length(oldData.config),            
        handles.config(i).Channels   = oldData.config(i).Channels;
        handles.config(i).Name       = oldData.config(i).Name;
        handles.config(i).Units      = oldData.config(i).Units;
        handles.config(i).SensorMin  = oldData.config(i).SensorMin;
        handles.config(i).SensorMax  = oldData.config(i).SensorMax;
        handles.config(i).UnitsMin   = oldData.config(i).UnitsMin;
        handles.config(i).UnitsMax   = oldData.config(i).UnitsMax;
        handles.config(i).trig       = oldData.config(i).trig;
        handles.config(i).trigType   = oldData.config(i).trigType;
        handles.config(i).trigVolts  = oldData.config(i).trigVolts;
    end 
    handles.numChans = length(handles.config);
    set(handles.sampleRate,'string',oldData.rate);
    set(handles.duration,'string',oldData.total);
    guidata(h,handles);
   
    listboxDisplay(h,handles);
    
    set([handles.deleteChannel,handles.editChannel,...
            handles.clearChannel,handles.copyChannel,handles.getData],'Enable','on');
    
    for i=1:length(handles.config),
        if (handles.config(i).trig == 1), triggerSet = 1; end
    end
    
% --------------------------------------------------------------------
function varargout = save_file_Callback(oldData)
    oldData.total = get(oldData.duration,'string');
    oldData.rate = get(oldData.sampleRate,'string');
    uisave;

% --------------------------------------------------------------------
function varargout = listChannels_Callback(h, handles)
function varargout = rate_Callback(h, handles)
function varargout = time_Callback(h, handles)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GUI for adding or editing channel data
% --------------------------------------------------------------------
function varargout = addCh(varargin)
global changeData   data  triggerSet    trigYes

    if nargin==0
        fig2 = open('addCh.fig');
        handles2 = guihandles(fig2);
        
        trigger{1} = 'Above High Level';
        trigger{2} = 'Below Low Level';
        set(handles2.trigType,'string',trigger);
        set(handles2.trigVolts,'string',8);
        set(handles2.num,'string',[0:15]);
        set([handles2.trigNo],'Value',1.0);
        set([handles2.trigType,handles2.trigVolts],'Enable','off');
        if triggerSet == 1,
            set([handles2.trigYes,handles2.trigNo],'Enable','off');
        end
        
        trigYes = 0;
        guidata(fig2,handles2);
        
        if changeData == 1,
            val = 1;
            while str2num(data.Channels) ~= (val-1),
                val = val+1;
            end    
            set(handles2.num,'Value',val);
            set(handles2.Name,'String',data.Name);
            set(handles2.Units,'String',data.Units);
            set(handles2.sensorMin,'String',data.SensorMin);
            set(handles2.sensorMax,'String',data.SensorMax);
            set(handles2.unitsMin,'String',data.UnitsMin);
            set(handles2.unitsMax,'String',data.UnitsMax);        
            if data.trig==1,       
                set([handles2.trigYes,handles2.trigType,...
                        handles2.trigNo,handles2.trigVolts],'Enable','on');
                set(handles2.trigYes,'Value',1);
                set(handles2.trigVolts,'String',data.trigVolts);
                trigYes = 1;
                set(handles2.trigNo,'Value',0);
                if strcmp(data.trigType,'AboveHighLevel');
                    set(handles2.trigType,'Value',1);
                else
                    set(handles2.trigType,'Value',2);
                end
            end
            clear data;
            guidata(fig2,handles2);
        end
            
        uiwait(fig2);
        if nargout > 0
		    varargout{1} = fig2;
	    end
    elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK
	    try
		    [varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
	    catch
		    disp(lasterr);
    	end
    end

% --------------------------------------------------------------------
function varargout = configData_Callback(h2, handles2)   
global chanConfig  cancel2  cancel  triggerSet changeData   trigYes

    chVal = get(handles2.num,'Value');
    chanConfig.Channels = num2str(chVal - 1);
    
    chanConfig.Name = get(handles2.Name,'string');
    chanConfig.Units = get(handles2.Units,'string');    
    chanConfig.SensorMax = get(handles2.sensorMax,'string');    
    chanConfig.SensorMin = get(handles2.sensorMin,'string');
    chanConfig.UnitsMax = get(handles2.unitsMax,'string');
    chanConfig.UnitsMin = get(handles2.unitsMin,'string');
    
    chanConfig.trig = get(handles2.trigYes,'Value');
    if chanConfig.trig==1,
        triggerSet = 1;
    elseif trigYes==1,
        triggerSet = 0;
    end
    
    value = get(handles2.trigType,'value');
    if value==1,
        chanConfig.trigType = 'AboveHighLevel';
    else
        chanConfig.trigType = 'BelowLowLevel';
    end
    chanConfig.trigVolts = get(handles2.trigVolts,'string');
    
    guidata(h2,handles2);
    
    cancel = 0;
    cancel2 = 0;
    delete(handles2.figure2);
        
% --------------------------------------------------------------------
function varargout = cancel_Callback(h2, handles2)
global cancel2 cancel
    cancel = 1;
    cancel2 = 1;
    delete(handles2.figure2);

% --------------------------------------------------------------------
function varargout = trigYes_Callback(h2, handles2)               
    mutual_exclude(handles2.trigNo)
    set([handles2.trigType,handles2.trigVolts],'Enable','on');
    guidata(h2,handles2);    
        
% --------------------------------------------------------------------
function varargout = trigNo_Callback(h2, handles2)
    mutual_exclude(handles2.trigYes)
    set([handles2.trigType,handles2.trigVolts],'Enable','off');
    guidata(h2,handles2);
    
% --------------------------------------------------------------------   
function mutual_exclude(off)
    set(off,'Value',0);

% --------------------------------------------------------------------
function varargout = sensorMin_Callback(h2, handles2)
    sensorMin = get(handles2.sensorMin,'string');
    sensorMax = get(handles2.sensorMax,'string');
    if str2num(sensorMin) > str2num(sensorMax),
        set(handles2.sensorMin,'string',sensorMax);
        errordlg('Minimum voltage must be less than the maximum.','Error');
    end
    guidata(h2,handles2);
    
% --------------------------------------------------------------------
function varargout = sensorMax_Callback(h2, handles2)
    sensorMax = get(handles2.sensorMax,'string');
    if str2num(sensorMax) > 20,
        set(handles2.sensorMax,'string','20');
        errordlg('Maximum Voltage is 20V.','Error');
    end
    guidata(h2,handles2);

% --------------------------------------------------------------------
function varargout = trigVolts_Callback(h2, handles2)
    trigVolts = get(handles2.trigVolts,'string');
    if str2num(trigVolts) > 20,
        set(handles2.trigVolts,'string','20');
        errordlg('Maximum voltage is 20V.','Error');
    elseif ~isnum(trigVolts)
        set(handles2.trigVolts,'string','5');
        errordlg('Voltage must be numeric.  Set to default of 5V.','Error');
    end
    guidata(h2,handles2);

% --------------------------------------------------------------------
function varargout = unitsMin_Callback(h2, handles2)
    unitsMin = get(handles2.unitsMin,'string');
    unitsMax = get(handles2.unitsMax,'string');
    if str2num(unitsMin) > str2num(unitsMax),
        set(handles2.unitsMin,'string',unitsMax);
        errordlg('The minimum must be less than the maximum.','Error');
    end
    guidata(h2,handles2);

% --------------------------------------------------------------------
function varargout = unitsMax_Callback(h2, handles2)
function varargout = trigType_Callback(h2, handles2)
function varargout = num_Callback(h2, handles2)
function varargout = Name_Callback(h2, handles2)
function varargout = Units_Callback(h2, handles2)        

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GUI for copying Channel data
% --------------------------------------------------------------------
function varargout = copyCh(varargin)
global  ch2copy 

    if nargin==0
        fig3 = open('copyCh.fig');
        handles3 = guihandles(fig3);
        
        set(handles3.ch,'String',ch2copy.Name);
        guidata(fig3,handles3);
            
        uiwait(fig3);
        if nargout > 0
		    varargout{1} = fig3;
        end
    elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK
	    try
		    [varargout{1:nargout}] = feval(varargin{:}) % FEVAL switchyard   
        catch
		    disp(lasterr);
        end
    end
    
% --------------------------------------------------------------------
function varargout = cancelCopy_Callback(h, handles3)
global cancel3
    cancel3 = 1;
    delete(handles3.figure3);
    
% -------------------------------------------------------------------- 
% Callback for the OK button
function varargout = copyChan_Callback(h, handles3)
global copy     cancel3
    
    copy.increase = get(handles3.increase,'Value');
    copy.num = get(handles3.numCopy,'String');

    cancel3 = 0;
    delete(handles3.figure3);
    
% --------------------------------------------------------------------
function varargout = increase_Callback(h, handles3)
function varargout = numCopy_Callback(h, handles3)


%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
function [h,handles] = displayCh(h,handles,n)
global chanConfig

if isempty(chanConfig), return; end

%Add a new Channel
handles.config(n).Channels   = chanConfig.Channels;
handles.config(n).Name       = chanConfig.Name;
handles.config(n).Units      = chanConfig.Units;
handles.config(n).SensorMin  = chanConfig.SensorMin;
handles.config(n).SensorMax  = chanConfig.SensorMax;
handles.config(n).UnitsMin   = chanConfig.UnitsMin;
handles.config(n).UnitsMax   = chanConfig.UnitsMax;
handles.config(n).trig       = chanConfig.trig;
handles.config(n).trigType   = chanConfig.trigType;
handles.config(n).trigVolts  = chanConfig.trigVolts;                 
 
listboxDisplay(h,handles)

%---------------------------------------------------------------------------
function listboxDisplay(h,handles)
    
allCh = handles.numChans;
chLength = size(char(handles.config.Channels),2); 
for i=1:allCh,
    currentLength=length(char(handles.config(i).Channels));
    addBlanks = chLength-currentLength;
    if addBlanks > 0,
        ch{i} = [handles.config(i).Channels blanks(addBlanks+9)];
    else   
        ch{i} = [handles.config(i).Channels blanks(9)];
    end
end
    
nameLength = size(char(handles.config.Name),2);
if nameLength<4, nameLength==4; end
for i=1:allCh,
    currentLength=length(char(handles.config(i).Name));
    addBlanks = nameLength-currentLength;
    if addBlanks > 0,
        chName{i} = [handles.config(i).Name blanks(addBlanks+3)];
    else   
        chName{i} = [handles.config(i).Name blanks(3)];
    end
    chUnits{i} = ['[' handles.config(i).Units ']'];
    listData{i} = [ch{i}, chName{i}, chUnits{i}];
end
    
pad = blanks(nameLength-1);
padCh = blanks(chLength+2);
n = [padCh,'Name', pad];
listboxTitle = ['Channel',n,'Units'];
    
set(handles.chTitle,'String',listboxTitle);
set(handles.listChannels,'String',listData,'Value',1);