function []=sample_data3(Fs,duration,handles)
trig=0;

% Channel Configuration Parameters:
% {HW Channel} {Input Range} {Sensor Range} {Units} {UnitsRange} {Name}
for i=1:length(handles.config)
    Ch = str2num(handles.config(i).Channels);
    Name = handles.config(i).Name; 
    Units = handles.config(i).Units;
    inputRange = [str2num(handles.config(i).SensorMin),str2num(handles.config(i).SensorMax)];
    sensorRange = [str2num(handles.config(i).SensorMin),str2num(handles.config(i).SensorMax)];
    unitsRange = [str2num(handles.config(i).UnitsMin),str2num(handles.config(i).UnitsMax)];
    if handles.config(i).trig==1,
        trig = i;
    end
    config{i} = {Ch, inputRange, sensorRange, Units, unitsRange, Name};
end


if trig > 1,
    chanConfig{1} = config{trig};
    for i=1:(trig-1),
        chanConfig{i+1} = config{i};
    end
    for i=(trig+1):length(config)
        chanConfig{i} = config{i};
    end
else 
    chanConfig = config;
end
    
        

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
daqreset;
ai = analoginput('nidaq',1);

% Set up sampling rate and time
ActualRate = setverify(ai, 'SampleRate', Fs);
SampleSize = ActualRate*duration;
set(ai, 'SamplesPerTrigger', SampleSize);
set(ai,'InputType', 'SingleEnded');

% Set up event handler
set(ai,'StartFcn',{@actionfcn,handles});
set(ai,'StopFcn',{@actionfcn,handles});
set(ai,'TriggerFcn',{@actionfcn,handles});

% Setup trigger
if trig~=0,
    set(ai,'TriggerType','HwAnalogChannel');
    set(ai,'TriggerCondition',handles.config(trig).trigType);
    set(ai,'TriggerConditionValue',str2num(handles.config(trig).trigVolts)); 
else
    set(ai,'TriggerType','Immediate');
end

% Set up channels
for i = 1:length(handles.config),
   num = chanConfig{i}{1};
   addchannel(ai,num);
   set(ai.Channel(i),...
      'InputRange',chanConfig{i}{2},...
      'SensorRange',chanConfig{i}{3},...
      'Units',chanConfig{i}{4},...
      'UnitsRange',chanConfig{i}{5},...
      'ChannelName',chanConfig{i}{6});
end
  
% Start collecting data once the trigger is received
start(ai)
waittilstop(ai,2*duration)
delete(ai)
clear ai
