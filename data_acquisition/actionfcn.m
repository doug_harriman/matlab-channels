function actionfcn(obj,event,handles)

switch lower(event.Type),
case 'stop'
    set(handles.update,'string','Data Acquisition Complete');drawnow;
    [data,time] = getdata(obj);
    data = double(data);
    [m,numChans] = size(data);
    
    %Send all info to the Channel Manager
    test.time = datestr(rem(now,1));
    test.date = datestr(floor(now));
    test.name = '';
    test.type = '';

    chanman
    chanman('ClearAll');
    sampleTime = 1/get(obj,'SampleRate');
    for i = 1:numChans,
        sensor_data = data(:,i);
        Name = obj.channel(i).ChannelName;
        Units = obj.channel(i).Units;
        chan = channel(Name,sensor_data,sampleTime,Units,test);
        chanman('AddChannel',chan);   
    end
    
case 'start'
    time = datestr(now,13);
    set(handles.update,'string',['Start event occurred at ' time '']);drawnow;
    
case 'trigger'
    time = datestr(now,13);
    set(handles.update,'string',['Trigger event occurred at ' time '']);drawnow;
    
otherwise
    disp(['Unhandled message: ''' event ''' passed to ACTIONHANDLER.']);

end