%TEK2CHAN  Import Tektronix scope data file to Channel Manager.
%  TEK2CHAN(FILENAME)
%
%  Targeted at file format from Tektronix TDS2024C.
%

% Doug Harriman (doug.harriman@simplexitypd.com)
% Created: 07-Nov-2013

function [] = tek2chan(filename)

% Error checks
narginchk(0,1);
if nargin < 1
    [filename,pathname] = uigetfile('*.csv','Select Tek CSV File');
    if filename == 0
        error('No file name selected.');
    end
    filename = [pathname filesep filename];
end

% Make sure we can find the file.
if ~exist(filename,'file')
    error(['Unable to find file: ' filename]);
end

% Make sure ChannelManager is open.
chanman;

%% Initialize variables.
delimiter = ',';

%% Format string for each line of text:
%   column1: text (%s)
%	column2: text (%s)
%   column4: double (%f)
%	column5: double (%f)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%s%s%*s%f%f%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');
finishup = onCleanup(@()fclose(fileID));

%% Read columns of data according to format string.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);

%% Post processing for unimportable data.

%% Allocate imported array to column variable names
vars = dataArray{1}(1:18);
vals = dataArray{2}(1:18);

% Create variable dictionary
d = dict;
for i = 1:length(vars)
    var = strtrim(vars{i});
    if ~isempty(var)
        d(var) = vals{i};
    end
end    

% Data vectors.
%time = dataArray{:, 3};
data = dataArray{:, 4};

% Apply metadata.
name  = d('Source');
ustr  = d('Vertical Units');
dt    = str2double(d('Sample Interval'));
tustr = d('Horizontal Units');
dt    = unit(dt,tustr);

% Create channel
ch = channel(name,data,dt,ustr);
chanman('AddChannel',ch);